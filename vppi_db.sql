-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: vppi_db
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mf0kn_admintools_acl`
--

DROP TABLE IF EXISTS `mf0kn_admintools_acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_acl` (
  `user_id` bigint(20) unsigned NOT NULL,
  `permissions` mediumtext,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_acl`
--

LOCK TABLES `mf0kn_admintools_acl` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_acl` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_acl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_adminiplist`
--

DROP TABLE IF EXISTS `mf0kn_admintools_adminiplist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_adminiplist` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_adminiplist`
--

LOCK TABLES `mf0kn_admintools_adminiplist` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_adminiplist` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_adminiplist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_badwords`
--

DROP TABLE IF EXISTS `mf0kn_admintools_badwords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_badwords` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(255) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_badwords`
--

LOCK TABLES `mf0kn_admintools_badwords` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_badwords` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_badwords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_cookies`
--

DROP TABLE IF EXISTS `mf0kn_admintools_cookies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_cookies` (
  `series` varchar(255) CHARACTER SET latin1 NOT NULL,
  `client_hash` varchar(255) CHARACTER SET latin1 NOT NULL,
  `valid_to` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_cookies`
--

LOCK TABLES `mf0kn_admintools_cookies` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_cookies` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_cookies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_customperms`
--

DROP TABLE IF EXISTS `mf0kn_admintools_customperms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_customperms` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `perms` varchar(4) DEFAULT '0644',
  UNIQUE KEY `id` (`id`),
  KEY `path` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_customperms`
--

LOCK TABLES `mf0kn_admintools_customperms` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_customperms` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_customperms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_filescache`
--

DROP TABLE IF EXISTS `mf0kn_admintools_filescache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_filescache` (
  `admintools_filescache_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `path` varchar(2048) NOT NULL,
  `filedate` int(11) NOT NULL DEFAULT '0',
  `filesize` int(11) NOT NULL DEFAULT '0',
  `data` blob,
  `checksum` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`admintools_filescache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_filescache`
--

LOCK TABLES `mf0kn_admintools_filescache` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_filescache` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_filescache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_ipautoban`
--

DROP TABLE IF EXISTS `mf0kn_admintools_ipautoban`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_ipautoban` (
  `ip` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT 'other',
  `until` datetime DEFAULT NULL,
  UNIQUE KEY `ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_ipautoban`
--

LOCK TABLES `mf0kn_admintools_ipautoban` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_ipautoban` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_ipautoban` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_ipautobanhistory`
--

DROP TABLE IF EXISTS `mf0kn_admintools_ipautobanhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_ipautobanhistory` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) CHARACTER SET latin1 NOT NULL,
  `reason` varchar(255) CHARACTER SET latin1 DEFAULT 'other',
  `until` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_ipautobanhistory`
--

LOCK TABLES `mf0kn_admintools_ipautobanhistory` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_ipautobanhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_ipautobanhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_ipblock`
--

DROP TABLE IF EXISTS `mf0kn_admintools_ipblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_ipblock` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_ipblock`
--

LOCK TABLES `mf0kn_admintools_ipblock` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_ipblock` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_ipblock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_log`
--

DROP TABLE IF EXISTS `mf0kn_admintools_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `logdate` datetime NOT NULL,
  `ip` varchar(40) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `reason` enum('other','adminpw','ipwl','ipbl','sqlishield','antispam','tpone','tmpl','template','muashield','csrfshield','badbehaviour','geoblocking','rfishield','dfishield','uploadshield','xssshield','httpbl','loginfailure','securitycode','external','awayschedule','admindir') DEFAULT NULL,
  `extradata` mediumtext,
  UNIQUE KEY `id` (`id`),
  KEY `idx_admintools_log_logdate_reason` (`logdate`,`reason`)
) ENGINE=InnoDB AUTO_INCREMENT=269363 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mf0kn_admintools_profiles`
--

DROP TABLE IF EXISTS `mf0kn_admintools_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `configuration` longtext,
  `filters` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_profiles`
--

LOCK TABLES `mf0kn_admintools_profiles` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_profiles` DISABLE KEYS */;
INSERT INTO `mf0kn_admintools_profiles` VALUES (1,'Default PHP Change Scanner Profile','','');
/*!40000 ALTER TABLE `mf0kn_admintools_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_redirects`
--

DROP TABLE IF EXISTS `mf0kn_admintools_redirects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_redirects` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL,
  `dest` varchar(255) DEFAULT NULL,
  `ordering` bigint(20) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `keepurlparams` tinyint(1) NOT NULL DEFAULT '1',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_redirects`
--

LOCK TABLES `mf0kn_admintools_redirects` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_redirects` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_redirects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_scanalerts`
--

DROP TABLE IF EXISTS `mf0kn_admintools_scanalerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_scanalerts` (
  `admintools_scanalert_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `path` varchar(2048) NOT NULL,
  `scan_id` bigint(20) NOT NULL DEFAULT '0',
  `diff` mediumtext,
  `threat_score` int(11) NOT NULL DEFAULT '0',
  `acknowledged` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`admintools_scanalert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_scanalerts`
--

LOCK TABLES `mf0kn_admintools_scanalerts` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_scanalerts` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_scanalerts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_scans`
--

DROP TABLE IF EXISTS `mf0kn_admintools_scans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_scans` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `comment` longtext,
  `backupstart` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `backupend` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('run','fail','complete') NOT NULL DEFAULT 'run',
  `origin` varchar(30) NOT NULL DEFAULT 'backend',
  `type` varchar(30) NOT NULL DEFAULT 'full',
  `profile_id` bigint(20) NOT NULL DEFAULT '1',
  `archivename` longtext,
  `absolute_path` longtext,
  `multipart` int(11) NOT NULL DEFAULT '0',
  `tag` varchar(255) DEFAULT NULL,
  `backupid` varchar(255) DEFAULT NULL,
  `filesexist` tinyint(3) NOT NULL DEFAULT '1',
  `remote_filename` varchar(1000) DEFAULT NULL,
  `total_size` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_fullstatus` (`filesexist`,`status`),
  KEY `idx_stale` (`status`,`origin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_scans`
--

LOCK TABLES `mf0kn_admintools_scans` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_scans` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_scans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_storage`
--

DROP TABLE IF EXISTS `mf0kn_admintools_storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_storage` (
  `key` varchar(255) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_storage`
--

LOCK TABLES `mf0kn_admintools_storage` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_storage` DISABLE KEYS */;
INSERT INTO `mf0kn_admintools_storage` VALUES ('cparams','{\"ipwl\":\"0\",\"ipbl\":\"0\",\"adminpw\":\"lusted\",\"adminlogindir\":\"\",\"awayschedule_from\":\"\",\"awayschedule_to\":\"\",\"sqlishield\":\"1\",\"xssshield\":\"0\",\"xssshield_allowphp\":\"0\",\"xssshield_safe_keys\":\"password, passwd, token, _token, password1, password2, text\",\"muashield\":\"1\",\"csrfshield\":\"0\",\"rfishield\":\"1\",\"dfishield\":\"1\",\"uploadshield\":\"1\",\"antispam\":\"0\",\"blockinstall\":\"0\",\"nonewadmins\":\"0\",\"resetjoomlatfa\":\"0\",\"nofesalogin\":\"0\",\"trackfailedlogins\":\"1\",\"custgenerator\":\"0\",\"generator\":\"\",\"tmpl\":\"1\",\"tmplwhitelist\":\"component,system,raw\",\"template\":\"1\",\"allowsitetemplate\":\"0\",\"httpblenable\":\"0\",\"bbhttpblkey\":\"\",\"httpblthreshold\":\"25\",\"httpblmaxage\":\"30\",\"httpblblocksuspicious\":\"0\",\"neverblockips\":\"\",\"whitelist_domains\":\".googlebot.com,.search.msn.com\",\"tsrenable\":\"0\",\"emailafteripautoban\":\"\",\"tsrstrikes\":\"3\",\"tsrnumfreq\":\"1\",\"tsrfrequency\":\"hour\",\"tsrbannum\":\"1\",\"tsrbanfrequency\":\"day\",\"permaban\":\"0\",\"permabannum\":\"0\",\"spammermessage\":\"You are a spammer, hacker or an otherwise bad person.\",\"saveusersignupip\":\"0\",\"logbreaches\":\"1\",\"iplookupscheme\":\"http\",\"iplookup\":\"ip-lookup.net\\/index.php?ip={ip}\",\"emailbreaches\":\"\",\"emailonadminlogin\":\"\",\"emailonfailedadminlogin\":\"\",\"showpwonloginfailure\":\"1\",\"reasons_nolog\":\"geoblocking\",\"reasons_noemail\":\"geoblocking\",\"email_throttle\":\"1\",\"custom403msg\":\"\",\"use403view\":\"0\",\"htconfig\":\"eyJodHRwc2hvc3QiOiJuZXcudnBwaWhvbWVzLmNvbSIsImh0dHBob3N0IjoibmV3LnZwcGlob21lcy5jb20iLCJzeW1saW5rcyI6IjAiLCJyZXdyaXRlYmFzZSI6IlwvIiwiZmlsZW9yZGVyIjoiMSIsImV4cHRpbWUiOiIwIiwiYXV0b2NvbXByZXNzIjoiMCIsImF1dG9yb290IjoiMSIsInd3d3JlZGlyIjoiMSIsIm9sZGRvbWFpbiI6IiIsImh0dHBzdXJscyI6W10sImhzdHNoZWFkZXIiOiIwIiwibm9mcmFtZSI6MCwibm90cmFjZXRyYWNrIjowLCJub2Rpcmxpc3RzIjoiMSIsImZpbGVpbmoiOiIxIiwicGhwZWFzdGVyIjoiMSIsIm5vaG9nZ2VycyI6IjAiLCJsZWZ0b3ZlcnMiOiIxIiwiaG9nZ2VyYWdlbnRzIjpbIldlYkJhbmRpdCIsIndlYmJhbmRpdCIsIkFjdW5ldGl4IiwiYmlubGFyIiwiQmxhY2tXaWRvdyIsIkJvbHQgMCIsIkJvdCBtYWlsdG86Y3JhZnRib3RAeWFob28uY29tIiwiQk9UIGZvciBKQ0UiLCJjYXNwZXIiLCJjaGVja3ByaXZhY3kiLCJDaGluYUNsYXciLCJjbHNodHRwIiwiY21zd29ybGRtYXAiLCJjb21vZG8iLCJDdXN0byIsIkRlZmF1bHQgQnJvd3NlciAwIiwiZGlhdm9sIiwiRElJYm90IiwiRElTQ28iLCJkb3Rib3QiLCJEb3dubG9hZCBEZW1vbiIsImVDYXRjaCIsIkVpckdyYWJiZXIiLCJFbWFpbENvbGxlY3RvciIsIkVtYWlsU2lwaG9uIiwiRW1haWxXb2xmIiwiRXhwcmVzcyBXZWJQaWN0dXJlcyIsImV4dHJhY3QiLCJFeHRyYWN0b3JQcm8iLCJFeWVOZXRJRSIsImZlZWRmaW5kZXIiLCJGSHNjYW4iLCJGbGFzaEdldCIsImZsaWNreSIsIkdldFJpZ2h0IiwiR2V0V2ViISIsIkdvLUFoZWFkLUdvdC1JdCIsIkdvIVppbGxhIiwiZ3JhYiIsIkdyYWJOZXQiLCJHcmFmdWxhIiwiaGFydmVzdCIsIkhNVmlldyIsImlhX2FyY2hpdmVyIiwiSW1hZ2UgU3RyaXBwZXIiLCJJbWFnZSBTdWNrZXIiLCJJbnRlckdFVCIsIkludGVybmV0IE5pbmphIiwiSW50ZXJuZXRTZWVyLmNvbSIsImpha2FydGEiLCJKYXZhIiwiSmV0Q2FyIiwiSk9DIFdlYiBTcGlkZXIiLCJrbWNjcmV3IiwibGFyYmluIiwiTGVlY2hGVFAiLCJsaWJ3d3ciLCJNYXNzIERvd25sb2FkZXIiLCJNYXh0aG9uJCIsIm1pY3Jvc29mdC51cmwiLCJNSURvd24gdG9vbCIsIm1pbmVyIiwiTWlzdGVyIFBpWCIsIk5FV1QiLCJNU0Zyb250UGFnZSIsIk5hdnJvYWQiLCJOZWFyU2l0ZSIsIk5ldCBWYW1waXJlIiwiTmV0QW50cyIsIk5ldFNwaWRlciIsIk5ldFpJUCIsIm51dGNoIiwiT2N0b3B1cyIsIk9mZmxpbmUgRXhwbG9yZXIiLCJPZmZsaW5lIE5hdmlnYXRvciIsIlBhZ2VHcmFiYmVyIiwiUGFwYSBGb3RvIiwicGF2dWsiLCJwY0Jyb3dzZXIiLCJQZW9wbGVQYWwiLCJwbGFuZXR3b3JrIiwicHNib3QiLCJwdXJlYm90IiwicHljdXJsIiwiUmVhbERvd25sb2FkIiwiUmVHZXQiLCJSaXBwZXJzIDAiLCJTZWFNb25rZXkkIiwic2l0ZWNoZWNrLmludGVybmV0c2Vlci5jb20iLCJTaXRlU25hZ2dlciIsInNreWdyaWQiLCJTbWFydERvd25sb2FkIiwic3Vja2VyIiwiU3VwZXJCb3QiLCJTdXBlckhUVFAiLCJTdXJmYm90IiwidEFrZU91dCIsIlRlbGVwb3J0IFBybyIsIlRvYXRhIGRyYWdvc3RlYSBtZWEgcGVudHJ1IGRpYXZvbGEiLCJ0dXJuaXQiLCJ2aWtzcGlkZXIiLCJWb2lkRVlFIiwiV2ViIEltYWdlIENvbGxlY3RvciIsIldlYiBTdWNrZXIiLCJXZWJBdXRvIiwiV2ViQ29waWVyIiwiV2ViRmV0Y2giLCJXZWJHbyBJUyIsIldlYkxlYWNoZXIiLCJXZWJSZWFwZXIiLCJXZWJTYXVnZXIiLCJXZWJzaXRlIGVYdHJhY3RvciIsIldlYnNpdGUgUXVlc3RlciIsIldlYlN0cmlwcGVyIiwiV2ViV2hhY2tlciIsIldlYlpJUCIsIldnZXQiLCJXaWRvdyIsIldXVy1NZWNoYW5pemUiLCJXV1dPRkZMRSIsIlhhbGRvbiBXZWJTcGlkZXIiLCJZYW5kZXgiLCJaZXVzIiwiem1ldSIsIkNhem9vZGxlQm90IiwiZGlzY29ib3QiLCJlY3hpIiwiR1Q6OldXVyIsImhlcml0cml4IiwiSFRUUDo6TGl0ZSIsIkhUVHJhY2siLCJpYV9hcmNoaXZlciIsImlkLXNlYXJjaCIsImlkLXNlYXJjaC5vcmciLCJJREJvdCIsIkluZHkgTGlicmFyeSIsIklSTGJvdCIsIklTQyBTeXN0ZW1zIGlSYyBTZWFyY2ggMi4xIiwiTGlua3NNYW5hZ2VyLmNvbV9ib3QiLCJsaW5rd2Fsa2VyIiwibHdwLXRyaXZpYWwiLCJNRkNfVGVhcl9TYW1wbGUiLCJNaWNyb3NvZnQgVVJMIENvbnRyb2wiLCJNaXNzaWd1YSBMb2NhdG9yIiwicGFuc2NpZW50LmNvbSIsIlBFQ0w6OkhUVFAiLCJQSFBDcmF3bCIsIlBsZWFzZUNyYXdsIiwiU0JJZGVyIiwiU25vb3B5IiwiU3RlZWxlciIsIlVSSTo6RmV0Y2giLCJ1cmxsaWIiLCJXZWIgU3Vja2VyIiwid2ViYWx0YSIsIldlYkNvbGxhZ2UiLCJXZWxscyBTZWFyY2ggSUkiLCJXRVAgU2VhcmNoIiwiemVybWVsbyIsIlp5Qm9yZyIsIkluZHkgTGlicmFyeSIsImxpYnd3dy1wZXJsIiwiR28hWmlsbGEiLCJUdXJuaXRpbkJvdCJdLCJiYWNrZW5kcHJvdCI6IjEiLCJmcm9udGVuZHByb3QiOiIxIiwiYmVwZXhkaXJzIjpbImNvbXBvbmVudHMiLCJtb2R1bGVzIiwidGVtcGxhdGVzIiwiaW1hZ2VzIiwicGx1Z2lucyJdLCJiZXBleHR5cGVzIjpbImpwZSIsImpwZyIsImpwZWciLCJqcDIiLCJqcGUyIiwicG5nIiwiZ2lmIiwiYm1wIiwiY3NzIiwianMiLCJzd2YiLCJodG1sIiwibXBnIiwibXAzIiwibXBlZyIsIm1wNCIsImF2aSIsIndhdiIsIm9nZyIsIm9ndiIsInhscyIsInhsc3giLCJkb2MiLCJkb2N4IiwicHB0IiwicHB0eCIsInppcCIsInJhciIsInBkZiIsInhwcyIsInR4dCIsIjd6Iiwic3ZnIiwib2R0Iiwib2RzIiwib2RwIiwiZmx2IiwibW92IiwiaHRtIiwidHRmIiwid29mZiIsImVvdCIsIkpQRyIsIkpQRUciLCJQTkciLCJHSUYiLCJDU1MiLCJKUyIsIlRURiIsIldPRkYiLCJFT1QiXSwiZmVwZXhkaXJzIjpbImNvbXBvbmVudHMiLCJtb2R1bGVzIiwidGVtcGxhdGVzIiwiaW1hZ2VzIiwicGx1Z2lucyIsIm1lZGlhIiwibGlicmFyaWVzIiwibWVkaWFcL2p1aVwvZm9udHMiXSwiZmVwZXh0eXBlcyI6WyJqcGUiLCJqcGciLCJqcGVnIiwianAyIiwianBlMiIsInBuZyIsImdpZiIsImJtcCIsImNzcyIsImpzIiwic3dmIiwiaHRtbCIsIm1wZyIsIm1wMyIsIm1wZWciLCJtcDQiLCJhdmkiLCJ3YXYiLCJvZ2ciLCJvZ3YiLCJ4bHMiLCJ4bHN4IiwiZG9jIiwiZG9jeCIsInBwdCIsInBwdHgiLCJ6aXAiLCJyYXIiLCJwZGYiLCJ4cHMiLCJ0eHQiLCI3eiIsInN2ZyIsIm9kdCIsIm9kcyIsIm9kcCIsImZsdiIsIm1vdiIsImljbyIsImh0bSIsInR0ZiIsIndvZmYiLCJlb3QiLCJKUEciLCJKUEVHIiwiUE5HIiwiR0lGIiwiQ1NTIiwiSlMiLCJUVEYiLCJXT0ZGIiwiRU9UIl0sImV4Y2VwdGlvbmZpbGVzIjpbImFkbWluaXN0cmF0b3JcL2NvbXBvbmVudHNcL2NvbV9ha2VlYmFcL3Jlc3RvcmUucGhwIiwiYWRtaW5pc3RyYXRvclwvY29tcG9uZW50c1wvY29tX2FkbWludG9vbHNcL3Jlc3RvcmUucGhwIiwiYWRtaW5pc3RyYXRvclwvY29tcG9uZW50c1wvY29tX2pvb21sYXVwZGF0ZVwvcmVzdG9yZS5waHAiXSwiZXhjZXB0aW9uZGlycyI6WyJsaWJyYXJpZXNcL2dhbnRyeVwvYWRtaW5cL3dpZGdldHNcL2ZvbnRzXC9qcyJdLCJmdWxsYWNjZXNzZGlycyI6WyJhZG1pbmlzdHJhdG9yXC90ZW1wbGF0ZXNcL2lzaXMiXSwiY3VzdGhlYWQiOiIiLCJjdXN0Zm9vdCI6IiJ9\"}');
/*!40000 ALTER TABLE `mf0kn_admintools_storage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_wafexceptions`
--

DROP TABLE IF EXISTS `mf0kn_admintools_wafexceptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_wafexceptions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option` varchar(255) DEFAULT NULL,
  `view` varchar(255) DEFAULT NULL,
  `query` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_wafexceptions`
--

LOCK TABLES `mf0kn_admintools_wafexceptions` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_wafexceptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_admintools_wafexceptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_admintools_waftemplates`
--

DROP TABLE IF EXISTS `mf0kn_admintools_waftemplates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_admintools_waftemplates` (
  `admintools_waftemplate_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reason` varchar(255) NOT NULL,
  `language` varchar(10) NOT NULL DEFAULT '*',
  `subject` varchar(255) NOT NULL,
  `template` text NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `email_num` tinyint(3) unsigned NOT NULL,
  `email_numfreq` tinyint(3) unsigned NOT NULL,
  `email_freq` enum('','second','minute','hour','day') NOT NULL DEFAULT '',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`admintools_waftemplate_id`),
  UNIQUE KEY `admintools_waftemplate_keylang` (`reason`,`language`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_admintools_waftemplates`
--

LOCK TABLES `mf0kn_admintools_waftemplates` WRITE;
/*!40000 ALTER TABLE `mf0kn_admintools_waftemplates` DISABLE KEYS */;
INSERT INTO `mf0kn_admintools_waftemplates` VALUES (1,'all','*','Security exception on [SITENAME]','<div style=\"background-color: #e0e0e0; padding: 10px 20px;\">\r\n<div style=\"background-color: #f9f9f9; border-radius: 10px; padding: 5px 10px;\">\r\n<p>Hello,</p>\r\n<p>We would like to notify you that a security exception was detected on your site, [SITENAME], with the following details:</p>\r\n<p>IP Address: [IP] (IP Lookup: [LOOKUP])<br />Reason: [REASON]</p>\r\n<p>If this kind of security exception repeats itself, please log in to your site\'s back-end and add this IP address to your Admin Tools\'s Web Application Firewall feature in order to completely block the misbehaving user.</p>\r\n<p>Best regards,</p>\r\n<p>The [SITENAME] team</p>\r\n</div>\r\n<p style=\"font-size: x-small; color: #667;\">You are receiving this automatic email message because you have a subscription in <em>[SITENAME]</em>. <span style=\"line-height: 1.3em;\">Do not reply to this email, it\'s sent from an unmonitored email address.</span></p>\r\n</div>\r\n<p style=\"text-align: right; font-size: 7pt; color: #ccc;\">Powered by <a style=\"color: #ccf; text-decoration: none;\" href=\"https://www.akeebabackup.com/products/admin-tools.html\">Akeeba AdminTools</a></p>',1,5,1,'hour','2014-04-14 14:36:34',800,'2014-04-14 14:42:38',800),(2,'user-reactivate','*','User deactivated on [SITENAME]','<div style=\"background-color: #e0e0e0; padding: 10px 20px;\">\r\n<div style=\"background-color: #f9f9f9; border-radius: 10px; padding: 5px 10px;\">\r\n<p>Hello,</p>\r\n<p>on [DATE] the user [USER] has been de-activated since he failed too many logins (recorded IP: [IP]).</p>\r\n<p>Please click on the following link to activate the user again:<br />[ACTIVATE]</p>\r\n<p>If this kind of security exception repeats itself, please log in to your site\'s back-end and add this IP address to your Admin Tools\'s Web Application Firewall feature in order to completely block the misbehaving user.</p>\r\n<p>Best regards,</p>\r\n<p>The [SITENAME] team</p>\r\n</div>\r\n<p style=\"font-size: x-small; color: #667;\">You are receiving this automatic email message because you have a subscription in <em>[SITENAME]</em>. <span style=\"line-height: 1.3em;\">Do not reply to this email, it\'s sent from an unmonitored email address.</span></p>\r\n</div>\r\n<p style=\"text-align: right; font-size: 7pt; color: #ccc;\">Powered by <a style=\"color: #ccf; text-decoration: none;\" href=\"https://www.akeebabackup.com/products/admin-tools.html\">Akeeba AdminTools</a></p>',1,0,0,'','2014-04-24 14:44:49',800,'0000-00-00 00:00:00',0);
/*!40000 ALTER TABLE `mf0kn_admintools_waftemplates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_ak_profiles`
--

DROP TABLE IF EXISTS `mf0kn_ak_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_ak_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `configuration` longtext,
  `filters` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_ak_profiles`
--

LOCK TABLES `mf0kn_ak_profiles` WRITE;
/*!40000 ALTER TABLE `mf0kn_ak_profiles` DISABLE KEYS */;
INSERT INTO `mf0kn_ak_profiles` VALUES (1,'Default Backup Profile','###AES128###tVXwBTYN/AIKHU2IA+qaqTEQwufwNkpR7HYtshpFMvdmk13kOR9JvAlQ2w2UNg+BTDhb/H9yHtLGhoLMSJhmMseYJkbqtv2d/leTyE+W8r7xf0w3I9yPiygLvCDw3amQbG+UDOhuJ6Q7rASJHHjW59dmPjlrvOl3AzfubS86OG6CjnB9c6yedtZkdk3uaPZp1xjhHA4qKGvuBKGOom6mRgWDpIl32QGL1lBdtd6Thay2VGVryZw7cm2Pd6GeZKyjFzSYbHCttII4stH5fLyxAOTIgdDUDLgvd0XXgWWbHWiMRVo0N7YW31JSWuGD99mzR0+p+hYTi2Zy5xKLlnRkNCq2Ix/mCJFDzTsivbqMrMPJKQnJa0RcfezDGRtnFJhW8202X7q6Ls4BCgGMktrfP8ZIGvbrFG1Zd5FmjurELsMPKSMKZKYZLXerejdNAZ6o80QQH4mhjLg3cMxeyuo+7gmILlvYmWGpcXF/OFtAbM19d0KSuttKFu5pIjuY0G872Av2EEgPFJCXXVQJu9n9aE0roTiJk9bEFEUJUgFKjxvUDXgHHQ+2VNez3eR0Oos5LNqeqzz/XqykX7d2xHz7VW2mUSSCrfw87RPFPYmz1k77Edbwl8YmreMBJ6rPVuGCretbspwJ3qTa72DILSgzQIkAEBx6BOZOVj9n1Cx+B/rnah/xHT4YY8YjflLJ1lDhFqrA/aZgWvs/GTKS+zufoj1FqKgAkyaoXq7ASzM+u0o/ERopmsboCFZruewxMb4W8+gd+ax/1FDg8qotHo0IYFjfi9i5Wx50tgyhNYerk72boiivV5zWdIYzya9J4H0DsZ7G4t7naLwZaKrypxg/IZ8hIw57WaQDBPNtArHFSJUtLTkHTVhwmsNFc2dRcj3ynQH2/UkUU1+Xv/f0LCDTZVstIZiuPiLE4PJuiVq0wpv7Y8a2SoL8X8jaGt1Vmlep4bp9hOaHdLzhl59MDr5Timk2ISqQGgATo+GCsN3jDKiV1MouDmxuPFLFChs2WtjdHC6WlDkGATKZjQ8rwQgianbxklY9d5T9Yex5gRKfiV6Q/BftQofENqJ6HUfskiib2gn7vYvaG8YdxOOQwc6wSlMU3WW9PUa9Ad0Ka+h6F8fJiJF7ir6LQjRYTRaAnt4H/ewQxv8CqNTGJcAzqJKjZ2ZLD352ndSq5aTJVrZ7NQetvQ9XB3GIX61pBVpHXsWcKe2bXvj/5waxTiw5HJS5xu7e8+tBe0BdUfx8FrG9pxTA21Sj50rLIRJNlBtPPzMjBwYJQaTPrgqZc2MXYN+J0Yw5hmOxaeEV2OiEaGEFgtwMUUnGpYf15PVPVNIwt1gj4nQTJZNXUr1Y1l9WHmtBaejctrVGLPK+WMtp/H+EkVdeI+jSxSsWyfITpt2/VBpbukIgoBgRBPVNwfB+qBiJMZ9Yp54/VBG+kJHi7a1p7obeW/011WSSBRWw2nvHZp8W6wc/4iTaNhpeX+3ZqzDebecumKOtRUYMJ29iaoimHpSC/FFdQhYohWlnroQRkweoThIHGa1XokUqH3IHYo8YAVWsSxipWXjljAcms1EgqRbZ24yg6OjZeYZ+ABsSl0829IPrDpCSx/5jMsfCtkp2PhzSQhdsC0XjLXqs29LVu1c7O8lAAbDVesBZLbFBhuIEF5SgrmxUeqllb4uYkm1MZsWt5tlRtRZ6BcUxR+PAcbc4qTHlp03do0GcCi0lA1Vf0WdScJpMnPsZQ100i4HdRG7zRna+scmjevE6q1ulJoQBPd9T4pEJtC620N23hQ68DZ+KdHlgxX07uAa/zUXX82n/+3vYvkkODrHNrwpno7Dsnq5zOC4qQV//f/XapdUKgPOu0Ma8s3pxvsj5tcFQXHTOXwqNt1MQdjgq2IXrb1+9i2OOYMLALkBjGAgiNHSoMCLYEOgBnVUdX55nk9viQxJUj4773nJcLG2FKbkzPdR7z0+aShzCzDOLaVMVv1zNorFAf+wLR3UZUE7MR/8abE1jDLfdKgwK9Vnrfxt77+F45NIPkzAymBZkIuYJJ0w5CQaIHsrbJy0ltyB6PphnR1nW7TkWhUf81uaCZ7uFxKs/+oN22B27Ezfp5xooKiH2MmZOvu/B9mFYAbiYvSjiSr+AZbE6CWaMMpj/gteZo97ZT2cyRsG2644gJsRSea1tY75HhjSKf0y+O8j3GfNQgGQ0YQehmuiVB9StixV0Mg6YMGBwiwl9j+XzYLB2nhjmVtOewwtwNHI8eQ7fW+I0jWS3/QpJIqmCe8sh/SerACf9YDW/g6JHr7dX5GzdJHaNVzZ8xe3XQhi32kC/bJ2Dt8EKhP6KN9ifw2c0J9h50VRQ9wdjfcWD0MNdTjfJsTcGUbnmiBa3usC9FsQPCepnIVGHzrM46IPMsz9R5qDZiHQq2HvVLdyMrPKEYQK01jK7LwboFmdpPiJHVAeExtS317UxKnE8duWBO3C7zSDgtno1cJ7ZARVc7qCAvlQ1JKrlPvzvE+z3GpVrhwTjV/Rz20n4VTCJoJN3gSvoay92I+axzgUWlmzvxTMTgOevF3PyWb6wk2A0cOD8VyfLRaqqMUYAyfmr4+WNowz7ti9Wyvr33DZjs/ylBEZGOHPLzP0nuwiBgMoD2ooq6aC7IbOLUwTxkwIk01x5EjFwPgmepnzksy/B0OjAcDfTSpW81guxtZpQ3laoVevfOc+ugb/u3vW0nvR/E4n2b67/EqL5CDI+IP+WWOl6cv5ddlx2FEH8mStLTrLU+CnQWjLO0xF4L+DyhaJ/SiSlNcBT0uHmuWI0D+BfBHyZzzBMTu4Pk/3j6hvgTNgNdJywye6oD2vVOrOO1Bhp3ZmlYeglJdeqJb2ISoqv/DeXc+jNj0x1OeRrnnpX8meCGZN6NkWL8BdTAIZ74G5jMrgKUbt3hvvJ3gebU8sUjgDsiJmcsv2Sp1+RP8aNPpxEgRsJRxT4ep98oAFLn+WEjiLHEnhdi/xn0Fauijxv838yb9WDE056q7bHWb6MSz83JxUIBQm4tOWa1FQNxXepvw/2+BeJEvL/v2jxtfzr0SzByc64YArVW9fI0377gJbY9maQX+8gSuMFS7baFzkh669LG5rw+n8Vm7P/uwg50BDlF6QZPjv+Ko+dDP6h7qXt6tFA9bzOTWvVE9EtCGuqtE0dLXkhlp1MlDIcGi60aIyE5q51d7HZaeBUnYYINs80B4CZWqaG96iJhklmbkrISV/Uu5O624kDUvittDJGCldYjekYjMMizXtBwLgffn7Rv5yqoBAG0fATefcxkjKEt304R4dvEu0WnKh5M7GDxNWp5LtQ/okIqn0+f7QKWdFD7lr9n09tZju73R3GCs5MP+2k1SEaU48V8QhuOIaxBbuheEVcieF2aLWEEFnjze31gJMW58j0ONVr8Lh24hRlbDknwPpmIVAXfryKGALjoeDuD2XTOeqtW1gJNkDcoehMmekuhTsmHPBCVdwKM5exifIsRt77Jkc2jueX79tER6Y3HZoBToajfT5YXLIlM6cd5n/k0iOPPExdHNN8d6scSFwjKKz7Y4emFFtb0rOnqnGqsa8nGYVUU1mt65szItS3kd8gRpXZUBw097WsjZzdRK+pvbGLN5WvVZ+JnSntwAskiHzU5snqrYrcho2WtuYEWXc6/RA9rxcJ7kF5kEnFlVolnLAq2bD33sm6UpRUIFYrcz7iT1+6h0IZT+zJMyJgRHisU+vVoigF97nbjuteQM9VzdCqW1RnHia04886HW8uX9O8EDXBa39swD2M5oZFF09UU4bCJmTy8plqPVIKBV7+lCZtNs+X79uo/kPKtfA3T6POT7jkx11R6cMNFk0B1k9Lcfeo6SeA4GAO1fuE0iyBys9nNk6vDFnCZDCjZHWq24JSOti0xeQl37rPtqIhXDhidtO6WKyYKXIky/DM/ikhoRZyv8hbv0gPVODaFvruhagMv+BQFNWaUrgEjaqfId7KtjfSjmhwc25g7yuooGdydqkys1gXnHPHdIz16crmK+laKRHhDqys9BPLsDmBB4CCVKMsuK0kAaBai3ku0BzFd89hud3JveRt1PAfoWLaIauxwncB5y3H2yZhLM0eRG0CATMrSE4i/QXrKj4ZBhR1m0hHpmRDi3iFKyRPdnzst/gExJ08g2YQbsRjdtVhSzhtN9BYkLfsRPeAJGpW6+DASOCWGuGBsWqecL3XSKXGgpPwGOeo/A19epQx3yWi8AAHFpRAIO4b7Farn5wWm6HdOJKDTnBAq6cy9A914DbM4drzD8PKalB/oeOmGr6JDWRNXD+P50BmO6mdEQ08YFjdfCpm/e4WcSB1/sOmf0ql6U7DQPn8YFWEeV55DAFymGSiN40Fx5b+x7Vmy9ZACAgufMmcQjeWonXq7L6+chx/aC2tS1e1gA/HtCwFNQSJe6AwPFeqzhYkKh6dnffTgnAK0nPvNYKXdSlTkmuip9PvacmHqDCAWIPetSrhKCjzk5aVma4WZG1kDzjzbehcWwrDphUKQ5DgvySe3kjWzySfp2ftucJi6R1WWcU06361cH7235L6HNXTJ+ktpQzMs+44phy6m51EK3SztEDvARQKyqJI7LiU3KWp9XJGVeFm3JjQFAN//Dhw03x3xmyJc40undUD2zvIJY6wGyGpKWmJ90iZFKNftCBtQdc+rEFOJxOoxdd0mfonWQXyGEWPJoymzVSxIJOA59QusQeO5KyO9i6nol0S43Ku0gYv1Re3x0dFrwhvAINlpGWdSi0svf5N85NUwZbaTTPB/84qqvyxkbxbBdHPuka+5rGS/m+rLoz8rI3KR6vurC95YkEcTnipUA5246iZ/BJrTHy5iwbd8ae+Oyf0mRKdt1RXaA/nGcnltYMeUQ7sRqEhoGwpFoguB/l8gtP1DiCganigZKKeCKGPzA14ShyKQ8OAnVHyiX2DiY+eFFxpldS5XU4ZtU9TvdZDHpjCVpH/u1dkkxnE8ZI6gfSGo8I7LEzzh93sdW149shiOTcfowRj5JBzcpuqAf64gtXg9i7FbuA02k5ga805BEJ3EkqsjAQMDPZnbkpvWvOq0Mp1cqIFL3VGNiYkoh1XO7USTKHvdd4eucljOIXODwjGihLPqlCuD/5QQIet4axatfZrwODQT+r0h9/4H+Sjr2rvJm5oV0+pWc0GLQvWaYdNuQw1U54Lb1f43zv7JZDD6715d8VfBrHTWIT+KH+PJlGSM7RJrCnYg7GG+lYZu/24Jk2tEmBsMpTG9MoasT0zDrxmGdYAfdH5O6kq5P6ET7KMu0Bsbqrtd56AAgqGVFxaRKI6GQazDCMkrDRX7XCcfRIa3iAflYYnDCHdJSwnheSok8nt1xObbfo4FMcjmJUf56glrgWZ0stAZ4pg6HEA6dnch25hk9fDr656TVoRaTEOk1Es09rW46tQZvBPTBKfbSOMAfqU7SiwGfcim8ivn+VCwsYMhAS5XQMN1rOoABoq+9jN9pNu6/zjDrmcQ+WNDVCAVtu0rR1X871qO4TbHxEPXYeBo78PP7WINWFyRwhvZTOltgGCYpntUBs15errVLsZXxXOM5RxFIPlAWXye6LPgt6EgInzDWp94eCG0+kGwqBm5JDsCRI+vKBVTSxodik/V3jh+EOzgK53E4pYaa8o7EnVB20O+/nWyIGcILHtkrcVUkRKgp85GtkQnJvAk8S5Edg5zDwrOnkZ0yFhvMDzM9ZfaRgw8FuNykG9Vgpc3LRghxg9/EGRraOES1tU6z/+GSTzKWTM3QUTfZjDb/qnCb3aPqDFbNR0CmXLMpR5mpAVhVzcUieylaqfwT8+LxauKEravPV5yrtkr1Oc41WAKyEY4uKKyaPPUj1o2GtgJTgxn41tJu8QsdothXLdMKP72hic0Z6r7K+6oErpsDjv/Qd21mQzI9y2djXMnqefknmROWbyabfgugr1P1CdlHa30Fs6MferBFd3kPxVhHR6JJJA56ifkjOO0U3VMbYwpSKBI5dhxfjuwDNcdWOPJBx2BIrb0yaSAepkgORfMz/U7xi0lIRW/Na9QjGAtpuDnacXuwl0HiePAX9czKnbe+hiT9a1a8viaE0dP9akMZXjGeBZnYvhNFU+2mFhbP67Y24ioJ4jhM8S/xIJ9c3ev8VciZPFm7WtR3QpCYQ2Rk8b+392GU9Xi740U2e43HoHv3aSZbgLtMeULWjhJSAwX8kSE2n47HhYMZJ7ewlW6qGlcj2PHuJxp/bnqeUGiROLXErppcenJvsuUCvKw/VodPAvYm2FRawavXR4nwqJsFmlsGUSAAA=','');
/*!40000 ALTER TABLE `mf0kn_ak_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_ak_stats`
--

DROP TABLE IF EXISTS `mf0kn_ak_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_ak_stats` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `comment` longtext,
  `backupstart` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `backupend` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('run','fail','complete') NOT NULL DEFAULT 'run',
  `origin` varchar(30) NOT NULL DEFAULT 'backend',
  `type` varchar(30) NOT NULL DEFAULT 'full',
  `profile_id` bigint(20) NOT NULL DEFAULT '1',
  `archivename` longtext,
  `absolute_path` longtext,
  `multipart` int(11) NOT NULL DEFAULT '0',
  `tag` varchar(255) DEFAULT NULL,
  `backupid` varchar(255) DEFAULT NULL,
  `filesexist` tinyint(3) NOT NULL DEFAULT '1',
  `remote_filename` varchar(1000) DEFAULT NULL,
  `total_size` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_fullstatus` (`filesexist`,`status`),
  KEY `idx_stale` (`status`,`origin`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_ak_stats`
--

LOCK TABLES `mf0kn_ak_stats` WRITE;
/*!40000 ALTER TABLE `mf0kn_ak_stats` DISABLE KEYS */;
INSERT INTO `mf0kn_ak_stats` VALUES (1,'Backup taken on Wednesday, 02 July 2014 19:00','Initial Backup','2014-07-03 06:01:04','2014-07-03 06:01:35','complete','backend','full',1,'site-new.vppihomes.com-20140702-190104.jpa','/var/www/vppi_j3/administrator/components/com_akeeba/backup/site-new.vppihomes.com-20140702-190104.jpa',0,'backend',NULL,0,NULL,22574334),(2,'Backup taken on Monday, 14 July 2014 10:18','','2014-07-14 21:18:08','2014-07-14 21:18:43','complete','backend','full',1,'site-new.vppihomes.com-20140714-101808.jpa','/var/www/vppi_j3/administrator/components/com_akeeba/backup/site-new.vppihomes.com-20140714-101808.jpa',0,'backend',NULL,0,NULL,26659462),(3,'Backup taken on Tuesday, 15 July 2014 17:51','','2014-07-16 04:51:31','2014-07-16 04:52:09','complete','backend','full',1,'site-new.vppihomes.com-20140715-175131.jpa','/var/www/vppi_j3/administrator/components/com_akeeba/backup/site-new.vppihomes.com-20140715-175131.jpa',0,'backend',NULL,0,NULL,26817351),(4,'Backup taken on Wednesday, 30 July 2014 13:06','','2014-07-31 00:07:03','2014-07-31 00:07:37','complete','backend','full',1,'site-new.vppihomes.com-20140730-130703.jpa','/var/www/vppi_j3/administrator/components/com_akeeba/backup/site-new.vppihomes.com-20140730-130703.jpa',0,'backend',NULL,0,NULL,26852688),(5,'Backup taken on Wednesday, 30 July 2014 13:31','Updated Joomla v 3.3.3\nUpdated AdminTools v 3.0.3\nUpdated AkeebaBackup v 3.11.3\nUpdated JCE Editor v 2.4.2','2014-07-31 00:32:09','2014-07-31 00:32:40','complete','backend','full',1,'site-new.vppihomes.com-20140730-133209.jpa','/var/www/vppi_j3/administrator/components/com_akeeba/backup/site-new.vppihomes.com-20140730-133209.jpa',0,'backend',NULL,0,NULL,27338204),(6,'Backup taken on Wednesday, 10 September 2014 10:07','','2014-09-10 21:07:15','2014-09-10 21:07:50','complete','backend','full',1,'site-www.vppihomes.com-20140910-100715.jpa','/var/www/vppi_j3/administrator/components/com_akeeba/backup/site-www.vppihomes.com-20140910-100715.jpa',0,'backend',NULL,0,NULL,29984721),(7,'Backup taken on Tuesday, 23 September 2014 09:20','','2014-09-23 20:20:38','2014-09-23 20:21:18','complete','backend','full',1,'site-www.vppihomes.com-20140923-092038.jpa','/var/www/vppi_j3/administrator/components/com_akeeba/backup/site-www.vppihomes.com-20140923-092038.jpa',0,'backend','id7',0,NULL,28887655),(8,'Backup taken on Thursday, 02 October 2014 15:36','','2014-10-03 02:36:12','2014-10-03 02:36:51','complete','backend','full',1,'site-www.vppihomes.com-20141002-153612.jpa','/var/www/vppi_j3/administrator/components/com_akeeba/backup/site-www.vppihomes.com-20141002-153612.jpa',0,'backend','id8',0,NULL,28902139),(9,'Backup taken on Thursday, 02 October 2014 15:57','Updated Joomla 3.3.6\nUpdated Akeeba Backup 4.0.5\nUpdated AdminTools 3.2.1','2014-10-03 02:58:27','2014-10-03 02:58:55','complete','backend','full',1,'site-www.vppihomes.com-20141002-155827.jpa','/var/www/vppi_j3/administrator/components/com_akeeba/backup/site-www.vppihomes.com-20141002-155827.jpa',0,'backend','id9',0,NULL,29366136),(10,'Backup taken on Monday, 29 December 2014 17:35','','2014-12-30 06:35:18','0000-00-00 00:00:00','complete','backend','full',1,'site-vppihomes.com-20141229-173518.jpa','/var/www/vppi_j3/administrator/components/com_akeeba/backup/site-vppihomes.com-20141229-173518.jpa',0,'backend','id10',0,NULL,0),(11,'Backup taken on Thursday, 29 January 2015 12:52','','2015-01-30 04:52:48','2015-01-30 04:53:40','complete','backend','full',1,'site-www.vppihomes.com-20150129-125248.jpa','/var/www/vppihomes.com/www/administrator/components/com_akeeba/backup/site-www.vppihomes.com-20150129-125248.jpa',0,'backend','id11',1,NULL,61447261),(12,'Backup taken on Sunday, 03 April 2016 12:11','','2016-04-04 02:11:21','2016-04-04 02:12:34','complete','backend','full',1,'site-www.vppihomes.com-20160403-121120.jpa','/var/www/vppihomes.com/www/administrator/components/com_akeeba/backup/site-www.vppihomes.com-20160403-121120.jpa',0,'backend','id12',1,NULL,181331552),(13,'Backup taken on Wednesday, 23 November 2016 23:12','','2016-11-24 15:12:21','2016-11-24 15:13:53','complete','backend','full',1,'site-vppihomes.com-20161123-231221.jpa','/var/www/vppihomes.com/www/administrator/components/com_akeeba/backup/site-vppihomes.com-20161123-231221.jpa',0,'backend','id13',1,NULL,181437224);
/*!40000 ALTER TABLE `mf0kn_ak_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_ak_storage`
--

DROP TABLE IF EXISTS `mf0kn_ak_storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_ak_storage` (
  `tag` varchar(255) NOT NULL,
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data` longtext,
  PRIMARY KEY (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_ak_storage`
--

LOCK TABLES `mf0kn_ak_storage` WRITE;
/*!40000 ALTER TABLE `mf0kn_ak_storage` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_ak_storage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_akeeba_common`
--

DROP TABLE IF EXISTS `mf0kn_akeeba_common`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_akeeba_common` (
  `key` varchar(255) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_akeeba_common`
--

LOCK TABLES `mf0kn_akeeba_common` WRITE;
/*!40000 ALTER TABLE `mf0kn_akeeba_common` DISABLE KEYS */;
INSERT INTO `mf0kn_akeeba_common` VALUES ('stats_lastrun','1479971533'),('stats_siteid','11444308dc29deb5b640ace33e12b74f1a544662'),('stats_siteurl','930d00cb4d21b647981eba577f023c98');
/*!40000 ALTER TABLE `mf0kn_akeeba_common` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_assets`
--

DROP TABLE IF EXISTS `mf0kn_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) unsigned NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_asset_name` (`name`),
  KEY `idx_lft_rgt` (`lft`,`rgt`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_assets`
--

LOCK TABLES `mf0kn_assets` WRITE;
/*!40000 ALTER TABLE `mf0kn_assets` DISABLE KEYS */;
INSERT INTO `mf0kn_assets` VALUES (1,0,0,169,0,'root.1','Root Asset','{\"core.login.site\":{\"6\":1,\"2\":1},\"core.login.admin\":{\"6\":1},\"core.login.offline\":{\"6\":1},\"core.admin\":{\"8\":1},\"core.manage\":{\"7\":1},\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),(2,1,1,2,1,'com_admin','com_admin','{}'),(3,1,3,6,1,'com_banners','com_banners','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(4,1,7,8,1,'com_cache','com_cache','{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),(5,1,9,10,1,'com_checkin','com_checkin','{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),(6,1,11,12,1,'com_config','com_config','{}'),(7,1,13,16,1,'com_contact','com_contact','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),(8,1,17,28,1,'com_content','com_content','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),(9,1,29,30,1,'com_cpanel','com_cpanel','{}'),(10,1,31,32,1,'com_installer','com_installer','{\"core.admin\":[],\"core.manage\":{\"7\":0},\"core.delete\":{\"7\":0},\"core.edit.state\":{\"7\":0}}'),(11,1,33,34,1,'com_languages','com_languages','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(12,1,35,36,1,'com_login','com_login','{}'),(13,1,37,38,1,'com_mailto','com_mailto','{}'),(14,1,39,40,1,'com_massmail','com_massmail','{}'),(15,1,41,42,1,'com_media','com_media','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":{\"5\":1}}'),(16,1,43,44,1,'com_menus','com_menus','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(17,1,45,46,1,'com_messages','com_messages','{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),(18,1,47,106,1,'com_modules','com_modules','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(19,1,107,110,1,'com_newsfeeds','com_newsfeeds','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),(20,1,111,112,1,'com_plugins','com_plugins','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(21,1,113,114,1,'com_redirect','com_redirect','{\"core.admin\":{\"7\":1},\"core.manage\":[]}'),(22,1,115,116,1,'com_search','com_search','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),(23,1,117,118,1,'com_templates','com_templates','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(24,1,119,122,1,'com_users','com_users','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(25,1,123,126,1,'com_weblinks','com_weblinks','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),(26,1,127,128,1,'com_wrapper','com_wrapper','{}'),(27,8,18,27,2,'com_content.category.2','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),(28,3,4,5,2,'com_banners.category.3','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(29,7,14,15,2,'com_contact.category.4','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),(30,19,108,109,2,'com_newsfeeds.category.5','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),(31,25,124,125,2,'com_weblinks.category.6','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),(32,24,120,121,1,'com_users.category.7','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(33,1,129,130,1,'com_finder','com_finder','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),(34,1,131,132,1,'com_joomlaupdate','com_joomlaupdate','{\"core.admin\":[],\"core.manage\":[],\"core.delete\":[],\"core.edit.state\":[]}'),(35,1,133,134,1,'com_tags','com_tags','{\"core.admin\":[],\"core.manage\":[],\"core.manage\":[],\"core.delete\":[],\"core.edit.state\":[]}'),(36,1,135,136,1,'com_contenthistory','com_contenthistory','{}'),(37,1,137,138,1,'com_ajax','com_ajax','{}'),(38,1,139,140,1,'com_postinstall','com_postinstall','{}'),(39,18,48,49,2,'com_modules.module.1','Main Menu','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(40,18,50,51,2,'com_modules.module.2','Login','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(41,18,52,53,2,'com_modules.module.3','Popular Articles','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(42,18,54,55,2,'com_modules.module.4','Recently Added Articles','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(43,18,56,57,2,'com_modules.module.8','Toolbar','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(44,18,58,59,2,'com_modules.module.9','Quick Icons','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(45,18,60,61,2,'com_modules.module.10','Logged-in Users','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(46,18,62,63,2,'com_modules.module.12','Admin Menu','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(47,18,64,65,2,'com_modules.module.13','Admin Submenu','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(48,18,66,67,2,'com_modules.module.14','User Status','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(49,18,68,69,2,'com_modules.module.15','Title','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(50,18,70,71,2,'com_modules.module.16','Login Form','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(51,18,72,73,2,'com_modules.module.17','Breadcrumbs','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(52,18,74,75,2,'com_modules.module.79','Multilanguage status','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(53,18,76,77,2,'com_modules.module.86','Joomla Version','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(55,1,141,142,1,'#__vppi_homes.1','#__vppi_homes.1',''),(56,1,143,144,1,'com_gantry','gantry','{}'),(57,18,78,79,2,'com_modules.module.87','SCLogin',''),(58,1,145,146,1,'com_admintools','admintools','{\"core.admin\":[],\"core.manage\":[],\"admintools.utils\":[],\"admintools.security\":[],\"admintools.maintenance\":[]}'),(59,1,147,148,1,'com_akeeba','akeeba','{\"core.admin\":[],\"core.manage\":[],\"akeeba.backup\":[],\"akeeba.configure\":[],\"akeeba.download\":[]}'),(60,1,149,150,1,'com_jce','jce','{}'),(61,1,151,152,1,'com_vppi','com_vppi','{}'),(62,1,153,154,1,'#__vppi_homes.2','#__vppi_homes.2',''),(64,18,80,81,2,'com_modules.module.88','SCSocialStream',''),(65,18,82,83,2,'com_modules.module.89','SCSocialWidget',''),(66,18,84,85,2,'com_modules.module.90','JFBCSocialShare',''),(67,18,86,87,2,'com_modules.module.91','Footer Icons','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(68,18,88,89,2,'com_modules.module.92','Facebook Bar','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(69,27,19,20,3,'com_content.article.1','Mission Statement','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(70,27,21,22,3,'com_content.article.2','Vantage Point Brokers','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(71,27,23,24,3,'com_content.article.3','What Every Buyer Should Know','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(72,18,90,91,2,'com_modules.module.93','Home Search Heading','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(73,1,155,156,1,'#__vppi_homes.3','#__vppi_homes.3',''),(74,1,157,158,1,'#__vppi_homes.4','#__vppi_homes.4',''),(75,1,159,160,1,'#__vppi_homes.5','#__vppi_homes.5',''),(76,1,161,162,1,'#__vppi_homes.6','#__vppi_homes.6',''),(77,1,163,164,1,'#__vppi_homes.7','#__vppi_homes.7',''),(78,27,25,26,3,'com_content.article.4','Contact Us','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(79,18,92,93,2,'com_modules.module.94','Contact Form','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(80,18,94,95,2,'com_modules.module.95','Map','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(81,18,96,97,2,'com_modules.module.96','Facebook Sidebar','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(82,1,165,166,1,'com_xmap','com_xmap','{}'),(83,1,167,168,1,'#__vppi_homes.8','#__vppi_homes.8',''),(84,18,98,99,2,'com_modules.module.97','Vantage Point Properties, Inc.','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(85,18,100,101,2,'com_modules.module.98','VPPI Admin Icon','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),(86,18,102,103,2,'com_modules.module.99','Facebook Footer','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}'),(87,18,104,105,2,'com_modules.module.100','Facebook Sidebar Wrapper','{\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1}}');
/*!40000 ALTER TABLE `mf0kn_assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_associations`
--

DROP TABLE IF EXISTS `mf0kn_associations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.',
  PRIMARY KEY (`context`,`id`),
  KEY `idx_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_associations`
--

LOCK TABLES `mf0kn_associations` WRITE;
/*!40000 ALTER TABLE `mf0kn_associations` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_associations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_banner_clients`
--

DROP TABLE IF EXISTS `mf0kn_banner_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_banner_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_banner_clients`
--

LOCK TABLES `mf0kn_banner_clients` WRITE;
/*!40000 ALTER TABLE `mf0kn_banner_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_banner_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_banner_tracks`
--

DROP TABLE IF EXISTS `mf0kn_banner_tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  KEY `idx_track_date` (`track_date`),
  KEY `idx_track_type` (`track_type`),
  KEY `idx_banner_id` (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_banner_tracks`
--

LOCK TABLES `mf0kn_banner_tracks` WRITE;
/*!40000 ALTER TABLE `mf0kn_banner_tracks` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_banner_tracks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_banners`
--

DROP TABLE IF EXISTS `mf0kn_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `custombannercode` varchar(2048) NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `params` text NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) NOT NULL DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`),
  KEY `idx_banner_catid` (`catid`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_banners`
--

LOCK TABLES `mf0kn_banners` WRITE;
/*!40000 ALTER TABLE `mf0kn_banners` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_categories`
--

DROP TABLE IF EXISTS `mf0kn_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`extension`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_categories`
--

LOCK TABLES `mf0kn_categories` WRITE;
/*!40000 ALTER TABLE `mf0kn_categories` DISABLE KEYS */;
INSERT INTO `mf0kn_categories` VALUES (1,0,0,0,13,0,'','system','ROOT','root','','',1,0,'0000-00-00 00:00:00',1,'{}','','','{}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1),(2,27,1,1,2,1,'uncategorised','com_content','Uncategorised','uncategorised','','',1,0,'0000-00-00 00:00:00',1,'{\"category_layout\":\"\",\"image\":\"\"}','','','{\"author\":\"\",\"robots\":\"\"}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1),(3,28,1,3,4,1,'uncategorised','com_banners','Uncategorised','uncategorised','','',1,0,'0000-00-00 00:00:00',1,'{\"category_layout\":\"\",\"image\":\"\"}','','','{\"author\":\"\",\"robots\":\"\"}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1),(4,29,1,5,6,1,'uncategorised','com_contact','Uncategorised','uncategorised','','',1,0,'0000-00-00 00:00:00',1,'{\"category_layout\":\"\",\"image\":\"\"}','','','{\"author\":\"\",\"robots\":\"\"}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1),(5,30,1,7,8,1,'uncategorised','com_newsfeeds','Uncategorised','uncategorised','','',1,0,'0000-00-00 00:00:00',1,'{\"category_layout\":\"\",\"image\":\"\"}','','','{\"author\":\"\",\"robots\":\"\"}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1),(6,31,1,9,10,1,'uncategorised','com_weblinks','Uncategorised','uncategorised','','',1,0,'0000-00-00 00:00:00',1,'{\"category_layout\":\"\",\"image\":\"\"}','','','{\"author\":\"\",\"robots\":\"\"}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1),(7,32,1,11,12,1,'uncategorised','com_users','Uncategorised','uncategorised','','',1,0,'0000-00-00 00:00:00',1,'{\"category_layout\":\"\",\"image\":\"\"}','','','{\"author\":\"\",\"robots\":\"\"}',42,'2011-01-01 00:00:01',0,'0000-00-00 00:00:00',0,'*',1);
/*!40000 ALTER TABLE `mf0kn_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_contact_details`
--

DROP TABLE IF EXISTS `mf0kn_contact_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  `sortname1` varchar(255) NOT NULL,
  `sortname2` varchar(255) NOT NULL,
  `sortname3` varchar(255) NOT NULL,
  `language` char(7) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_contact_details`
--

LOCK TABLES `mf0kn_contact_details` WRITE;
/*!40000 ALTER TABLE `mf0kn_contact_details` DISABLE KEYS */;
INSERT INTO `mf0kn_contact_details` VALUES (1,'Gary Slac','gary-slac','Owner/Principle Broker','','','','','','503-243-4620','503-243-4621','<p>I am the owner and Principal Broker of Vantage Point Properties, Inc., a family owned business since 2002. I\'ve been an active, full-time agent for over 25 years. I am a first-time buyer specialist. After all these years, there is nothing more satisfying than helping someone find and purchase their first home. I\'ve been proudly associated with the Portland Housing Center for over 10 years.</p>\r\n<p>In addition to my real estate career, I am a licensed and bonded general contractor. My construction company is Vantage Point Renovations, LLC. I am well-versed in home restoration and renovation and have held my contractor\'s license since 2001. I received my formal education from Pomona College, where I received my BA degree in Psychology. I just recently completed a course of study in conflict mangagement and dispute resolution at Marylhurst University. I am constantly striving to improve my negotiation skills as well as my ability to interact effectively with people.</p>\r\n<p>I am a dedicated husband and father. My interests include gardening, golf and woodworking. I remain forever dedicated to the principle that everyone deserves equal access to home ownership.</p>','images/gary.jpg','gary@vppihomes.com',0,1,0,'0000-00-00 00:00:00',1,'{\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"presentation_style\":\"\",\"show_tags\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"\",\"linka_name\":\"\",\"linka\":false,\"linkb_name\":\"\",\"linkb\":false,\"linkc_name\":\"\",\"linkc\":false,\"linkd_name\":\"\",\"linkd\":false,\"linke_name\":\"\",\"linke\":\"\",\"contact_layout\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\"}',0,4,1,'503-860-3740','http://vppihomes.com','','','','*','2014-07-30 01:28:52',404,'','2014-07-30 22:52:44',404,'','','{\"robots\":\"\",\"rights\":\"\"}',1,'','0000-00-00 00:00:00','0000-00-00 00:00:00',3,13),(2,'Mary Mayther-Slac','mary-mayther-slac','President','','','','','','503-243-4620','503-243-4621','<p>Mary Mayther-Slac is a Portland native with 25+ years experience assessing, refining and achieving the real estate goals of her clients. She is the President of Vantage Point Properties, Inc. A homegrown real estate firm that has helped clients to realize their property ownership dreams since 2002. Her inherent knack for spotting the potential of diamond-in-the-rough properties, recognizing opportunities, and networking for her clients earned her the Portland Monthly magazine 5 Star Real Estate Agent award in 2011 – 2013, based on her buyers satisfaction.</p>\r\n<p>Mary is service oriented and solution driven, and very skilled in negotiating and creating opportunities for her clients to obtain maximum results with minimum anxiety. She loves assisting her sellers in maximizing their profit potential, by counseling them on making value adding improvements to their properties, crucial market timing, and creating opportunities for building wealth through investment in real estate. She has been a volunteer educator for first time home buyers at the Portland Housing Center for ten years.</p>\r\n<p>In her free time Mary loves spending time with her husband Gary and son Andrew, fund raising for non-profits, volunteering in her community, supporting friends, family and colleagues in their dreams and endeavors, gardening, and snuggling with her two rescued cats, Fiona and Alejandro.</p>','images/mary.jpg','mary@vppihomes.com',0,1,0,'0000-00-00 00:00:00',2,'{\"show_contact_category\":\"\",\"show_contact_list\":\"\",\"presentation_style\":\"\",\"show_tags\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_profile\":\"\",\"show_links\":\"\",\"linka_name\":\"\",\"linka\":false,\"linkb_name\":\"\",\"linkb\":false,\"linkc_name\":\"\",\"linkc\":false,\"linkd_name\":\"\",\"linkd\":false,\"linke_name\":\"\",\"linke\":\"\",\"contact_layout\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\"}',0,4,1,'503-998-1780','http://vppihomes.com','','','','*','2014-07-30 01:30:35',404,'','2014-07-30 22:44:36',404,'','','{\"robots\":\"\",\"rights\":\"\"}',1,'','0000-00-00 00:00:00','0000-00-00 00:00:00',3,35);
/*!40000 ALTER TABLE `mf0kn_contact_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_content`
--

DROP TABLE IF EXISTS `mf0kn_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` varchar(5120) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_content`
--

LOCK TABLES `mf0kn_content` WRITE;
/*!40000 ALTER TABLE `mf0kn_content` DISABLE KEYS */;
INSERT INTO `mf0kn_content` VALUES (1,69,'Mission Statement','mission-statement','<p>To serve the real estate needs of our clients, customers and our community, with the emphasis on cooperation, teamwork and utmost integrity. Our company has been part of the Portland real estate community since 2002. Our market areas include close-in, east- and west-sides, North Portland, Gresham, and East County neighborhoods. With a long history working with first-time buyers, we volunteer at the <a href=\"http://portlandhousingcenter.org/\" target=\"_blank\">Portland Housing Center</a>.</p>\r\n<p>In addition to our expertise with single-family homes, we are well-versed in condominium and small-income unit sales and commercial leasing. Again, we believe in working as a team with our buyers, lenders, home inspectors and escrow agents to provide complete and well-rounded service to our clients. Our goal is <em>your</em> success.</p>','',1,2,'2014-07-03 00:47:25',404,'','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00','2014-07-03 00:47:25','0000-00-00 00:00:00','{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}','{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}','{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}',1,3,'','',1,3685,'{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}',0,'*',''),(2,70,'Vantage Point Brokers','vantage-point-brokers','<div class=\"contactinfo\"><a href=\"mailto:gary@vppihomes.com\">gary@vppihomes.com</a><br />Cell: <a href=\"tel:5038603740\">503-860-3740</a><br />Office: 503-243-4620<br />Fax: 503-243-4621<br />Home: 503-663-1357</div>\r\n<div>\r\n<p><img style=\"float: left; margin-right: 15px; margin-bottom: 10px;\" src=\"images/gary.jpg\" alt=\"Gary Slac\" width=\"129\" height=\"181\" /><span style=\"font-size: x-large;\">Gary Slac</span><br /><br /> I am the owner and Principal Broker of Vantage Point Properties, Inc., a family owned business since 2002. I\'ve been an active, full-time agent for over 25 years. I am a first-time buyer specialist. After all these years, there is nothing more satisfying than helping someone find and purchase their first home. I\'ve been proudly associated with the Portland Housing Center for over 10 years.</p>\r\n<p>In addition to my real estate career, I am a licensed and bonded general contractor. My construction company is Vantage Point Renovations, LLC. I am well-versed in home restoration and renovation and have held my contractor\'s license since 2001. I received my formal education from Pomona College, where I received my BA degree in Psychology. I just recently completed a course of study in conflict mangagement and dispute resolution at Marylhurst University. I am constantly striving to improve my negotiation skills as well as my ability to interact effectively with people.</p>\r\n<p>I am a dedicated husband and father. My interests include gardening, golf and woodworking. I remain forever dedicated to the principle that everyone deserves equal access to home ownership.</p>\r\n</div>\r\n<div style=\"clear: left;\"><hr /></div>\r\n<div class=\"contactinfo\"><a href=\"mailto:mary@vppihomes.com\">mary@vppihomes.com</a><br />Cell: <a href=\"tel:5039017480\">503-998-1780</a><br />Office: 503-243-4620<br />Fax: 503-243-4621<br />Home: 503-663-1357</div>\r\n<div>\r\n<p><img style=\"float: left; margin-right: 15px; margin-bottom: 10px;\" src=\"images/mary.jpg\" alt=\"Mary Mayther-Slac\" width=\"171\" height=\"183\" /><span style=\"font-size: x-large;\">Mary Mayther-Slac</span><br /><br /> Mary Mayther-Slac is a Portland native with 25+ years experience assessing, refining and achieving the real estate goals of her clients. She is the President of Vantage Point Properties, Inc. A homegrown real estate firm that has helped clients to realize their property ownership dreams since 2002. Her inherent knack for spotting the potential of diamond-in-the-rough properties, recognizing opportunities, and networking for her clients earned her the Portland Monthly magazine 5 Star Real Estate Agent award in 2011 – 2013, based on her buyers satisfaction.</p>\r\n<p>Mary is service oriented and solution driven, and very skilled in negotiating and creating opportunities for her clients to obtain maximum results with minimum anxiety. She loves assisting her sellers in maximizing their profit potential, by counseling them on making value adding improvements to their properties, crucial market timing, and creating opportunities for building wealth through investment in real estate. She has been a volunteer educator for first time home buyers at the Portland Housing Center for ten years.</p>\r\n<p>In her free time Mary loves spending time with her husband Gary and son Andrew, fund raising for non-profits, volunteering in her community, supporting friends, family and colleagues in their dreams and endeavors, gardening, and snuggling with her two rescued cats, Fiona and Alejandro.</p>\r\n</div>','',1,2,'2014-07-03 00:49:56',404,'','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00','2014-07-03 00:49:56','0000-00-00 00:00:00','{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}','{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}','{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}',1,2,'','',1,18,'{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}',0,'*',''),(3,71,'What Every Buyer Should Know','what-every-buyer-should-know','<p>We think it\'s always a good idea to make informed decisions. We\'ve noticed over the years that there is frequently confusion about the home buying process. What follows is an overview of that process and how it works.</p>\r\n<p>The most important piece of this puzzle comes early in the game. Probably one of the most significant decisions you will make will be what Real Estate Agent you choose. Baring glaring differences in skill levels most Real Estate professionals are capable and able people. In other words, most brokers will be able to steward you through this process. We feel that the most important criterion for choosing your agent is based on feel. Does this person feel right for me? Is this person a good fit for me? Is this person a good listener? How well do our personalities match? And finally, do I trust this person? In effect, do I like this person?</p>\r\n<p>Our method for determining this fit is simple. We arrange a meeting that fits your schedule in a place where you feel comfortable. We take the time to get to know each other. The meeting takes its own course and we both decide on making the commitment to work together. We tailor our time together to fit around your work schedule and other personal commitments. We arrange our first day of house hunting.</p>\r\n<p>Our house hunting is also tailor made around your needs and style. It is something that we design together. It revolves around how we exchange information, how we remain in contact and how often. What you’ll experience is a custom built relationship.</p>\r\n<p>Our job is to make you the best buyer possible before we go out shopping, getting you pre-approved for a loan, funds available for purchase and inspection costs and criteria of your search clearly stated.</p>\r\n<p>Typically, that relationship yields exactly what you’ve envisioned in your home. It will be a reflection of your personality and a statement about who you are. When that special home is found we will sit down and craft an offer to be presented to the sellers through their broker/representative. They decide to either accept your offer, counter it with changes to some of the terms of our offer, or they can simply say that they’re not going to accept it. Let’s assume that your offer is accepted.</p>\r\n<p>What follows after writing an offer and getting it accepted is the time that the value of your agent really comes through. There are numerous tasks that much be completed in a timely manner that will determine the successful maneuvering through the transaction. Here’s the sequence of events that occur:</p>\r\n<ol>\r\n<li>All paperwork is sent to an escrow company (we’ll talk more about that later)</li>\r\n<li>We make formal loan application within a specified time period and provide the bank with a copy of the sale agreement and any additional paperwork they might need</li>\r\n<li>We start to order all of our inspections for the home. These include:<ol style=\"list-style-type: lower-alpha;\">\r\n<li>A full home and dry rot inspection</li>\r\n<li>Additional inspection noted by your home inspector (electrical, furnace, roof, mold, etc.)</li>\r\n<li>A sewer inspection</li>\r\n<li>Radon inspection (with homes with basements)</li>\r\n</ol></li>\r\n<li>After the home inspections are done we negotiate with the seller(s) to see what repairs they’re willing to do.</li>\r\n<li>When we’ve reached an agreement on them we notify the bank to proceed to the next step and order the appraisal of the property (this will tell us about the value and condition of the home (did we pay too much or did we offer the true value of the home?)</li>\r\n<li>When the appraisal comes back the bank goes into the final approval stage and you are given formal full approval to buy the home.</li>\r\n<li>All of the paperwork required to purchase the home goes to your escrow company. This is a neutral third party that processes paperwork and acts according to the terms of your sale agreement. They can only do what you and the seller have instructed them to do in writing.</li>\r\n<li>We all go to the escrow company. You bring them the funds needed to get your loan and sign all of the paperwork.</li>\r\n<li>The paperwork goes back to the bank for review and they give their approval.</li>\r\n<li>The escrow company is told about the approval, they get the deed to the county for recording, and the home is yours! You can usually move in by 5:00 the day the deed is recorded.</li>\r\n</ol>','',1,2,'2014-07-03 00:51:56',404,'','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00','2014-07-03 00:51:56','0000-00-00 00:00:00','{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}','{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}','{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}',1,1,'','',1,2942,'{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}',0,'*',''),(4,78,'Contact Us','contact-us','<p style=\"text-align: center;\">1410 SW 11th Ave #606<br />Portland, Oregon 97201-3342</p>\r\n<p style=\"text-align: center;\">(503) 243-4620<br />Fax: (503) 243-4621<br />Gary Cell: (503) 860-3740<br />Mary Cell: (503) 998-1780<br />Crispin Cell: (503) 888-3656</p>\r\n<p style=\"text-align: center;\"><a href=\"mailto:info@vppihomes.com?subject=Inquiry%20From%20Website\">info@vppihomes.com</a></p>\r\n<p>{loadposition contactform}</p>','',1,2,'2014-07-03 20:23:39',404,'','2014-07-04 01:12:43',404,0,'0000-00-00 00:00:00','2014-07-03 20:23:39','0000-00-00 00:00:00','{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}','{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}','{\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_layout\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}',3,0,'','',1,121,'{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}',0,'*','');
/*!40000 ALTER TABLE `mf0kn_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_content_frontpage`
--

DROP TABLE IF EXISTS `mf0kn_content_frontpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_content_frontpage`
--

LOCK TABLES `mf0kn_content_frontpage` WRITE;
/*!40000 ALTER TABLE `mf0kn_content_frontpage` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_content_frontpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_content_rating`
--

DROP TABLE IF EXISTS `mf0kn_content_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_content_rating`
--

LOCK TABLES `mf0kn_content_rating` WRITE;
/*!40000 ALTER TABLE `mf0kn_content_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_content_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_content_types`
--

DROP TABLE IF EXISTS `mf0kn_content_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_content_types` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_title` varchar(255) NOT NULL DEFAULT '',
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `table` varchar(255) NOT NULL DEFAULT '',
  `rules` text NOT NULL,
  `field_mappings` text NOT NULL,
  `router` varchar(255) NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) DEFAULT NULL COMMENT 'JSON string for com_contenthistory options',
  PRIMARY KEY (`type_id`),
  KEY `idx_alias` (`type_alias`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_content_types`
--

LOCK TABLES `mf0kn_content_types` WRITE;
/*!40000 ALTER TABLE `mf0kn_content_types` DISABLE KEYS */;
INSERT INTO `mf0kn_content_types` VALUES (1,'Article','com_content.article','{\"special\":{\"dbtable\":\"#__content\",\"key\":\"id\",\"type\":\"Content\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"introtext\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"attribs\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"asset_id\"}, \"special\":{\"fulltext\":\"fulltext\"}}','ContentHelperRoute::getArticleRoute','{\"formFile\":\"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),(2,'Weblink','com_weblinks.weblink','{\"special\":{\"dbtable\":\"#__weblinks\",\"key\":\"id\",\"type\":\"Weblink\",\"prefix\":\"WeblinksTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{}}','WeblinksHelperRoute::getWeblinkRoute','{\"formFile\":\"administrator\\/components\\/com_weblinks\\/models\\/forms\\/weblink.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"featured\",\"images\"], \"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),(3,'Contact','com_contact.contact','{\"special\":{\"dbtable\":\"#__contact_details\",\"key\":\"id\",\"type\":\"Contact\",\"prefix\":\"ContactTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"address\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"image\", \"core_urls\":\"webpage\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"con_position\":\"con_position\",\"suburb\":\"suburb\",\"state\":\"state\",\"country\":\"country\",\"postcode\":\"postcode\",\"telephone\":\"telephone\",\"fax\":\"fax\",\"misc\":\"misc\",\"email_to\":\"email_to\",\"default_con\":\"default_con\",\"user_id\":\"user_id\",\"mobile\":\"mobile\",\"sortname1\":\"sortname1\",\"sortname2\":\"sortname2\",\"sortname3\":\"sortname3\"}}','ContactHelperRoute::getContactRoute','{\"formFile\":\"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml\",\"hideFields\":[\"default_con\",\"checked_out\",\"checked_out_time\",\"version\",\"xreference\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[ {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ] }'),(4,'Newsfeed','com_newsfeeds.newsfeed','{\"special\":{\"dbtable\":\"#__newsfeeds\",\"key\":\"id\",\"type\":\"Newsfeed\",\"prefix\":\"NewsfeedsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"numarticles\":\"numarticles\",\"cache_time\":\"cache_time\",\"rtl\":\"rtl\"}}','NewsfeedsHelperRoute::getNewsfeedRoute','{\"formFile\":\"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),(5,'User','com_users.user','{\"special\":{\"dbtable\":\"#__users\",\"key\":\"id\",\"type\":\"User\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"username\",\"core_created_time\":\"registerdate\",\"core_modified_time\":\"lastvisitDate\",\"core_body\":\"null\", \"core_hits\":\"null\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"access\":\"null\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"null\", \"core_language\":\"null\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"null\", \"core_ordering\":\"null\", \"core_metakey\":\"null\", \"core_metadesc\":\"null\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{}}','UsersHelperRoute::getUserRoute',''),(6,'Article Category','com_content.category','{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}','ContentHelperRoute::getCategoryRoute','{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),(7,'Contact Category','com_contact.category','{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}','ContactHelperRoute::getCategoryRoute','{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),(8,'Newsfeeds Category','com_newsfeeds.category','{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}','NewsfeedsHelperRoute::getCategoryRoute','{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),(9,'Weblinks Category','com_weblinks.category','{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}','WeblinksHelperRoute::getCategoryRoute','{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),(10,'Tag','com_tags.tag','{\"special\":{\"dbtable\":\"#__tags\",\"key\":\"tag_id\",\"type\":\"Tag\",\"prefix\":\"TagsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\"}}','TagsHelperRoute::getTagRoute','{\"formFile\":\"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"lft\", \"rgt\", \"level\", \"path\", \"urls\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),(11,'Banner','com_banners.banner','{\"special\":{\"dbtable\":\"#__banners\",\"key\":\"id\",\"type\":\"Banner\",\"prefix\":\"BannersTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"null\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"imptotal\":\"imptotal\", \"impmade\":\"impmade\", \"clicks\":\"clicks\", \"clickurl\":\"clickurl\", \"custombannercode\":\"custombannercode\", \"cid\":\"cid\", \"purchase_type\":\"purchase_type\", \"track_impressions\":\"track_impressions\", \"track_clicks\":\"track_clicks\"}}','','{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"reset\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"imptotal\", \"impmade\", \"reset\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"cid\",\"targetTable\":\"#__banner_clients\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),(12,'Banners Category','com_banners.category','{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}','','{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),(13,'Banner Client','com_banners.client','{\"special\":{\"dbtable\":\"#__banner_clients\",\"key\":\"id\",\"type\":\"Client\",\"prefix\":\"BannersTable\"}}','','','','{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\"], \"ignoreChanges\":[\"checked_out\", \"checked_out_time\"], \"convertToInt\":[], \"displayLookup\":[]}'),(14,'User Notes','com_users.note','{\"special\":{\"dbtable\":\"#__user_notes\",\"key\":\"id\",\"type\":\"Note\",\"prefix\":\"UsersTable\"}}','','','','{\"formFile\":\"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\"], \"convertToInt\":[\"publish_up\", \"publish_down\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),(15,'User Notes Category','com_users.category','{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}','','{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}','','{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}');
/*!40000 ALTER TABLE `mf0kn_content_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_contentitem_tag_map`
--

DROP TABLE IF EXISTS `mf0kn_contentitem_tag_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_contentitem_tag_map` (
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `core_content_id` int(10) unsigned NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) unsigned NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table',
  UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  KEY `idx_tag_type` (`tag_id`,`type_id`),
  KEY `idx_date_id` (`tag_date`,`tag_id`),
  KEY `idx_tag` (`tag_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_core_content_id` (`core_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maps items from content tables to tags';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_contentitem_tag_map`
--

LOCK TABLES `mf0kn_contentitem_tag_map` WRITE;
/*!40000 ALTER TABLE `mf0kn_contentitem_tag_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_contentitem_tag_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_core_log_searches`
--

DROP TABLE IF EXISTS `mf0kn_core_log_searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_core_log_searches`
--

LOCK TABLES `mf0kn_core_log_searches` WRITE;
/*!40000 ALTER TABLE `mf0kn_core_log_searches` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_core_log_searches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_extensions`
--

DROP TABLE IF EXISTS `mf0kn_extensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_extensions` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `element` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) unsigned NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text NOT NULL,
  `params` text NOT NULL,
  `custom_data` text NOT NULL,
  `system_data` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`extension_id`),
  KEY `element_clientid` (`element`,`client_id`),
  KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  KEY `extension` (`type`,`element`,`folder`,`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10061 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_extensions`
--

LOCK TABLES `mf0kn_extensions` WRITE;
/*!40000 ALTER TABLE `mf0kn_extensions` DISABLE KEYS */;
INSERT INTO `mf0kn_extensions` VALUES (1,'com_mailto','component','com_mailto','',0,1,1,1,'{\"name\":\"com_mailto\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MAILTO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mailto\"}','','','',0,'0000-00-00 00:00:00',0,0),(2,'com_wrapper','component','com_wrapper','',0,1,1,1,'{\"name\":\"com_wrapper\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"wrapper\"}','','','',0,'0000-00-00 00:00:00',0,0),(3,'com_admin','component','com_admin','',1,1,1,1,'{\"name\":\"com_admin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_ADMIN_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(4,'com_banners','component','com_banners','',1,1,1,0,'{\"name\":\"com_banners\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"banners\"}','{\"purchase_type\":\"3\",\"track_impressions\":\"0\",\"track_clicks\":\"0\",\"metakey_prefix\":\"\",\"save_history\":\"1\",\"history_limit\":10}','','',0,'0000-00-00 00:00:00',0,0),(5,'com_cache','component','com_cache','',1,1,1,1,'{\"name\":\"com_cache\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CACHE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(6,'com_categories','component','com_categories','',1,1,1,1,'{\"name\":\"com_categories\",\"type\":\"component\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(7,'com_checkin','component','com_checkin','',1,1,1,1,'{\"name\":\"com_checkin\",\"type\":\"component\",\"creationDate\":\"Unknown\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CHECKIN_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(8,'com_contact','component','com_contact','',1,1,1,0,'{\"name\":\"com_contact\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}','{\"contact_layout\":\"_:default\",\"show_contact_category\":\"hide\",\"save_history\":\"1\",\"history_limit\":10,\"show_contact_list\":\"0\",\"presentation_style\":\"sliders\",\"show_name\":\"1\",\"show_position\":\"1\",\"show_email\":\"1\",\"show_street_address\":\"0\",\"show_suburb\":\"0\",\"show_state\":\"0\",\"show_postcode\":\"0\",\"show_country\":\"0\",\"show_telephone\":\"1\",\"show_mobile\":\"1\",\"show_fax\":\"1\",\"show_webpage\":\"1\",\"show_misc\":\"1\",\"show_image\":\"1\",\"image\":\"\",\"allow_vcard\":\"0\",\"show_articles\":\"0\",\"show_profile\":\"0\",\"show_links\":\"0\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"show_tags\":\"1\",\"contact_icons\":\"0\",\"icon_address\":\"\",\"icon_email\":\"\",\"icon_telephone\":\"\",\"icon_mobile\":\"\",\"icon_fax\":\"\",\"icon_misc\":\"\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"0\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"0\",\"show_subcat_desc\":\"1\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_headings\":\"1\",\"show_position_headings\":\"1\",\"show_email_headings\":\"1\",\"show_telephone_headings\":\"1\",\"show_mobile_headings\":\"1\",\"show_fax_headings\":\"1\",\"show_suburb_headings\":\"1\",\"show_state_headings\":\"1\",\"show_country_headings\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"initial_sort\":\"ordering\",\"captcha\":\"\",\"show_email_form\":\"1\",\"show_email_copy\":\"1\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"1\",\"custom_reply\":\"0\",\"redirect\":\"\",\"show_feed_link\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(9,'com_cpanel','component','com_cpanel','',1,1,1,1,'{\"name\":\"com_cpanel\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CPANEL_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(10,'com_installer','component','com_installer','',1,1,1,1,'{\"name\":\"com_installer\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_INSTALLER_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(11,'com_languages','component','com_languages','',1,1,1,1,'{\"name\":\"com_languages\",\"type\":\"component\",\"creationDate\":\"2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\"}','{\"administrator\":\"en-GB\",\"site\":\"en-GB\"}','','',0,'0000-00-00 00:00:00',0,0),(12,'com_login','component','com_login','',1,1,1,1,'{\"name\":\"com_login\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(13,'com_media','component','com_media','',1,1,0,1,'{\"name\":\"com_media\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}','{\"upload_extensions\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\",\"upload_maxsize\":\"10\",\"file_path\":\"images\",\"image_path\":\"images\",\"restrict_uploads\":\"1\",\"allowed_media_usergroup\":\"3\",\"check_mime\":\"1\",\"image_extensions\":\"bmp,gif,jpg,png\",\"ignore_extensions\":\"\",\"upload_mime\":\"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip\",\"upload_mime_illegal\":\"text\\/html\"}','','',0,'0000-00-00 00:00:00',0,0),(14,'com_menus','component','com_menus','',1,1,1,1,'{\"name\":\"com_menus\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MENUS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(15,'com_messages','component','com_messages','',1,1,1,1,'{\"name\":\"com_messages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MESSAGES_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(16,'com_modules','component','com_modules','',1,1,1,1,'{\"name\":\"com_modules\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MODULES_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(17,'com_newsfeeds','component','com_newsfeeds','',1,1,1,0,'{\"name\":\"com_newsfeeds\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}','{\"newsfeed_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_feed_image\":\"1\",\"show_feed_description\":\"1\",\"show_item_description\":\"1\",\"feed_character_count\":\"0\",\"feed_display_order\":\"des\",\"float_first\":\"right\",\"float_second\":\"right\",\"show_tags\":\"1\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"1\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"0\",\"show_subcat_desc\":\"1\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_headings\":\"1\",\"show_articles\":\"0\",\"show_link\":\"1\",\"show_pagination\":\"1\",\"show_pagination_results\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(18,'com_plugins','component','com_plugins','',1,1,1,1,'{\"name\":\"com_plugins\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_PLUGINS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(19,'com_search','component','com_search','',1,1,1,0,'{\"name\":\"com_search\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"search\"}','{\"enabled\":\"0\",\"show_date\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(20,'com_templates','component','com_templates','',1,1,1,1,'{\"name\":\"com_templates\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_TEMPLATES_XML_DESCRIPTION\",\"group\":\"\"}','{\"template_positions_display\":\"0\",\"upload_limit\":\"2\",\"image_formats\":\"gif,bmp,jpg,jpeg,png\",\"source_formats\":\"txt,less,ini,xml,js,php,css\",\"font_formats\":\"woff,ttf,otf\",\"compressed_formats\":\"zip\"}','','',0,'0000-00-00 00:00:00',0,0),(21,'com_weblinks','component','com_weblinks','',1,1,1,0,'{\"name\":\"com_weblinks\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\"}','{\"target\":\"0\",\"save_history\":\"1\",\"history_limit\":5,\"count_clicks\":\"1\",\"icons\":1,\"link_icons\":\"\",\"float_first\":\"right\",\"float_second\":\"right\",\"show_tags\":\"1\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"1\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"0\",\"show_subcat_desc\":\"1\",\"show_cat_num_links\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_num_links_cat\":\"1\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_headings\":\"0\",\"show_link_description\":\"1\",\"show_link_hits\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_feed_link\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(22,'com_content','component','com_content','',1,1,0,1,'{\"name\":\"com_content\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}','{\"article_layout\":\"_:default\",\"show_title\":\"1\",\"link_titles\":\"1\",\"show_intro\":\"1\",\"info_block_position\":\"0\",\"show_category\":\"0\",\"link_category\":\"1\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_author\":\"0\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"0\",\"show_readmore\":\"1\",\"show_readmore_title\":\"1\",\"readmore_limit\":\"100\",\"show_tags\":\"1\",\"show_icons\":\"0\",\"show_print_icon\":\"0\",\"show_email_icon\":\"0\",\"show_hits\":\"0\",\"show_noauth\":\"0\",\"urls_position\":\"0\",\"show_publishing_options\":\"1\",\"show_article_options\":\"1\",\"save_history\":\"1\",\"history_limit\":10,\"show_urls_images_frontend\":\"0\",\"show_urls_images_backend\":\"1\",\"targeta\":0,\"targetb\":0,\"targetc\":0,\"float_intro\":\"left\",\"float_fulltext\":\"left\",\"category_layout\":\"_:blog\",\"show_category_heading_title_text\":\"1\",\"show_category_title\":\"0\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"1\",\"show_empty_categories\":\"0\",\"show_no_articles\":\"1\",\"show_subcat_desc\":\"1\",\"show_cat_num_articles\":\"0\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_num_articles_cat\":\"1\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"0\",\"show_subcategory_content\":\"0\",\"show_pagination_limit\":\"1\",\"filter_field\":\"hide\",\"show_headings\":\"1\",\"list_show_date\":\"0\",\"date_format\":\"\",\"list_show_hits\":\"1\",\"list_show_author\":\"1\",\"orderby_pri\":\"order\",\"orderby_sec\":\"rdate\",\"order_date\":\"published\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\",\"feed_show_readmore\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),(23,'com_config','component','com_config','',1,1,0,1,'{\"name\":\"com_config\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONFIG_XML_DESCRIPTION\",\"group\":\"\"}','{\"filters\":{\"1\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"9\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"6\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"7\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"2\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"3\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"4\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"5\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"8\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"}}}','','',0,'0000-00-00 00:00:00',0,0),(24,'com_redirect','component','com_redirect','',1,1,0,1,'{\"name\":\"com_redirect\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(25,'com_users','component','com_users','',1,1,0,1,'{\"name\":\"com_users\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_USERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"users\"}','{\"allowUserRegistration\":\"0\",\"new_usertype\":\"2\",\"guest_usergroup\":\"9\",\"sendpassword\":\"1\",\"useractivation\":\"1\",\"mail_to_admin\":\"0\",\"captcha\":\"\",\"frontend_userparams\":\"1\",\"site_language\":\"0\",\"change_login_name\":\"0\",\"reset_count\":\"10\",\"reset_time\":\"1\",\"minimum_length\":\"4\",\"minimum_integers\":\"0\",\"minimum_symbols\":\"0\",\"minimum_uppercase\":\"0\",\"save_history\":\"1\",\"history_limit\":5,\"mailSubjectPrefix\":\"\",\"mailBodySuffix\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),(27,'com_finder','component','com_finder','',1,1,0,0,'{\"name\":\"com_finder\",\"type\":\"component\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_FINDER_XML_DESCRIPTION\",\"group\":\"\"}','{\"show_description\":\"1\",\"description_length\":255,\"allow_empty_query\":\"0\",\"show_url\":\"1\",\"show_advanced\":\"1\",\"expand_advanced\":\"0\",\"show_date_filters\":\"0\",\"highlight_terms\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\",\"batch_size\":\"50\",\"memory_table_limit\":30000,\"title_multiplier\":\"1.7\",\"text_multiplier\":\"0.7\",\"meta_multiplier\":\"1.2\",\"path_multiplier\":\"2.0\",\"misc_multiplier\":\"0.3\",\"stemmer\":\"snowball\"}','','',0,'0000-00-00 00:00:00',0,0),(28,'com_joomlaupdate','component','com_joomlaupdate','',1,1,0,1,'{\"name\":\"com_joomlaupdate\",\"type\":\"component\",\"creationDate\":\"February 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2014 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(29,'com_tags','component','com_tags','',1,1,1,1,'{\"name\":\"com_tags\",\"type\":\"component\",\"creationDate\":\"December 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"COM_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}','{\"tag_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_tag_title\":\"0\",\"tag_list_show_tag_image\":\"0\",\"tag_list_show_tag_description\":\"0\",\"tag_list_image\":\"\",\"show_tag_num_items\":\"0\",\"tag_list_orderby\":\"title\",\"tag_list_orderby_direction\":\"ASC\",\"show_headings\":\"0\",\"tag_list_show_date\":\"0\",\"tag_list_show_item_image\":\"0\",\"tag_list_show_item_description\":\"0\",\"tag_list_item_maximum_characters\":0,\"return_any_or_all\":\"1\",\"include_children\":\"0\",\"maximum\":200,\"tag_list_language_filter\":\"all\",\"tags_layout\":\"_:default\",\"all_tags_orderby\":\"title\",\"all_tags_orderby_direction\":\"ASC\",\"all_tags_show_tag_image\":\"0\",\"all_tags_show_tag_descripion\":\"0\",\"all_tags_tag_maximum_characters\":20,\"all_tags_show_tag_hits\":\"0\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"tag_field_ajax_mode\":\"1\",\"show_feed_link\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(30,'com_contenthistory','component','com_contenthistory','',1,1,1,0,'{\"name\":\"com_contenthistory\",\"type\":\"component\",\"creationDate\":\"May 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_CONTENTHISTORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contenthistory\"}','','','',0,'0000-00-00 00:00:00',0,0),(31,'com_ajax','component','com_ajax','',1,1,1,0,'{\"name\":\"com_ajax\",\"type\":\"component\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_AJAX_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ajax\"}','','','',0,'0000-00-00 00:00:00',0,0),(32,'com_postinstall','component','com_postinstall','',1,1,1,1,'{\"name\":\"com_postinstall\",\"type\":\"component\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_POSTINSTALL_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(101,'SimplePie','library','simplepie','',0,1,1,1,'{\"name\":\"SimplePie\",\"type\":\"library\",\"creationDate\":\"2004\",\"author\":\"SimplePie\",\"copyright\":\"Copyright (c) 2004-2009, Ryan Parman and Geoffrey Sneddon\",\"authorEmail\":\"\",\"authorUrl\":\"http:\\/\\/simplepie.org\\/\",\"version\":\"1.2\",\"description\":\"LIB_SIMPLEPIE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"simplepie\"}','','','',0,'0000-00-00 00:00:00',0,0),(102,'phputf8','library','phputf8','',0,1,1,1,'{\"name\":\"phputf8\",\"type\":\"library\",\"creationDate\":\"2006\",\"author\":\"Harry Fuecks\",\"copyright\":\"Copyright various authors\",\"authorEmail\":\"hfuecks@gmail.com\",\"authorUrl\":\"http:\\/\\/sourceforge.net\\/projects\\/phputf8\",\"version\":\"0.5\",\"description\":\"LIB_PHPUTF8_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phputf8\"}','','','',0,'0000-00-00 00:00:00',0,0),(103,'Joomla! Platform','library','joomla','',0,1,1,1,'{\"name\":\"Joomla! Platform\",\"type\":\"library\",\"creationDate\":\"2008\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"http:\\/\\/www.joomla.org\",\"version\":\"13.1\",\"description\":\"LIB_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}','{\"mediaversion\":\"d27d8ea554c20d8b5100b0f16eff232a\"}','','',0,'0000-00-00 00:00:00',0,0),(104,'IDNA Convert','library','idna_convert','',0,1,1,1,'{\"name\":\"IDNA Convert\",\"type\":\"library\",\"creationDate\":\"2004\",\"author\":\"phlyLabs\",\"copyright\":\"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de\",\"authorEmail\":\"phlymail@phlylabs.de\",\"authorUrl\":\"http:\\/\\/phlylabs.de\",\"version\":\"0.8.0\",\"description\":\"LIB_IDNA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"idna_convert\"}','','','',0,'0000-00-00 00:00:00',0,0),(105,'FOF','library','fof','',0,1,1,1,'{\"name\":\"FOF\",\"type\":\"library\",\"creationDate\":\"2015-04-22 13:15:32\",\"author\":\"Nicholas K. Dionysopoulos \\/ Akeeba Ltd\",\"copyright\":\"(C)2011-2015 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"https:\\/\\/www.akeebabackup.com\",\"version\":\"2.4.3\",\"description\":\"LIB_FOF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fof\"}','','','',0,'0000-00-00 00:00:00',0,0),(106,'PHPass','library','phpass','',0,1,1,1,'{\"name\":\"PHPass\",\"type\":\"library\",\"creationDate\":\"2004-2006\",\"author\":\"Solar Designer\",\"copyright\":\"\",\"authorEmail\":\"solar@openwall.com\",\"authorUrl\":\"http:\\/\\/www.openwall.com\\/phpass\\/\",\"version\":\"0.3\",\"description\":\"LIB_PHPASS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpass\"}','','','',0,'0000-00-00 00:00:00',0,0),(200,'mod_articles_archive','module','mod_articles_archive','',0,1,1,0,'{\"name\":\"mod_articles_archive\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_archive\"}','','','',0,'0000-00-00 00:00:00',0,0),(201,'mod_articles_latest','module','mod_articles_latest','',0,1,1,0,'{\"name\":\"mod_articles_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_latest\"}','','','',0,'0000-00-00 00:00:00',0,0),(202,'mod_articles_popular','module','mod_articles_popular','',0,1,1,0,'{\"name\":\"mod_articles_popular\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_popular\"}','','','',0,'0000-00-00 00:00:00',0,0),(203,'mod_banners','module','mod_banners','',0,1,1,0,'{\"name\":\"mod_banners\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_banners\"}','','','',0,'0000-00-00 00:00:00',0,0),(204,'mod_breadcrumbs','module','mod_breadcrumbs','',0,1,1,1,'{\"name\":\"mod_breadcrumbs\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BREADCRUMBS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_breadcrumbs\"}','','','',0,'0000-00-00 00:00:00',0,0),(205,'mod_custom','module','mod_custom','',0,1,1,1,'{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}','','','',0,'0000-00-00 00:00:00',0,0),(206,'mod_feed','module','mod_feed','',0,1,1,0,'{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}','','','',0,'0000-00-00 00:00:00',0,0),(207,'mod_footer','module','mod_footer','',0,1,1,0,'{\"name\":\"mod_footer\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FOOTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_footer\"}','','','',0,'0000-00-00 00:00:00',0,0),(208,'mod_login','module','mod_login','',0,1,1,1,'{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}','','','',0,'0000-00-00 00:00:00',0,0),(209,'mod_menu','module','mod_menu','',0,1,1,1,'{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}','','','',0,'0000-00-00 00:00:00',0,0),(210,'mod_articles_news','module','mod_articles_news','',0,1,1,0,'{\"name\":\"mod_articles_news\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_news\"}','','','',0,'0000-00-00 00:00:00',0,0),(211,'mod_random_image','module','mod_random_image','',0,1,1,0,'{\"name\":\"mod_random_image\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RANDOM_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_random_image\"}','','','',0,'0000-00-00 00:00:00',0,0),(212,'mod_related_items','module','mod_related_items','',0,1,1,0,'{\"name\":\"mod_related_items\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RELATED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_related_items\"}','','','',0,'0000-00-00 00:00:00',0,0),(213,'mod_search','module','mod_search','',0,1,1,0,'{\"name\":\"mod_search\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_search\"}','','','',0,'0000-00-00 00:00:00',0,0),(214,'mod_stats','module','mod_stats','',0,1,1,0,'{\"name\":\"mod_stats\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats\"}','','','',0,'0000-00-00 00:00:00',0,0),(215,'mod_syndicate','module','mod_syndicate','',0,1,1,1,'{\"name\":\"mod_syndicate\",\"type\":\"module\",\"creationDate\":\"May 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SYNDICATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_syndicate\"}','','','',0,'0000-00-00 00:00:00',0,0),(216,'mod_users_latest','module','mod_users_latest','',0,1,1,0,'{\"name\":\"mod_users_latest\",\"type\":\"module\",\"creationDate\":\"December 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_USERS_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_users_latest\"}','','','',0,'0000-00-00 00:00:00',0,0),(217,'mod_weblinks','module','mod_weblinks','',0,1,1,0,'{\"name\":\"mod_weblinks\",\"type\":\"module\",\"creationDate\":\"July 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(218,'mod_whosonline','module','mod_whosonline','',0,1,1,0,'{\"name\":\"mod_whosonline\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WHOSONLINE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_whosonline\"}','','','',0,'0000-00-00 00:00:00',0,0),(219,'mod_wrapper','module','mod_wrapper','',0,1,1,0,'{\"name\":\"mod_wrapper\",\"type\":\"module\",\"creationDate\":\"October 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_wrapper\"}','','','',0,'0000-00-00 00:00:00',0,0),(220,'mod_articles_category','module','mod_articles_category','',0,1,1,0,'{\"name\":\"mod_articles_category\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_category\"}','','','',0,'0000-00-00 00:00:00',0,0),(221,'mod_articles_categories','module','mod_articles_categories','',0,1,1,0,'{\"name\":\"mod_articles_categories\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_categories\"}','','','',0,'0000-00-00 00:00:00',0,0),(222,'mod_languages','module','mod_languages','',0,1,1,1,'{\"name\":\"mod_languages\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_languages\"}','','','',0,'0000-00-00 00:00:00',0,0),(223,'mod_finder','module','mod_finder','',0,1,0,0,'{\"name\":\"mod_finder\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FINDER_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(300,'mod_custom','module','mod_custom','',1,1,1,1,'{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}','','','',0,'0000-00-00 00:00:00',0,0),(301,'mod_feed','module','mod_feed','',1,1,1,0,'{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}','','','',0,'0000-00-00 00:00:00',0,0),(302,'mod_latest','module','mod_latest','',1,1,1,0,'{\"name\":\"mod_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_latest\"}','','','',0,'0000-00-00 00:00:00',0,0),(303,'mod_logged','module','mod_logged','',1,1,1,0,'{\"name\":\"mod_logged\",\"type\":\"module\",\"creationDate\":\"January 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGGED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_logged\"}','','','',0,'0000-00-00 00:00:00',0,0),(304,'mod_login','module','mod_login','',1,1,1,1,'{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"March 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}','','','',0,'0000-00-00 00:00:00',0,0),(305,'mod_menu','module','mod_menu','',1,1,1,0,'{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}','','','',0,'0000-00-00 00:00:00',0,0),(307,'mod_popular','module','mod_popular','',1,1,1,0,'{\"name\":\"mod_popular\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_popular\"}','','','',0,'0000-00-00 00:00:00',0,0),(308,'mod_quickicon','module','mod_quickicon','',1,1,1,1,'{\"name\":\"mod_quickicon\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_QUICKICON_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_quickicon\"}','','','',0,'0000-00-00 00:00:00',0,0),(309,'mod_status','module','mod_status','',1,1,1,0,'{\"name\":\"mod_status\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_status\"}','','','',0,'0000-00-00 00:00:00',0,0),(310,'mod_submenu','module','mod_submenu','',1,1,1,0,'{\"name\":\"mod_submenu\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SUBMENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_submenu\"}','','','',0,'0000-00-00 00:00:00',0,0),(311,'mod_title','module','mod_title','',1,1,1,0,'{\"name\":\"mod_title\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TITLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_title\"}','','','',0,'0000-00-00 00:00:00',0,0),(312,'mod_toolbar','module','mod_toolbar','',1,1,1,1,'{\"name\":\"mod_toolbar\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TOOLBAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_toolbar\"}','','','',0,'0000-00-00 00:00:00',0,0),(313,'mod_multilangstatus','module','mod_multilangstatus','',1,1,1,0,'{\"name\":\"mod_multilangstatus\",\"type\":\"module\",\"creationDate\":\"September 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MULTILANGSTATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_multilangstatus\"}','{\"cache\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),(314,'mod_version','module','mod_version','',1,1,1,0,'{\"name\":\"mod_version\",\"type\":\"module\",\"creationDate\":\"January 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_VERSION_XML_DESCRIPTION\",\"group\":\"\"}','{\"format\":\"short\",\"product\":\"1\",\"cache\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),(315,'mod_stats_admin','module','mod_stats_admin','',1,1,1,0,'{\"name\":\"mod_stats_admin\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats_admin\"}','{\"serverinfo\":\"0\",\"siteinfo\":\"0\",\"counter\":\"0\",\"increase\":\"0\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}','','',0,'0000-00-00 00:00:00',0,0),(316,'mod_tags_popular','module','mod_tags_popular','',0,1,1,0,'{\"name\":\"mod_tags_popular\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_popular\"}','{\"maximum\":\"5\",\"timeframe\":\"alltime\",\"owncache\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(317,'mod_tags_similar','module','mod_tags_similar','',0,1,1,0,'{\"name\":\"mod_tags_similar\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_SIMILAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_similar\"}','{\"maximum\":\"5\",\"matchtype\":\"any\",\"owncache\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(400,'plg_authentication_gmail','plugin','gmail','authentication',0,0,1,0,'{\"name\":\"plg_authentication_gmail\",\"type\":\"plugin\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_GMAIL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"gmail\"}','{\"applysuffix\":\"0\",\"suffix\":\"\",\"verifypeer\":\"1\",\"user_blacklist\":\"\"}','','',0,'0000-00-00 00:00:00',1,0),(401,'plg_authentication_joomla','plugin','joomla','authentication',0,1,1,1,'{\"name\":\"plg_authentication_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}','','','',0,'0000-00-00 00:00:00',0,0),(402,'plg_authentication_ldap','plugin','ldap','authentication',0,0,1,0,'{\"name\":\"plg_authentication_ldap\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LDAP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ldap\"}','{\"host\":\"\",\"port\":\"389\",\"use_ldapV3\":\"0\",\"negotiate_tls\":\"0\",\"no_referrals\":\"0\",\"auth_method\":\"bind\",\"base_dn\":\"\",\"search_string\":\"\",\"users_dn\":\"\",\"username\":\"admin\",\"password\":\"bobby7\",\"ldap_fullname\":\"fullName\",\"ldap_email\":\"mail\",\"ldap_uid\":\"uid\"}','','',0,'0000-00-00 00:00:00',3,0),(403,'plg_content_contact','plugin','contact','content',0,1,1,0,'{\"name\":\"plg_content_contact\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.2\",\"description\":\"PLG_CONTENT_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}','','','',0,'0000-00-00 00:00:00',1,0),(404,'plg_content_emailcloak','plugin','emailcloak','content',0,1,1,0,'{\"name\":\"plg_content_emailcloak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"emailcloak\"}','{\"mode\":\"1\"}','','',0,'0000-00-00 00:00:00',1,0),(406,'plg_content_loadmodule','plugin','loadmodule','content',0,1,1,0,'{\"name\":\"plg_content_loadmodule\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOADMODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"loadmodule\"}','{\"style\":\"xhtml\"}','','',0,'2011-09-18 15:22:50',0,0),(407,'plg_content_pagebreak','plugin','pagebreak','content',0,1,1,0,'{\"name\":\"plg_content_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}','{\"title\":\"1\",\"multipage_toc\":\"1\",\"showall\":\"1\"}','','',0,'0000-00-00 00:00:00',4,0),(408,'plg_content_pagenavigation','plugin','pagenavigation','content',0,1,1,0,'{\"name\":\"plg_content_pagenavigation\",\"type\":\"plugin\",\"creationDate\":\"January 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_PAGENAVIGATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagenavigation\"}','{\"position\":\"1\"}','','',0,'0000-00-00 00:00:00',5,0),(409,'plg_content_vote','plugin','vote','content',0,1,1,0,'{\"name\":\"plg_content_vote\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_VOTE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"vote\"}','','','',0,'0000-00-00 00:00:00',6,0),(410,'plg_editors_codemirror','plugin','codemirror','editors',0,1,1,1,'{\"name\":\"plg_editors_codemirror\",\"type\":\"plugin\",\"creationDate\":\"28 March 2011\",\"author\":\"Marijn Haverbeke\",\"copyright\":\"Copyright (C) 2014 by Marijn Haverbeke <marijnh@gmail.com> and others\",\"authorEmail\":\"marijnh@gmail.com\",\"authorUrl\":\"http:\\/\\/codemirror.net\\/\",\"version\":\"5.3\",\"description\":\"PLG_CODEMIRROR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"codemirror\"}','{\"lineNumbers\":\"1\",\"lineWrapping\":\"1\",\"matchTags\":\"1\",\"matchBrackets\":\"1\",\"marker-gutter\":\"1\",\"autoCloseTags\":\"1\",\"autoCloseBrackets\":\"1\",\"autoFocus\":\"1\",\"theme\":\"default\",\"tabmode\":\"indent\"}','','',0,'0000-00-00 00:00:00',1,0),(411,'plg_editors_none','plugin','none','editors',0,1,1,1,'{\"name\":\"plg_editors_none\",\"type\":\"plugin\",\"creationDate\":\"September 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_NONE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"none\"}','','','',0,'0000-00-00 00:00:00',2,0),(412,'plg_editors_tinymce','plugin','tinymce','editors',0,1,1,0,'{\"name\":\"plg_editors_tinymce\",\"type\":\"plugin\",\"creationDate\":\"2005-2014\",\"author\":\"Moxiecode Systems AB\",\"copyright\":\"Moxiecode Systems AB\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"tinymce.moxiecode.com\",\"version\":\"4.1.7\",\"description\":\"PLG_TINY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tinymce\"}','{\"mode\":\"1\",\"skin\":\"0\",\"mobile\":\"0\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"extended_elements\":\"\",\"html_height\":\"550\",\"html_width\":\"750\",\"resizing\":\"1\",\"element_path\":\"1\",\"fonts\":\"1\",\"paste\":\"1\",\"searchreplace\":\"1\",\"insertdate\":\"1\",\"colors\":\"1\",\"table\":\"1\",\"smilies\":\"1\",\"hr\":\"1\",\"link\":\"1\",\"media\":\"1\",\"print\":\"1\",\"directionality\":\"1\",\"fullscreen\":\"1\",\"alignment\":\"1\",\"visualchars\":\"1\",\"visualblocks\":\"1\",\"nonbreaking\":\"1\",\"template\":\"1\",\"blockquote\":\"1\",\"wordcount\":\"1\",\"advlist\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"inlinepopups\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"}','','',0,'0000-00-00 00:00:00',3,0),(413,'plg_editors-xtd_article','plugin','article','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_article\",\"type\":\"plugin\",\"creationDate\":\"October 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_ARTICLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"article\"}','','','',0,'0000-00-00 00:00:00',1,0),(414,'plg_editors-xtd_image','plugin','image','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_image\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"image\"}','','','',0,'0000-00-00 00:00:00',2,0),(415,'plg_editors-xtd_pagebreak','plugin','pagebreak','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}','','','',0,'0000-00-00 00:00:00',3,0),(416,'plg_editors-xtd_readmore','plugin','readmore','editors-xtd',0,1,1,0,'{\"name\":\"plg_editors-xtd_readmore\",\"type\":\"plugin\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_READMORE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"readmore\"}','','','',0,'0000-00-00 00:00:00',4,0),(417,'plg_search_categories','plugin','categories','search',0,1,1,0,'{\"name\":\"plg_search_categories\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(418,'plg_search_contacts','plugin','contacts','search',0,1,1,0,'{\"name\":\"plg_search_contacts\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(419,'plg_search_content','plugin','content','search',0,1,1,0,'{\"name\":\"plg_search_content\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(420,'plg_search_newsfeeds','plugin','newsfeeds','search',0,1,1,0,'{\"name\":\"plg_search_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(421,'plg_search_weblinks','plugin','weblinks','search',0,1,1,0,'{\"name\":\"plg_search_weblinks\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(422,'plg_system_languagefilter','plugin','languagefilter','system',0,0,1,1,'{\"name\":\"plg_system_languagefilter\",\"type\":\"plugin\",\"creationDate\":\"July 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagefilter\"}','','','',0,'0000-00-00 00:00:00',1,0),(423,'plg_system_p3p','plugin','p3p','system',0,1,1,0,'{\"name\":\"plg_system_p3p\",\"type\":\"plugin\",\"creationDate\":\"September 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_P3P_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"p3p\"}','{\"headers\":\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"}','','',0,'0000-00-00 00:00:00',2,0),(424,'plg_system_cache','plugin','cache','system',0,0,1,1,'{\"name\":\"plg_system_cache\",\"type\":\"plugin\",\"creationDate\":\"February 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CACHE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cache\"}','{\"browsercache\":\"0\",\"cachetime\":\"15\"}','','',0,'0000-00-00 00:00:00',9,0),(425,'plg_system_debug','plugin','debug','system',0,1,1,0,'{\"name\":\"plg_system_debug\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_DEBUG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"debug\"}','{\"profile\":\"1\",\"queries\":\"1\",\"memory\":\"1\",\"language_files\":\"1\",\"language_strings\":\"1\",\"strip-first\":\"1\",\"strip-prefix\":\"\",\"strip-suffix\":\"\"}','','',0,'0000-00-00 00:00:00',4,0),(426,'plg_system_log','plugin','log','system',0,1,1,1,'{\"name\":\"plg_system_log\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"log\"}','','','',0,'0000-00-00 00:00:00',5,0),(427,'plg_system_redirect','plugin','redirect','system',0,0,1,1,'{\"name\":\"plg_system_redirect\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"redirect\"}','','','',0,'0000-00-00 00:00:00',6,0),(428,'plg_system_remember','plugin','remember','system',0,1,1,1,'{\"name\":\"plg_system_remember\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_REMEMBER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"remember\"}','','','',0,'0000-00-00 00:00:00',7,0),(429,'plg_system_sef','plugin','sef','system',0,1,1,0,'{\"name\":\"plg_system_sef\",\"type\":\"plugin\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sef\"}','','','',0,'0000-00-00 00:00:00',8,0),(430,'plg_system_logout','plugin','logout','system',0,1,1,1,'{\"name\":\"plg_system_logout\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"logout\"}','','','',0,'0000-00-00 00:00:00',3,0),(431,'plg_user_contactcreator','plugin','contactcreator','user',0,0,1,0,'{\"name\":\"plg_user_contactcreator\",\"type\":\"plugin\",\"creationDate\":\"August 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTACTCREATOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contactcreator\"}','{\"autowebpage\":\"\",\"category\":\"34\",\"autopublish\":\"0\"}','','',0,'0000-00-00 00:00:00',1,0),(432,'plg_user_joomla','plugin','joomla','user',0,1,1,0,'{\"name\":\"plg_user_joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}','{\"strong_passwords\":\"1\",\"autoregister\":\"1\"}','','',0,'0000-00-00 00:00:00',2,0),(433,'plg_user_profile','plugin','profile','user',0,0,1,0,'{\"name\":\"plg_user_profile\",\"type\":\"plugin\",\"creationDate\":\"January 2008\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_PROFILE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"profile\"}','{\"register-require_address1\":\"1\",\"register-require_address2\":\"1\",\"register-require_city\":\"1\",\"register-require_region\":\"1\",\"register-require_country\":\"1\",\"register-require_postal_code\":\"1\",\"register-require_phone\":\"1\",\"register-require_website\":\"1\",\"register-require_favoritebook\":\"1\",\"register-require_aboutme\":\"1\",\"register-require_tos\":\"1\",\"register-require_dob\":\"1\",\"profile-require_address1\":\"1\",\"profile-require_address2\":\"1\",\"profile-require_city\":\"1\",\"profile-require_region\":\"1\",\"profile-require_country\":\"1\",\"profile-require_postal_code\":\"1\",\"profile-require_phone\":\"1\",\"profile-require_website\":\"1\",\"profile-require_favoritebook\":\"1\",\"profile-require_aboutme\":\"1\",\"profile-require_tos\":\"1\",\"profile-require_dob\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(434,'plg_extension_joomla','plugin','joomla','extension',0,1,1,1,'{\"name\":\"plg_extension_joomla\",\"type\":\"plugin\",\"creationDate\":\"May 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}','','','',0,'0000-00-00 00:00:00',1,0),(435,'plg_content_joomla','plugin','joomla','content',0,1,1,0,'{\"name\":\"plg_content_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}','','','',0,'0000-00-00 00:00:00',0,0),(436,'plg_system_languagecode','plugin','languagecode','system',0,0,1,0,'{\"name\":\"plg_system_languagecode\",\"type\":\"plugin\",\"creationDate\":\"November 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagecode\"}','','','',0,'0000-00-00 00:00:00',10,0),(437,'plg_quickicon_joomlaupdate','plugin','joomlaupdate','quickicon',0,1,1,1,'{\"name\":\"plg_quickicon_joomlaupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomlaupdate\"}','','','',0,'0000-00-00 00:00:00',0,0),(438,'plg_quickicon_extensionupdate','plugin','extensionupdate','quickicon',0,1,1,1,'{\"name\":\"plg_quickicon_extensionupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"extensionupdate\"}','','','',0,'0000-00-00 00:00:00',0,0),(439,'plg_captcha_recaptcha','plugin','recaptcha','captcha',0,1,1,0,'{\"name\":\"plg_captcha_recaptcha\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.0\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"recaptcha\"}','{\"public_key\":\"6LeUxMUSAAAAAIm_9_Gfp-QUzsTJFQHzMPJ2wdsn\",\"private_key\":\"6LeUxMUSAAAAABWv7Z5ldYClGFu2HsWwHYViWHwl\",\"theme\":\"clean\"}','','',0,'0000-00-00 00:00:00',0,0),(440,'plg_system_highlight','plugin','highlight','system',0,1,1,0,'{\"name\":\"plg_system_highlight\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',7,0),(441,'plg_content_finder','plugin','finder','content',0,0,1,0,'{\"name\":\"plg_content_finder\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_FINDER_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(442,'plg_finder_categories','plugin','categories','finder',0,1,1,0,'{\"name\":\"plg_finder_categories\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}','','','',0,'0000-00-00 00:00:00',1,0),(443,'plg_finder_contacts','plugin','contacts','finder',0,1,1,0,'{\"name\":\"plg_finder_contacts\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}','','','',0,'0000-00-00 00:00:00',2,0),(444,'plg_finder_content','plugin','content','finder',0,1,1,0,'{\"name\":\"plg_finder_content\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}','','','',0,'0000-00-00 00:00:00',3,0),(445,'plg_finder_newsfeeds','plugin','newsfeeds','finder',0,1,1,0,'{\"name\":\"plg_finder_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}','','','',0,'0000-00-00 00:00:00',4,0),(446,'plg_finder_weblinks','plugin','weblinks','finder',0,1,1,0,'{\"name\":\"plg_finder_weblinks\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',5,0),(447,'plg_finder_tags','plugin','tags','finder',0,1,1,0,'{\"name\":\"plg_finder_tags\",\"type\":\"plugin\",\"creationDate\":\"February 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}','','','',0,'0000-00-00 00:00:00',0,0),(448,'plg_twofactorauth_totp','plugin','totp','twofactorauth',0,0,1,0,'{\"name\":\"plg_twofactorauth_totp\",\"type\":\"plugin\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"totp\"}','','','',0,'0000-00-00 00:00:00',0,0),(449,'plg_authentication_cookie','plugin','cookie','authentication',0,1,1,0,'{\"name\":\"plg_authentication_cookie\",\"type\":\"plugin\",\"creationDate\":\"July 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_COOKIE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(450,'plg_twofactorauth_yubikey','plugin','yubikey','twofactorauth',0,0,1,0,'{\"name\":\"plg_twofactorauth_yubikey\",\"type\":\"plugin\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"yubikey\"}','','','',0,'0000-00-00 00:00:00',0,0),(451,'plg_search_tags','plugin','tags','search',0,1,1,0,'{\"name\":\"plg_search_tags\",\"type\":\"plugin\",\"creationDate\":\"March 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}','{\"search_limit\":\"50\",\"show_tagged_items\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(503,'beez3','template','beez3','',0,1,1,0,'{\"name\":\"beez3\",\"type\":\"template\",\"creationDate\":\"25 November 2009\",\"author\":\"Angie Radtke\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"a.radtke@derauftritt.de\",\"authorUrl\":\"http:\\/\\/www.der-auftritt.de\",\"version\":\"3.1.0\",\"description\":\"TPL_BEEZ3_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}','{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"navposition\":\"center\",\"templatecolor\":\"nature\"}','','',0,'0000-00-00 00:00:00',0,0),(504,'hathor','template','hathor','',1,1,1,0,'{\"name\":\"hathor\",\"type\":\"template\",\"creationDate\":\"May 2010\",\"author\":\"Andrea Tarr\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"hathor@tarrconsulting.com\",\"authorUrl\":\"http:\\/\\/www.tarrconsulting.com\",\"version\":\"3.0.0\",\"description\":\"TPL_HATHOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}','{\"showSiteName\":\"0\",\"colourChoice\":\"0\",\"boldText\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),(506,'protostar','template','protostar','',0,1,1,0,'{\"name\":\"protostar\",\"type\":\"template\",\"creationDate\":\"4\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_PROTOSTAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}','{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),(507,'isis','template','isis','',1,1,1,0,'{\"name\":\"isis\",\"type\":\"template\",\"creationDate\":\"3\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_ISIS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}','{\"templateColor\":\"\",\"logoFile\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),(600,'English (en-GB)','language','en-GB','',0,1,1,1,'{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"2013-03-07\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.2\",\"description\":\"en-GB site language\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(601,'English (en-GB)','language','en-GB','',1,1,1,1,'{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"2013-03-07\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.2\",\"description\":\"en-GB administrator language\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(700,'files_joomla','file','joomla','',0,1,1,1,'{\"name\":\"files_joomla\",\"type\":\"file\",\"creationDate\":\"June 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2015 Open Source Matters. All rights reserved\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.3\",\"description\":\"FILES_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(801,'weblinks','package','pkg_weblinks','',0,1,1,0,'','','','',0,'0000-00-00 00:00:00',0,0),(10003,'gantry','component','com_gantry','',0,1,0,0,'{\"name\":\"Gantry\",\"type\":\"component\",\"creationDate\":\"August 6, 2014\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2014 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"4.1.25\",\"description\":\"Gantry Starting Template for Joomla! v4.1.25\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10004,'System - Gantry','plugin','gantry','system',0,1,1,0,'{\"name\":\"System - Gantry\",\"type\":\"plugin\",\"creationDate\":\"August 6, 2014\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2014 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"4.1.25\",\"description\":\"Gantry System Plugin for Joomla\",\"group\":\"\"}','{\"debugloglevel\":\"63\"}','','',0,'0000-00-00 00:00:00',1,0),(10005,'rt_voxel_responsive','template','rt_voxel_responsive','',0,1,1,0,'{\"name\":\"rt_voxel_responsive\",\"type\":\"template\",\"creationDate\":\" April 17, 2014\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"Copyright 2005-2014 - RocketTheme.com\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"1.6\",\"description\":\"Voxel\",\"group\":\"\"}','{\"master\":\"true\"}','','',0,'0000-00-00 00:00:00',0,0),(10006,'System - RokExtender','plugin','rokextender','system',0,1,1,0,'{\"name\":\"System - RokExtender\",\"type\":\"plugin\",\"creationDate\":\"October 31, 2012\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2012 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"2.0.0\",\"description\":\"System - Gantry\",\"group\":\"\"}','{\"registered\":\"\\/modules\\/mod_roknavmenu\\/lib\\/RokNavMenuEvents.php\"}','','',0,'0000-00-00 00:00:00',1,0),(10007,'RokNavMenu','module','mod_roknavmenu','',0,1,1,0,'{\"name\":\"RokNavMenu\",\"type\":\"module\",\"creationDate\":\"November 5, 2013\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2013 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"2.0.7\",\"description\":\"RocketTheme Customizable Navigation Menu\",\"group\":\"\"}','{\"limit_levels\":\"0\",\"startLevel\":\"0\",\"endLevel\":\"0\",\"showAllChildren\":\"0\",\"filteringspacer2\":\"\",\"theme\":\"default\",\"custom_layout\":\"default.php\",\"custom_formatter\":\"default.php\",\"cache\":\"0\",\"module_cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}','','',0,'0000-00-00 00:00:00',0,0),(10008,'admintools','component','com_admintools','',1,1,0,0,'{\"name\":\"Admintools\",\"type\":\"component\",\"creationDate\":\"2014-09-30\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2010 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"3.2.1\",\"description\":\"Security and utilitarian tools for Joomla! site administrators\",\"group\":\"\"}','{\"scandiffs\":\"0\",\"scanemail\":\"\",\"showstats\":\"1\",\"longconfigpage\":\"0\",\"downloadid\":\"40703:83e3fb4a30d457f9758232fc028c9d0a\",\"autoupdateCli\":\"1\",\"notificationFreq\":\"1\",\"notificationTime\":\"day\",\"notificationEmail\":\"\",\"htmaker_folders_fix_at240\":\"1\",\"lastversion\":\"3.1.1\",\"acceptlicense\":true,\"acceptsupport\":true}','','',0,'0000-00-00 00:00:00',0,0),(10009,'System - Admin Tools','plugin','admintools','system',0,1,1,0,'{\"name\":\"System - Admin Tools\",\"type\":\"plugin\",\"creationDate\":\"2014-09-30\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2010 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"3.2.1\",\"description\":\"\\n\\t\\tHandles URL redirections defined in Admin Tools, fends off common attacks\\n\\t\\tand automates session table and cache clean-up\\n\\t\",\"group\":\"\"}','{\"language_override\":\"\",\"@spacer\":\"\",\"sesoptimizer\":\"0\",\"sesopt_freq\":\"60\",\"sescleaner\":\"0\",\"ses_freq\":\"60\",\"cachecleaner\":\"0\",\"cache_freq\":\"1440\",\"cacheexpire\":\"0\",\"cacheexp_freq\":\"60\",\"cleantemp\":\"0\",\"cleantemp_freq\":\"60\",\"deleteinactive\":\"0\",\"deleteinactive_days\":\"7\",\"maxlogentries\":\"0\"}','','',0,'0000-00-00 00:00:00',-1,0),(10010,'System - One Click Action','plugin','oneclickaction','system',0,0,1,0,'{\"name\":\"System - One Click Action\",\"type\":\"plugin\",\"creationDate\":\"2011-05-26\",\"author\":\"Nicholas K. Dionysopoulos \\/ AkeebaBackup.com\",\"copyright\":\"Copyright (c)2011-2013 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"2.1\",\"description\":\"PLG_ONCECLICK_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10011,'System - Admin Tools Update Email','plugin','atoolsupdatecheck','system',0,0,1,0,'{\"name\":\"System - Admin Tools Update Email\",\"type\":\"plugin\",\"creationDate\":\"2011-05-26\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2011 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"1.0\",\"description\":\"PLG_ATOOLSUPDATECHECK_DESCRIPTION2\",\"group\":\"\"}','{\"language_override\":\"\",\"email\":\"\",\"singleclick\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),(10012,'System - Admin Tools Joomla! Update Email','plugin','atoolsjupdatecheck','system',0,0,1,0,'{\"name\":\"System - Admin Tools Joomla! Update Email\",\"type\":\"plugin\",\"creationDate\":\"2011-05-26\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2011 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"1.0\",\"description\":\"PLG_ATOOLSJUPDATECHECK_DESCRIPTION2\",\"group\":\"\"}','{\"language_override\":\"\",\"email\":\"\",\"singleclick\":\"0\",\"lastrun\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),(10013,'plg_installer_admintools','plugin','admintools','installer',0,1,1,0,'{\"name\":\"plg_installer_admintools\",\"type\":\"plugin\",\"creationDate\":\"2014-03-06\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2012 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"1.0\",\"description\":\"PLG_INSTALLER_ADMINTOOLS_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10015,'AkeebaStrapper','file','files_strapper','',0,1,0,0,'{\"name\":\"AkeebaStrapper\",\"type\":\"file\",\"creationDate\":\"2014-09-11 16:58:22\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"(C) 2012-2013 Akeeba Ltd.\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"https:\\/\\/www.akeebabackup.com\",\"version\":\"rev844F136-1410443902\",\"description\":\"Namespaced jQuery, jQuery UI and Bootstrap for Akeeba products.\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),(10016,'PLG_SYSTEM_AKGEOIP','plugin','akgeoip','system',0,0,1,0,'{\"name\":\"PLG_SYSTEM_AKGEOIP\",\"type\":\"plugin\",\"creationDate\":\"2014-03-12\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2013 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"1.0.3\",\"description\":\"PLG_SYSTEM_AKGEOIP_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10017,'akeeba','component','com_akeeba','',1,1,0,0,'{\"name\":\"Akeeba\",\"type\":\"component\",\"creationDate\":\"2014-09-30\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2006-2014 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"4.0.5\",\"description\":\"Akeeba Backup Pro - Full Joomla! site backup solution, Professional Edition\",\"group\":\"\"}','{\"frontend_enable\":\"0\",\"failure_frontend_enable\":\"0\",\"frontend_secret_word\":\"\",\"frontend_email_on_finish\":\"0\",\"frontend_email_address\":\"\",\"frontend_email_subject\":\"\",\"frontend_email_body\":\"\",\"failure_timeout\":180,\"failure_email_address\":\"\",\"failure_email_subject\":\"\",\"failure_email_body\":\"\",\"siteurl\":\"http:\\/\\/vppihomes.com\\/\",\"jversion\":\"1.6\",\"jlibrariesdir\":\"\\/var\\/www\\/vppihomes.com\\/www\\/libraries\",\"lastversion\":\"3.11.3\",\"angieupgrade\":1,\"update_dlid\":\"40703:83e3fb4a30d457f9758232fc028c9d0a\",\"displayphpwarning\":\"1\",\"autoupdateCli\":\"1\",\"notificationFreq\":\"1\",\"notificationTime\":\"day\",\"notificationEmail\":\"\",\"useencryption\":\"1\",\"dateformat\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),(10018,'plg_installer_akeebabackup','plugin','akeebabackup','installer',0,1,1,0,'{\"name\":\"plg_installer_akeebabackup\",\"type\":\"plugin\",\"creationDate\":\"2014-03-06\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2012 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"1.0\",\"description\":\"PLG_INSTALLER_AKEEBABACKUP_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10019,'plg_quickicon_akeebabackup','plugin','akeebabackup','quickicon',0,1,1,0,'{\"name\":\"plg_quickicon_akeebabackup\",\"type\":\"plugin\",\"creationDate\":\"2012-09-26\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2012 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"1.0\",\"description\":\"PLG_QUICKICON_AKEEBABACKUP_XML_DESCRIPTION\",\"group\":\"\"}','{\"context\":\"mod_quickicon\",\"enablewarning\":\"1\",\"warnfailed\":\"1\",\"maxbackupperiod\":\"24\"}','','',0,'0000-00-00 00:00:00',0,0),(10020,'PLG_SYSTEM_AKEEBAUPDATECHECK_TITLE','plugin','akeebaupdatecheck','system',0,0,1,0,'{\"name\":\"PLG_SYSTEM_AKEEBAUPDATECHECK_TITLE\",\"type\":\"plugin\",\"creationDate\":\"2011-05-26\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2009-2014 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"1.1\",\"description\":\"PLG_AKEEBAUPDATECHECK_DESCRIPTION2\",\"group\":\"\"}','{\"language_override\":\"\",\"email\":\"\",\"singleclick\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),(10021,'PLG_SYSTEM_BACKUPONUPDATE_TITLE','plugin','backuponupdate','system',0,0,1,0,'{\"name\":\"PLG_SYSTEM_BACKUPONUPDATE_TITLE\",\"type\":\"plugin\",\"creationDate\":\"2013-08-13\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2009-2014 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"3.7\",\"description\":\"PLG_SYSTEM_BACKUPONUPDATE_DESCRIPTION\",\"group\":\"\"}','{\"profileid\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(10022,'PLG_SRP_TITLE','plugin','srp','system',0,0,1,0,'{\"name\":\"PLG_SRP_TITLE\",\"type\":\"plugin\",\"creationDate\":\"2014-09-30\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2011-2014 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"4.0.5\",\"description\":\"PLG_SRP_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10023,'plg_editors_jce','plugin','jce','editors',0,1,1,0,'{\"name\":\"plg_editors_jce\",\"type\":\"plugin\",\"creationDate\":\"11 September 2014\",\"author\":\"Ryan Demmer\",\"copyright\":\"2006-2010 Ryan Demmer\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"http:\\/\\/www.joomlacontenteditor.net\",\"version\":\"2.4.3\",\"description\":\"WF_EDITOR_PLUGIN_DESC\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10024,'plg_quickicon_jcefilebrowser','plugin','jcefilebrowser','quickicon',0,1,1,0,'{\"name\":\"plg_quickicon_jcefilebrowser\",\"type\":\"plugin\",\"creationDate\":\"11 September 2014\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2014 Ryan Demmer. All rights reserved\",\"authorEmail\":\"@@email@@\",\"authorUrl\":\"www.joomalcontenteditor.net\",\"version\":\"2.4.3\",\"description\":\"PLG_QUICKICON_JCEFILEBROWSER_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10025,'jce','component','com_jce','',1,1,0,0,'{\"name\":\"JCE\",\"type\":\"component\",\"creationDate\":\"11 September 2014\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2014 Ryan Demmer. All rights reserved\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"www.joomlacontenteditor.net\",\"version\":\"2.4.3\",\"description\":\"WF_ADMIN_DESC\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10028,'com_vppi','component','com_vppi','',1,1,0,0,'{\"name\":\"com_vppi\",\"type\":\"component\",\"creationDate\":\"September 10, 2014\",\"author\":\"PDXfixIT\",\"copyright\":\"Copyleft (C) 2014. All rights reserved.\",\"authorEmail\":\"info@pdxfixit.com\",\"authorUrl\":\"http:\\/\\/www.pdxfixit.com\\/\",\"version\":\"1.1\",\"description\":\"<p style=\\\"font-size: 18px;\\\"><img src=\\\"..\\/media\\/com_vppi\\/images\\/vppi-logo.png\\\" alt=\\\"VPPI Logo\\\" title=\\\"Vantage Point Properties, Inc.\\\" style=\\\"float: left; margin: 0 10px 10px 0;\\\">VPPI component v1.1<br>Released: September 10, 2014<\\/p><p style=\\\"clear: left;\\\"><a href=\\\"http:\\/\\/www.pdxfixit.com\\/\\\" target=\\\"_blank\\\" title=\\\"Made by PDXfixIT.\\\"><img src=\\\"http:\\/\\/updates.pdxfixit.com\\/logo.png\\\" alt=\\\"PDXfixIT\\\" title=\\\"PDXfixIT\\\"><\\/a><\\/p>\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10043,'Contact Form','module','mod_contactform','',0,1,0,0,'{\"name\":\"Contact Form\",\"type\":\"module\",\"creationDate\":\"July 15, 2014\",\"author\":\"PDXfixIT\",\"copyright\":\"(C) 2012-2014 PDXfixIT, LLC. All rights reserved\",\"authorEmail\":\"info@pdxfixit.com\",\"authorUrl\":\"http:\\/\\/www.pdxfixit.com\\/\",\"version\":\"1.9.2\",\"description\":\"<img src=\\\"http:\\/\\/updates.pdxfixit.com\\/logo.png\\\" alt=\\\"PDXfixIT\\\" border=\\\"0\\\"\\/><br \\/>Version: 1.9.2<br \\/>Release Date: July 15, 2014<br \\/>Contact Form\",\"group\":\"\"}','{\"fname\":\"required\",\"fnameLabel\":\"First Name\",\"fnameValue\":\"\",\"fnameErrorMessage\":\"Please enter a first name.\",\"lname\":\"required\",\"lnameLabel\":\"Last Name\",\"lnameValue\":\"\",\"lnameErrorMessage\":\"Please enter a last name.\",\"title\":\"hide\",\"titleLabel\":\"Title\",\"titleValue\":\"\",\"titleErrorMessage\":\"Please enter a title.\",\"org\":\"hide\",\"orgLabel\":\"Organization\",\"orgValue\":\"\",\"orgErrorMessage\":\"Please enter your associated organization.\",\"address\":\"hide\",\"addressLabel\":\"Address\",\"addressValue\":\"\",\"addressErrorMessage\":\"Please enter your address.\",\"address2\":\"hide\",\"address2Label\":\"Address Line 2\",\"address2Value\":\"\",\"address2ErrorMessage\":\"Please enter additional address details.\",\"city\":\"hide\",\"cityLabel\":\"City\",\"cityValue\":\"\",\"cityErrorMessage\":\"Please enter your city.\",\"state\":\"hide\",\"stateLabel\":\"State\",\"stateValue\":\"\",\"stateErrorMessage\":\"Please choose your state.\",\"states\":\"\",\"zip\":\"hide\",\"zipLabel\":\"Zip Code\",\"zipValue\":\"\",\"zipErrorMessage\":\"Please enter your zip code.\",\"phone\":\"hide\",\"phoneLabel\":\"Phone\",\"phoneValue\":\"\",\"phoneErrorMessage\":\"Please enter your phone number.\",\"email\":\"required\",\"emailLabel\":\"Email\",\"emailValue\":\"\",\"emailErrorMessage\":\"Please enter your email address.\",\"comments\":\"show\",\"commentsLabel\":\"Comments\",\"commentsValue\":\"\",\"commentsErrorMessage\":\"Please enter your comments.\",\"custom1\":\"hide\",\"custom1Label\":\"Custom 1\",\"custom1Value\":\"\",\"custom1ErrorMessage\":\"Please choose one.\",\"custom1Type\":\"select\",\"custom1Values\":\"One,Two,Three\",\"custom1Mask\":\"\",\"custom1HelpTitle\":\"\",\"custom1HelpText\":\"\",\"custom2\":\"hide\",\"custom2Label\":\"Custom 2\",\"custom2Value\":\"\",\"custom2ErrorMessage\":\"Please choose one.\",\"custom2Type\":\"select\",\"custom2Values\":\"One,Two,Three\",\"custom2Mask\":\"\",\"custom2HelpTitle\":\"\",\"custom2HelpText\":\"\",\"custom3\":\"hide\",\"custom3Label\":\"Custom 3\",\"custom3Value\":\"\",\"custom3ErrorMessage\":\"Please choose one.\",\"custom3Type\":\"select\",\"custom3Values\":\"One,Two,Three\",\"custom3Mask\":\"\",\"custom3HelpTitle\":\"\",\"custom3HelpText\":\"\",\"custom4\":\"hide\",\"custom4Label\":\"Custom 4\",\"custom4Value\":\"\",\"custom4ErrorMessage\":\"Please choose one.\",\"custom4Type\":\"select\",\"custom4Values\":\"One,Two,Three\",\"custom4Mask\":\"\",\"custom4HelpTitle\":\"\",\"custom4HelpText\":\"\",\"custom5\":\"hide\",\"custom5Label\":\"Custom 5\",\"custom5Value\":\"\",\"custom5ErrorMessage\":\"Please choose one.\",\"custom5Type\":\"select\",\"custom5Values\":\"One,Two,Three\",\"custom5Mask\":\"\",\"custom5HelpTitle\":\"\",\"custom5HelpText\":\"\",\"captcha\":\"show\",\"captcha-public\":\"6LeUxMUSAAAAAIm_9_Gfp-QUzsTJFQHzMPJ2wdsn\",\"captcha-private\":\"6LeUxMUSAAAAABWv7Z5ldYClGFu2HsWwHYViWHwl\",\"sendemail\":\"\",\"emailsubject\":\"\",\"table\":\"#__contactform\",\"successnotice\":\"Submission received successfully.  Thank you.\",\"dberror\":\"We are sorry for the inconvenience, but there was an error saving your data.  Please try again.\"}','','',0,'0000-00-00 00:00:00',0,0),(10045,'com_xmap','component','com_xmap','',1,1,0,0,'{\"name\":\"com_xmap\",\"type\":\"component\",\"creationDate\":\"2011-04-10\",\"author\":\"Guillermo Vargas\",\"copyright\":\"This component is released under the GNU\\/GPL License\",\"authorEmail\":\"guille@vargas.co.cr\",\"authorUrl\":\"http:\\/\\/www.jooxmap.com\",\"version\":\"2.3.4\",\"description\":\"Xmap - Sitemap Generator for Joomla!\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10046,'Xmap - Content Plugin','plugin','com_content','xmap',0,1,1,0,'{\"name\":\"Xmap - Content Plugin\",\"type\":\"plugin\",\"creationDate\":\"01\\/26\\/2011\",\"author\":\"Guillermo Vargas\",\"copyright\":\"GNU GPL\",\"authorEmail\":\"guille@vargas.co.cr\",\"authorUrl\":\"joomla.vargas.co.cr\",\"version\":\"2.0.4\",\"description\":\"XMAP_CONTENT_PLUGIN_DESCRIPTION\",\"group\":\"\"}','{\"expand_categories\":\"1\",\"expand_featured\":\"1\",\"include_archived\":\"2\",\"show_unauth\":\"0\",\"add_pagebreaks\":\"1\",\"max_art\":\"0\",\"max_art_age\":\"0\",\"add_images\":\"1\",\"cat_priority\":\"-1\",\"cat_changefreq\":\"-1\",\"art_priority\":\"-1\",\"art_changefreq\":\"-1\",\"keywords\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),(10051,'Xmap - WebLinks Plugin','plugin','com_weblinks','xmap',0,1,1,0,'{\"name\":\"Xmap - WebLinks Plugin\",\"type\":\"plugin\",\"creationDate\":\"Apr 2004\",\"author\":\"Guillermo Vargas\",\"copyright\":\"GNU GPL\",\"authorEmail\":\"guille@vargas.co.cr\",\"authorUrl\":\"joomla.vargas.co.cr\",\"version\":\"2.0.1\",\"description\":\"XMAP_WL_PLUGIN_DESCRIPTION\",\"group\":\"\"}','{\"include_links\":\"1\",\"max_links\":\"\",\"cat_priority\":\"-1\",\"cat_changefreq\":\"-1\",\"link_priority\":\"-1\",\"link_changefreq\":\"-1\"}','','',0,'0000-00-00 00:00:00',0,0),(10053,'xmap','package','pkg_xmap','',0,1,1,0,'{\"name\":\"Xmap Package\",\"type\":\"package\",\"creationDate\":\"Unknown\",\"author\":\"Unknown\",\"copyright\":\"\",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"2.3.3\",\"description\":\"The Site Map generator for Joomla!\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10056,'VPPI Admin Icon','module','mod_vppiadminicon','',1,1,2,0,'{\"name\":\"VPPI Admin Icon\",\"type\":\"module\",\"creationDate\":\"September 10, 2014\",\"author\":\"PDXfixIT\",\"copyright\":\"Copyleft (C) 2014. All rights reserved.\",\"authorEmail\":\"info@pdxfixit.com\",\"authorUrl\":\"http:\\/\\/www.pdxfixit.com\\/\",\"version\":\"1.1\",\"description\":\"<div><p style=\\\"font-size: 18px;\\\"><img src=\\\"..\\/media\\/com_vppi\\/images\\/vppi-logo.png\\\" alt=\\\"VPPI Logo\\\" title=\\\"Vantage Point Properties, Inc.\\\" style=\\\"margin-bottom: 10px;\\\"><br>VPPI Admin Icon v1.1<br>September 10, 2014<br><a href=\\\"http:\\/\\/www.pdxfixit.com\\/\\\" target=\\\"_blank\\\" title=\\\"Made by PDXfixIT.\\\"><img src=\\\"http:\\/\\/updates.pdxfixit.com\\/logo.png\\\" alt=\\\"PDXfixIT\\\" title=\\\"PDXfixIT\\\" style=\\\"margin-top: 10px;\\\"><\\/a><\\/p><\\/div><p>This module shows a link to the VPPI component in the Joomla Administrator control panel. After installing, enable the module in Module Manager > Administrator and assign to the \\\"cpanel\\\" position.<\\/p>\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),(10058,'Gantry','library','lib_gantry','',0,1,1,0,'{\"name\":\"Gantry\",\"type\":\"library\",\"creationDate\":\"August 6, 2014\",\"author\":\"RocketTheme, LLC\",\"copyright\":\"(C) 2005 - 2014 RocketTheme, LLC. All rights reserved.\",\"authorEmail\":\"support@rockettheme.com\",\"authorUrl\":\"http:\\/\\/www.rockettheme.com\",\"version\":\"4.1.25\",\"description\":\"Gantry Starting Template for Joomla! v4.1.25\",\"group\":\"\"}','{}','{\"last_update\":1410412450}','',0,'0000-00-00 00:00:00',0,0),(10060,'F0F (NEW) DO NOT REMOVE','library','lib_f0f','',0,1,1,0,'{\"name\":\"F0F (NEW) DO NOT REMOVE\",\"type\":\"library\",\"creationDate\":\"2014-09-11 16:58:22\",\"author\":\"Nicholas K. Dionysopoulos \\/ Akeeba Ltd\",\"copyright\":\"(C)2011-2014 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"https:\\/\\/www.akeebabackup.com\",\"version\":\"rev844F136-1410443902\",\"description\":\"Framework-on-Framework (FOF) newer version - DO NOT REMOVE - The rapid component development framework for Joomla!. This package is the newer version of FOF, not the one shipped with Joomla! as the official Joomla! RAD Layer. The Joomla! RAD Layer has ceased development in March 2014. DO NOT UNINSTALL THIS PACKAGE, IT IS *** N O T *** A DUPLICATE OF THE \'FOF\' PACKAGE. REMOVING EITHER FOF PACKAGE WILL BREAK YOUR SITE.\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0);
/*!40000 ALTER TABLE `mf0kn_extensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_filters`
--

DROP TABLE IF EXISTS `mf0kn_finder_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_filters` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext,
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_filters`
--

LOCK TABLES `mf0kn_finder_filters` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links`
--

DROP TABLE IF EXISTS `mf0kn_finder_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double unsigned NOT NULL DEFAULT '0',
  `sale_price` double unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL,
  PRIMARY KEY (`link_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_title` (`title`),
  KEY `idx_md5` (`md5sum`),
  KEY `idx_url` (`url`(75)),
  KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links`
--

LOCK TABLES `mf0kn_finder_links` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_terms0`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_terms0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_terms0` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_terms0`
--

LOCK TABLES `mf0kn_finder_links_terms0` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms0` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_terms1`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_terms1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_terms1` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_terms1`
--

LOCK TABLES `mf0kn_finder_links_terms1` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms1` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_terms2`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_terms2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_terms2` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_terms2`
--

LOCK TABLES `mf0kn_finder_links_terms2` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms2` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_terms3`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_terms3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_terms3` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_terms3`
--

LOCK TABLES `mf0kn_finder_links_terms3` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms3` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_terms4`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_terms4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_terms4` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_terms4`
--

LOCK TABLES `mf0kn_finder_links_terms4` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms4` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_terms5`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_terms5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_terms5` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_terms5`
--

LOCK TABLES `mf0kn_finder_links_terms5` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms5` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_terms6`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_terms6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_terms6` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_terms6`
--

LOCK TABLES `mf0kn_finder_links_terms6` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms6` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_terms7`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_terms7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_terms7` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_terms7`
--

LOCK TABLES `mf0kn_finder_links_terms7` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms7` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_terms8`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_terms8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_terms8` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_terms8`
--

LOCK TABLES `mf0kn_finder_links_terms8` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms8` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_terms9`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_terms9`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_terms9` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_terms9`
--

LOCK TABLES `mf0kn_finder_links_terms9` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms9` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_terms9` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_termsa`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_termsa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_termsa` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_termsa`
--

LOCK TABLES `mf0kn_finder_links_termsa` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_termsa` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_termsa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_termsb`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_termsb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_termsb` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_termsb`
--

LOCK TABLES `mf0kn_finder_links_termsb` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_termsb` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_termsb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_termsc`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_termsc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_termsc` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_termsc`
--

LOCK TABLES `mf0kn_finder_links_termsc` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_termsc` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_termsc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_termsd`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_termsd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_termsd` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_termsd`
--

LOCK TABLES `mf0kn_finder_links_termsd` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_termsd` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_termsd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_termse`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_termse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_termse` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_termse`
--

LOCK TABLES `mf0kn_finder_links_termse` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_termse` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_termse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_links_termsf`
--

DROP TABLE IF EXISTS `mf0kn_finder_links_termsf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_links_termsf` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_links_termsf`
--

LOCK TABLES `mf0kn_finder_links_termsf` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_links_termsf` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_links_termsf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_taxonomy`
--

DROP TABLE IF EXISTS `mf0kn_finder_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_taxonomy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `access` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `state` (`state`),
  KEY `ordering` (`ordering`),
  KEY `access` (`access`),
  KEY `idx_parent_published` (`parent_id`,`state`,`access`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_taxonomy`
--

LOCK TABLES `mf0kn_finder_taxonomy` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_taxonomy` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_taxonomy_map`
--

DROP TABLE IF EXISTS `mf0kn_finder_taxonomy_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_taxonomy_map` (
  `link_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`node_id`),
  KEY `link_id` (`link_id`),
  KEY `node_id` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_taxonomy_map`
--

LOCK TABLES `mf0kn_finder_taxonomy_map` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_taxonomy_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_taxonomy_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_terms`
--

DROP TABLE IF EXISTS `mf0kn_finder_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_terms` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `idx_term` (`term`),
  KEY `idx_term_phrase` (`term`,`phrase`),
  KEY `idx_stem_phrase` (`stem`,`phrase`),
  KEY `idx_soundex_phrase` (`soundex`,`phrase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_terms`
--

LOCK TABLES `mf0kn_finder_terms` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_terms` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_terms_common`
--

DROP TABLE IF EXISTS `mf0kn_finder_terms_common`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL,
  KEY `idx_word_lang` (`term`,`language`),
  KEY `idx_lang` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_terms_common`
--

LOCK TABLES `mf0kn_finder_terms_common` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_terms_common` DISABLE KEYS */;
INSERT INTO `mf0kn_finder_terms_common` VALUES ('a','en'),('about','en'),('after','en'),('ago','en'),('all','en'),('am','en'),('an','en'),('and','en'),('ani','en'),('any','en'),('are','en'),('aren\'t','en'),('as','en'),('at','en'),('be','en'),('but','en'),('by','en'),('for','en'),('from','en'),('get','en'),('go','en'),('how','en'),('if','en'),('in','en'),('into','en'),('is','en'),('isn\'t','en'),('it','en'),('its','en'),('me','en'),('more','en'),('most','en'),('must','en'),('my','en'),('new','en'),('no','en'),('none','en'),('not','en'),('noth','en'),('nothing','en'),('of','en'),('off','en'),('often','en'),('old','en'),('on','en'),('onc','en'),('once','en'),('onli','en'),('only','en'),('or','en'),('other','en'),('our','en'),('ours','en'),('out','en'),('over','en'),('page','en'),('she','en'),('should','en'),('small','en'),('so','en'),('some','en'),('than','en'),('thank','en'),('that','en'),('the','en'),('their','en'),('theirs','en'),('them','en'),('then','en'),('there','en'),('these','en'),('they','en'),('this','en'),('those','en'),('thus','en'),('time','en'),('times','en'),('to','en'),('too','en'),('true','en'),('under','en'),('until','en'),('up','en'),('upon','en'),('use','en'),('user','en'),('users','en'),('veri','en'),('version','en'),('very','en'),('via','en'),('want','en'),('was','en'),('way','en'),('were','en'),('what','en'),('when','en'),('where','en'),('whi','en'),('which','en'),('who','en'),('whom','en'),('whose','en'),('why','en'),('wide','en'),('will','en'),('with','en'),('within','en'),('without','en'),('would','en'),('yes','en'),('yet','en'),('you','en'),('your','en'),('yours','en');
/*!40000 ALTER TABLE `mf0kn_finder_terms_common` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_tokens`
--

DROP TABLE IF EXISTS `mf0kn_finder_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '1',
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT '',
  KEY `idx_word` (`term`),
  KEY `idx_context` (`context`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_tokens`
--

LOCK TABLES `mf0kn_finder_tokens` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_tokens_aggregate`
--

DROP TABLE IF EXISTS `mf0kn_finder_tokens_aggregate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_tokens_aggregate` (
  `term_id` int(10) unsigned NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `term_weight` float unsigned NOT NULL,
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `context_weight` float unsigned NOT NULL,
  `total_weight` float unsigned NOT NULL,
  `language` char(3) NOT NULL DEFAULT '',
  KEY `token` (`term`),
  KEY `keyword_id` (`term_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_tokens_aggregate`
--

LOCK TABLES `mf0kn_finder_tokens_aggregate` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_tokens_aggregate` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_tokens_aggregate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_finder_types`
--

DROP TABLE IF EXISTS `mf0kn_finder_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_finder_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_finder_types`
--

LOCK TABLES `mf0kn_finder_types` WRITE;
/*!40000 ALTER TABLE `mf0kn_finder_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_finder_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_jfbconnect_channel`
--

DROP TABLE IF EXISTS `mf0kn_jfbconnect_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_jfbconnect_channel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `provider` varchar(20) NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `title` varchar(40) NOT NULL DEFAULT '',
  `description` text,
  `attribs` text,
  `published` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_jfbconnect_channel`
--

LOCK TABLES `mf0kn_jfbconnect_channel` WRITE;
/*!40000 ALTER TABLE `mf0kn_jfbconnect_channel` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_jfbconnect_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_jfbconnect_config`
--

DROP TABLE IF EXISTS `mf0kn_jfbconnect_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_jfbconnect_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `setting` varchar(50) NOT NULL,
  `value` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `setting` (`setting`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_jfbconnect_config`
--

LOCK TABLES `mf0kn_jfbconnect_config` WRITE;
/*!40000 ALTER TABLE `mf0kn_jfbconnect_config` DISABLE KEYS */;
INSERT INTO `mf0kn_jfbconnect_config` VALUES (1,'db_version','6.1.0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'facebook_app_id','449484815070477','2014-07-03 00:18:55','2014-07-30 20:23:29'),(3,'facebook_secret_key','46fc79188657ace5546578fde5ff0738','2014-07-03 00:18:55','2014-07-30 20:23:29'),(4,'github_app_id','','2014-07-03 00:18:55','2014-07-30 20:23:29'),(5,'github_secret_key','','2014-07-03 00:18:55','2014-07-30 20:23:29'),(6,'google_app_id','','2014-07-03 00:18:55','2014-07-30 20:23:29'),(7,'google_secret_key','','2014-07-03 00:18:55','2014-07-30 20:23:29'),(8,'instagram_app_id','','2014-07-03 00:18:55','2014-07-30 20:23:29'),(9,'instagram_secret_key','','2014-07-03 00:18:55','2014-07-30 20:23:29'),(10,'linkedin_app_id','dx7hhol3s5do','2014-07-03 00:18:55','2014-07-30 20:23:29'),(11,'linkedin_secret_key','g6ofAMBA0sI9Ukfm','2014-07-03 00:18:55','2014-07-30 20:23:29'),(12,'twitter_app_id','','2014-07-03 00:18:55','2014-07-30 20:23:29'),(13,'twitter_secret_key','','2014-07-03 00:18:55','2014-07-30 20:23:29'),(14,'vk_app_id','','2014-07-03 00:18:55','2014-07-30 20:23:29'),(15,'vk_secret_key','','2014-07-03 00:18:55','2014-07-30 20:23:29'),(16,'sc_download_id','1384d70318697d0d0bea538041ecd7bb','2014-07-03 00:18:55','2014-07-30 20:23:29'),(17,'autotune_authorization','O:8:\"stdClass\":2:{s:10:\"authorized\";b:0;s:8:\"messages\";O:8:\"stdClass\":3:{s:4:\"user\";s:15:\"User Not Found!\";s:12:\"subscription\";s:22:\"No Subscription Found!\";s:7:\"expires\";s:22:\"No Subscription Found!\";}}','2014-07-03 00:18:56','2014-07-30 20:23:30'),(18,'automatic_registration','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(19,'registration_component','jfbconnect','2014-07-03 00:24:54','2014-07-30 20:22:29'),(20,'facebook_auto_map_by_email','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(21,'joomla_skip_newuser_activation','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(22,'auto_username_format','2','2014-07-03 00:24:54','2014-07-30 20:22:29'),(23,'registration_send_new_user_email','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(24,'generate_random_password','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(25,'registration_display_mode','vertical','2014-07-03 00:24:54','2014-07-30 20:22:29'),(26,'registration_show_username','1','2014-07-03 00:24:54','2014-07-30 20:22:29'),(27,'registration_show_password','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(28,'registration_show_email','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(29,'registration_show_name','1','2014-07-03 00:24:54','2014-07-30 20:22:29'),(30,'facebook_new_user_redirect','','2014-07-03 00:24:54','2014-07-30 20:22:29'),(31,'facebook_login_redirect','','2014-07-03 00:24:54','2014-07-30 20:22:29'),(32,'show_login_with_joomla_reg','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(33,'social_force_scheme','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(34,'social_tags_always_parse','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(35,'social_toolbar_enable','1','2014-07-03 00:24:54','2014-07-30 20:22:29'),(36,'jquery_load','1','2014-07-03 00:24:54','2014-07-30 20:22:29'),(37,'cache_duration','15','2014-07-03 00:24:54','2014-07-30 20:22:29'),(38,'facebook_display_errors','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(39,'show_powered_by_link','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(40,'affiliate_id','','2014-07-03 00:24:54','2014-07-30 20:22:29'),(41,'facebook_auto_login','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(42,'login_use_popup','1','2014-07-03 00:24:54','2014-07-30 20:22:29'),(43,'facebook_login_show_modal','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(44,'logout_joomla_only','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(45,'facebook_perm_custom','','2014-07-03 00:24:54','2014-07-30 20:22:29'),(46,'facebook_curl_disable_ssl','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(47,'facebook_language_locale','','2014-07-03 00:24:54','2014-07-30 20:22:29'),(48,'facebook_login_button','icon_label.png','2014-07-03 00:24:54','2014-07-30 20:22:29'),(49,'github_login_button','icon_label.png','2014-07-03 00:24:54','2014-07-30 20:22:29'),(50,'google_openid_fallback','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(51,'google_login_button','icon_label.png','2014-07-03 00:24:54','2014-07-30 20:22:29'),(52,'instagram_callback_ssl','0','2014-07-03 00:24:54','2014-07-30 20:22:29'),(53,'instagram_login_button','icon_label.png','2014-07-03 00:24:54','2014-07-30 20:22:29'),(54,'linkedin_perm_custom','','2014-07-03 00:24:54','2014-07-30 20:22:29'),(55,'linkedin_login_button','icon_label.png','2014-07-03 00:24:54','2014-07-30 20:22:29'),(56,'twitter_login_button','icon_label.png','2014-07-03 00:24:54','2014-07-30 20:22:29'),(57,'vk_login_button','icon_label.png','2014-07-03 00:24:54','2014-07-30 20:22:29');
/*!40000 ALTER TABLE `mf0kn_jfbconnect_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_jfbconnect_notification`
--

DROP TABLE IF EXISTS `mf0kn_jfbconnect_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_jfbconnect_notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fb_request_id` bigint(20) NOT NULL,
  `fb_user_to` bigint(20) NOT NULL,
  `fb_user_from` bigint(20) NOT NULL,
  `jfbc_request_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_jfbconnect_notification`
--

LOCK TABLES `mf0kn_jfbconnect_notification` WRITE;
/*!40000 ALTER TABLE `mf0kn_jfbconnect_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_jfbconnect_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_jfbconnect_request`
--

DROP TABLE IF EXISTS `mf0kn_jfbconnect_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_jfbconnect_request` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `published` tinyint(4) NOT NULL,
  `title` varchar(50) NOT NULL,
  `message` varchar(250) NOT NULL,
  `destination_url` varchar(200) NOT NULL,
  `thanks_url` varchar(200) NOT NULL,
  `breakout_canvas` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_jfbconnect_request`
--

LOCK TABLES `mf0kn_jfbconnect_request` WRITE;
/*!40000 ALTER TABLE `mf0kn_jfbconnect_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_jfbconnect_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_jfbconnect_user_map`
--

DROP TABLE IF EXISTS `mf0kn_jfbconnect_user_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_jfbconnect_user_map` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `j_user_id` int(11) NOT NULL,
  `provider_user_id` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `access_token` text,
  `authorized` tinyint(1) DEFAULT '1',
  `params` text,
  `provider` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_jfbconnect_user_map`
--

LOCK TABLES `mf0kn_jfbconnect_user_map` WRITE;
/*!40000 ALTER TABLE `mf0kn_jfbconnect_user_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_jfbconnect_user_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_languages`
--

DROP TABLE IF EXISTS `mf0kn_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_languages` (
  `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` char(7) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_native` varchar(50) NOT NULL,
  `sef` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` varchar(512) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `sitename` varchar(1024) NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `idx_sef` (`sef`),
  UNIQUE KEY `idx_image` (`image`),
  UNIQUE KEY `idx_langcode` (`lang_code`),
  KEY `idx_access` (`access`),
  KEY `idx_ordering` (`ordering`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_languages`
--

LOCK TABLES `mf0kn_languages` WRITE;
/*!40000 ALTER TABLE `mf0kn_languages` DISABLE KEYS */;
INSERT INTO `mf0kn_languages` VALUES (1,'en-GB','English (UK)','English (UK)','en','en','','','','',1,1,1);
/*!40000 ALTER TABLE `mf0kn_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_menu`
--

DROP TABLE IF EXISTS `mf0kn_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(1024) NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`,`language`),
  KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  KEY `idx_menutype` (`menutype`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_path` (`path`(255)),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_menu`
--

LOCK TABLES `mf0kn_menu` WRITE;
/*!40000 ALTER TABLE `mf0kn_menu` DISABLE KEYS */;
INSERT INTO `mf0kn_menu` VALUES (1,'','Menu_Item_Root','root','','','','',1,0,0,0,0,'0000-00-00 00:00:00',0,0,'',0,'',0,79,0,'*',0),(2,'menu','com_banners','Banners','','Banners','index.php?option=com_banners','component',0,1,1,4,0,'0000-00-00 00:00:00',0,0,'class:banners',0,'',1,10,0,'*',1),(3,'menu','com_banners','Banners','','Banners/Banners','index.php?option=com_banners','component',0,2,2,4,0,'0000-00-00 00:00:00',0,0,'class:banners',0,'',2,3,0,'*',1),(4,'menu','com_banners_categories','Categories','','Banners/Categories','index.php?option=com_categories&extension=com_banners','component',0,2,2,6,0,'0000-00-00 00:00:00',0,0,'class:banners-cat',0,'',4,5,0,'*',1),(5,'menu','com_banners_clients','Clients','','Banners/Clients','index.php?option=com_banners&view=clients','component',0,2,2,4,0,'0000-00-00 00:00:00',0,0,'class:banners-clients',0,'',6,7,0,'*',1),(6,'menu','com_banners_tracks','Tracks','','Banners/Tracks','index.php?option=com_banners&view=tracks','component',0,2,2,4,0,'0000-00-00 00:00:00',0,0,'class:banners-tracks',0,'',8,9,0,'*',1),(7,'menu','com_contact','Contacts','','Contacts','index.php?option=com_contact','component',0,1,1,8,0,'0000-00-00 00:00:00',0,0,'class:contact',0,'',11,16,0,'*',1),(8,'menu','com_contact','Contacts','','Contacts/Contacts','index.php?option=com_contact','component',0,7,2,8,0,'0000-00-00 00:00:00',0,0,'class:contact',0,'',12,13,0,'*',1),(9,'menu','com_contact_categories','Categories','','Contacts/Categories','index.php?option=com_categories&extension=com_contact','component',0,7,2,6,0,'0000-00-00 00:00:00',0,0,'class:contact-cat',0,'',14,15,0,'*',1),(10,'menu','com_messages','Messaging','','Messaging','index.php?option=com_messages','component',0,1,1,15,0,'0000-00-00 00:00:00',0,0,'class:messages',0,'',17,22,0,'*',1),(11,'menu','com_messages_add','New Private Message','','Messaging/New Private Message','index.php?option=com_messages&task=message.add','component',0,10,2,15,0,'0000-00-00 00:00:00',0,0,'class:messages-add',0,'',18,19,0,'*',1),(12,'menu','com_messages_read','Read Private Message','','Messaging/Read Private Message','index.php?option=com_messages','component',0,10,2,15,0,'0000-00-00 00:00:00',0,0,'class:messages-read',0,'',20,21,0,'*',1),(13,'menu','com_newsfeeds','News Feeds','','News Feeds','index.php?option=com_newsfeeds','component',0,1,1,17,0,'0000-00-00 00:00:00',0,0,'class:newsfeeds',0,'',23,28,0,'*',1),(14,'menu','com_newsfeeds_feeds','Feeds','','News Feeds/Feeds','index.php?option=com_newsfeeds','component',0,13,2,17,0,'0000-00-00 00:00:00',0,0,'class:newsfeeds',0,'',24,25,0,'*',1),(15,'menu','com_newsfeeds_categories','Categories','','News Feeds/Categories','index.php?option=com_categories&extension=com_newsfeeds','component',0,13,2,6,0,'0000-00-00 00:00:00',0,0,'class:newsfeeds-cat',0,'',26,27,0,'*',1),(16,'menu','com_redirect','Redirect','','Redirect','index.php?option=com_redirect','component',0,1,1,24,0,'0000-00-00 00:00:00',0,0,'class:redirect',0,'',29,30,0,'*',1),(17,'menu','com_search','Basic Search','','Basic Search','index.php?option=com_search','component',0,1,1,19,0,'0000-00-00 00:00:00',0,0,'class:search',0,'',31,32,0,'*',1),(18,'menu','com_weblinks','Weblinks','','Weblinks','index.php?option=com_weblinks','component',0,1,1,21,0,'0000-00-00 00:00:00',0,0,'class:weblinks',0,'',33,38,0,'*',1),(19,'menu','com_weblinks_links','Links','','Weblinks/Links','index.php?option=com_weblinks','component',0,18,2,21,0,'0000-00-00 00:00:00',0,0,'class:weblinks',0,'',34,35,0,'*',1),(20,'menu','com_weblinks_categories','Categories','','Weblinks/Categories','index.php?option=com_categories&extension=com_weblinks','component',0,18,2,6,0,'0000-00-00 00:00:00',0,0,'class:weblinks-cat',0,'',36,37,0,'*',1),(21,'menu','com_finder','Smart Search','','Smart Search','index.php?option=com_finder','component',0,1,1,27,0,'0000-00-00 00:00:00',0,0,'class:finder',0,'',39,40,0,'*',1),(22,'menu','com_joomlaupdate','Joomla! Update','','Joomla! Update','index.php?option=com_joomlaupdate','component',1,1,1,28,0,'0000-00-00 00:00:00',0,0,'class:joomlaupdate',0,'',41,42,0,'*',1),(23,'main','com_tags','Tags','','Tags','index.php?option=com_tags','component',0,1,1,29,0,'0000-00-00 00:00:00',0,1,'class:tags',0,'',43,44,0,'',1),(24,'main','com_postinstall','Post-installation messages','','Post-installation messages','index.php?option=com_postinstall','component',0,1,1,32,0,'0000-00-00 00:00:00',0,1,'class:postinstall',0,'',45,46,0,'*',1),(101,'mainmenu','Home','home','','home','index.php?option=com_vppi&view=listings','component',1,1,1,10028,0,'0000-00-00 00:00:00',0,1,'',0,'{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":1,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"fusion_item_subtext\":\"\",\"fusion_customimage\":\"\",\"fusion_customclass\":\"\",\"fusion_columns\":\"1\",\"fusion_distribution\":\"even\",\"fusion_manual_distribution\":\"\",\"fusion_dropdown_width\":\"\",\"fusion_column_widths\":\"\",\"fusion_children_group\":\"0\",\"fusion_children_type\":\"menuitems\",\"fusion_modules\":\"87\",\"fusion_module_positions\":\"\",\"splitmenu_item_subtext\":\"\"}',47,48,1,'*',0),(103,'mainmenu','Home Search','homes','','homes','index.php?option=com_vppi&view=listings&layout=list','component',1,1,1,10028,0,'0000-00-00 00:00:00',0,1,'',0,'{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"fusion_item_subtext\":\"\",\"fusion_customimage\":\"\",\"fusion_customclass\":\"\",\"fusion_columns\":\"1\",\"fusion_distribution\":\"even\",\"fusion_manual_distribution\":\"\",\"fusion_dropdown_width\":\"\",\"fusion_column_widths\":\"\",\"fusion_children_group\":\"0\",\"fusion_children_type\":\"menuitems\",\"fusion_modules\":\"87\",\"fusion_module_positions\":\"\",\"splitmenu_item_subtext\":\"\"}',55,56,0,'*',0),(104,'mainmenu','About Us','about-us','','about-us','index.php?option=com_content&view=article&id=1','component',1,1,1,22,0,'0000-00-00 00:00:00',0,1,'',0,'{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"fusion_item_subtext\":\"\",\"fusion_customimage\":\"\",\"fusion_customclass\":\"\",\"fusion_columns\":\"1\",\"fusion_distribution\":\"even\",\"fusion_manual_distribution\":\"\",\"fusion_dropdown_width\":\"\",\"fusion_column_widths\":\"\",\"fusion_children_group\":\"0\",\"fusion_children_type\":\"menuitems\",\"fusion_modules\":\"87\",\"fusion_module_positions\":\"\",\"splitmenu_item_subtext\":\"\"}',49,54,0,'*',0),(129,'mainmenu','Bios','bios','','about-us/bios','index.php?option=com_content&view=article&id=2','component',-2,104,2,22,0,'0000-00-00 00:00:00',0,1,'',0,'{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"fusion_item_subtext\":\"\",\"fusion_customimage\":\"\",\"fusion_customclass\":\"\",\"fusion_columns\":\"1\",\"fusion_distribution\":\"even\",\"fusion_manual_distribution\":\"\",\"fusion_dropdown_width\":\"\",\"fusion_column_widths\":\"\",\"fusion_children_group\":\"0\",\"fusion_children_type\":\"menuitems\",\"fusion_modules\":\"87\",\"fusion_module_positions\":\"\",\"splitmenu_item_subtext\":\"\"}',50,51,0,'*',0),(130,'mainmenu','Mission Statement','mission-statement','','about-us/mission-statement','index.php?option=com_content&view=article&id=1','component',-2,104,2,22,0,'0000-00-00 00:00:00',0,1,'',0,'{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"fusion_item_subtext\":\"\",\"fusion_customimage\":\"\",\"fusion_customclass\":\"\",\"fusion_columns\":\"1\",\"fusion_distribution\":\"even\",\"fusion_manual_distribution\":\"\",\"fusion_dropdown_width\":\"\",\"fusion_column_widths\":\"\",\"fusion_children_group\":\"0\",\"fusion_children_type\":\"menuitems\",\"fusion_modules\":\"87\",\"fusion_module_positions\":\"\",\"splitmenu_item_subtext\":\"\"}',52,53,0,'*',0),(131,'mainmenu','Buyer Info','buyer-info','','buyer-info','index.php?option=com_content&view=article&id=3','component',1,1,1,22,0,'0000-00-00 00:00:00',0,1,'',0,'{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"fusion_item_subtext\":\"\",\"fusion_customimage\":\"\",\"fusion_customclass\":\"\",\"fusion_columns\":\"1\",\"fusion_distribution\":\"even\",\"fusion_manual_distribution\":\"\",\"fusion_dropdown_width\":\"\",\"fusion_column_widths\":\"\",\"fusion_children_group\":\"0\",\"fusion_children_type\":\"menuitems\",\"fusion_modules\":\"87\",\"fusion_module_positions\":\"\",\"splitmenu_item_subtext\":\"\"}',57,58,0,'*',0),(132,'mainmenu','Contact Us','contact-us','','contact-us','index.php?option=com_contact&view=featured','component',1,1,1,8,0,'0000-00-00 00:00:00',0,1,'',0,'{\"show_pagination_limit\":\"\",\"show_headings\":\"\",\"show_position_headings\":\"\",\"show_email_headings\":\"\",\"show_telephone_headings\":\"\",\"show_mobile_headings\":\"\",\"show_fax_headings\":\"\",\"show_suburb_headings\":\"\",\"show_state_headings\":\"\",\"show_country_headings\":\"\",\"show_pagination\":\"\",\"show_pagination_results\":\"\",\"presentation_style\":\"\",\"show_name\":\"\",\"show_position\":\"\",\"show_email\":\"\",\"show_street_address\":\"\",\"show_suburb\":\"\",\"show_state\":\"\",\"show_postcode\":\"\",\"show_country\":\"\",\"show_telephone\":\"\",\"show_mobile\":\"\",\"show_fax\":\"\",\"show_webpage\":\"\",\"show_misc\":\"\",\"show_image\":\"\",\"allow_vcard\":\"\",\"show_articles\":\"\",\"show_links\":\"\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"show_email_form\":\"\",\"show_email_copy\":\"\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"\",\"custom_reply\":\"\",\"redirect\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0,\"fusion_item_subtext\":\"\",\"fusion_customimage\":\"\",\"fusion_customclass\":\"\",\"fusion_columns\":\"1\",\"fusion_distribution\":\"even\",\"fusion_manual_distribution\":\"\",\"fusion_dropdown_width\":\"\",\"fusion_column_widths\":\"\",\"fusion_children_group\":\"0\",\"fusion_children_type\":\"menuitems\",\"fusion_modules\":\"89\",\"fusion_module_positions\":\"\",\"splitmenu_item_subtext\":\"\"}',59,60,0,'*',0),(147,'main','COM_XMAP_TITLE','com-xmap-title','','com-xmap-title','index.php?option=com_xmap','component',0,1,1,10045,0,'0000-00-00 00:00:00',0,1,'components/com_xmap/images/xmap-favicon.png',0,'',61,62,0,'',1),(162,'main','COM_VPPI','com-vppi','','com-vppi','index.php?option=com_vppi','component',0,1,1,10028,0,'0000-00-00 00:00:00',0,1,'../media/com_vppi/images/16.png',0,'',63,64,0,'',1),(172,'main','JCE','jce','','jce','index.php?option=com_jce','component',0,1,1,10025,0,'0000-00-00 00:00:00',0,1,'components/com_jce/media/img/menu/logo.png',0,'',65,74,0,'',1),(173,'main','WF_MENU_CPANEL','wf-menu-cpanel','','jce/wf-menu-cpanel','index.php?option=com_jce','component',0,172,2,10025,0,'0000-00-00 00:00:00',0,1,'components/com_jce/media/img/menu/jce-cpanel.png',0,'',66,67,0,'',1),(174,'main','WF_MENU_CONFIG','wf-menu-config','','jce/wf-menu-config','index.php?option=com_jce&view=config','component',0,172,2,10025,0,'0000-00-00 00:00:00',0,1,'components/com_jce/media/img/menu/jce-config.png',0,'',68,69,0,'',1),(175,'main','WF_MENU_PROFILES','wf-menu-profiles','','jce/wf-menu-profiles','index.php?option=com_jce&view=profiles','component',0,172,2,10025,0,'0000-00-00 00:00:00',0,1,'components/com_jce/media/img/menu/jce-profiles.png',0,'',70,71,0,'',1),(176,'main','WF_MENU_INSTALL','wf-menu-install','','jce/wf-menu-install','index.php?option=com_jce&view=installer','component',0,172,2,10025,0,'0000-00-00 00:00:00',0,1,'components/com_jce/media/img/menu/jce-install.png',0,'',72,73,0,'',1),(178,'main','COM_AKEEBA','com-akeeba','','com-akeeba','index.php?option=com_akeeba','component',1,1,1,10017,0,'0000-00-00 00:00:00',0,1,'../media/com_akeeba/icons/akeeba-16.png',0,'',75,76,0,'',1),(180,'main','COM_ADMINTOOLS','com-admintools','','com-admintools','index.php?option=com_admintools','component',1,1,1,10008,0,'0000-00-00 00:00:00',0,1,'../media/com_admintools/images/admintools-16.png',0,'',77,78,0,'',1);
/*!40000 ALTER TABLE `mf0kn_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_menu_types`
--

DROP TABLE IF EXISTS `mf0kn_menu_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL,
  `title` varchar(48) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_menutype` (`menutype`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_menu_types`
--

LOCK TABLES `mf0kn_menu_types` WRITE;
/*!40000 ALTER TABLE `mf0kn_menu_types` DISABLE KEYS */;
INSERT INTO `mf0kn_menu_types` VALUES (1,'mainmenu','Main Menu','The main menu for the site');
/*!40000 ALTER TABLE `mf0kn_menu_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_messages`
--

DROP TABLE IF EXISTS `mf0kn_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_messages`
--

LOCK TABLES `mf0kn_messages` WRITE;
/*!40000 ALTER TABLE `mf0kn_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_messages_cfg`
--

DROP TABLE IF EXISTS `mf0kn_messages_cfg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_messages_cfg`
--

LOCK TABLES `mf0kn_messages_cfg` WRITE;
/*!40000 ALTER TABLE `mf0kn_messages_cfg` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_messages_cfg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_modules`
--

DROP TABLE IF EXISTS `mf0kn_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) NOT NULL DEFAULT '',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_modules`
--

LOCK TABLES `mf0kn_modules` WRITE;
/*!40000 ALTER TABLE `mf0kn_modules` DISABLE KEYS */;
INSERT INTO `mf0kn_modules` VALUES (1,55,'Main Menu','','',1,'position-7',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_menu',1,1,'{\"menutype\":\"mainmenu\",\"startLevel\":\"0\",\"endLevel\":\"0\",\"showAllChildren\":\"0\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"\",\"moduleclass_sfx\":\"_menu\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}',0,'*'),(2,56,'Login','','',1,'login',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_login',1,1,'',1,'*'),(3,57,'Popular Articles','','',3,'cpanel',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_popular',3,1,'{\"count\":\"5\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}',1,'*'),(4,58,'Recently Added Articles','','',4,'cpanel',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_latest',3,1,'{\"count\":\"5\",\"ordering\":\"c_dsc\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}',1,'*'),(8,59,'Toolbar','','',1,'toolbar',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_toolbar',3,1,'',1,'*'),(9,60,'Quick Icons','','',1,'icon',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_quickicon',3,1,'',1,'*'),(10,61,'Logged-in Users','','',2,'cpanel',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_logged',3,1,'{\"count\":\"5\",\"name\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}',1,'*'),(12,62,'Admin Menu','','',1,'menu',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_menu',3,1,'{\"layout\":\"\",\"moduleclass_sfx\":\"\",\"shownew\":\"1\",\"showhelp\":\"1\",\"cache\":\"0\"}',1,'*'),(13,63,'Admin Submenu','','',1,'submenu',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_submenu',3,1,'',1,'*'),(14,64,'User Status','','',2,'status',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_status',3,1,'',1,'*'),(15,65,'Title','','',1,'title',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_title',3,1,'',1,'*'),(16,66,'Login Form','','',7,'position-7',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_login',1,1,'{\"greeting\":\"1\",\"name\":\"0\"}',0,'*'),(17,67,'Breadcrumbs','','',1,'position-2',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_breadcrumbs',1,1,'{\"moduleclass_sfx\":\"\",\"showHome\":\"1\",\"homeText\":\"\",\"showComponent\":\"1\",\"separator\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}',0,'*'),(79,68,'Multilanguage status','','',1,'status',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'mod_multilangstatus',3,1,'{\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}',1,'*'),(86,69,'Joomla Version','','',1,'footer',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_version',3,1,'{\"format\":\"short\",\"product\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}',1,'*'),(91,67,'Footer Icons','','<div style=\"text-align: center;\"><img src=\"images/eho.png\" alt=\"eho\" width=\"49\" height=\"50\" />&nbsp; &nbsp;<img src=\"images/rmls.png\" alt=\"rmls\" width=\"112\" height=\"50\" />&nbsp; &nbsp;<a href=\"http://portlandhousingcenter.org/\" target=\"_blank\"><img src=\"images/phc.png\" alt=\"phc\" width=\"69\" height=\"50\" /></a>&nbsp;&nbsp;&nbsp;<img src=\"images/oar.png\" alt=\"oar\" width=\"74\" height=\"50\" /></div>',1,'copyright-b',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_custom',1,0,'{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}',0,'*'),(92,68,'Facebook Bar','','<p>{SCOpenGraph image=http://www.vppihomes.com/images/logo-114.png} {JFBCFan height=200 width=908 colorscheme=light href=https://www.facebook.com/vppihomes show_faces=1 stream=0 header=0 show_border=0 force_wall=0 key=}</p>',1,'mainbottom-a',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'mod_custom',1,0,'{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}',0,'*'),(93,72,'Home Search Heading','','<h4>Browse <a href=\"/homes\">our listings</a> or search the <a href=\"http://www.rmls.com/RC2/UI/search_residential.asp\" target=\"_blank\">RMLS website</a>.</h4>',1,'maintop-a',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',-2,'mod_custom',1,0,'{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\" nopaddingbottom nomarginbottom nomargintop\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}',0,'*'),(94,79,'Contact Form','','',1,'contactform',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_contactform',1,0,'{\"fname\":\"required\",\"fnameLabel\":\"Name\",\"fnameValue\":\"\",\"fnameErrorMessage\":\"Please enter your name.\",\"lname\":\"required\",\"lnameLabel\":\"Last Name\",\"lnameValue\":\"\",\"lnameErrorMessage\":\"Please enter a last name.\",\"title\":\"hide\",\"titleLabel\":\"Title\",\"titleValue\":\"\",\"titleErrorMessage\":\"Please enter a title.\",\"org\":\"hide\",\"orgLabel\":\"Organization\",\"orgValue\":\"\",\"orgErrorMessage\":\"Please enter your associated organization.\",\"address\":\"hide\",\"addressLabel\":\"Address\",\"addressValue\":\"\",\"addressErrorMessage\":\"Please enter your address.\",\"address2\":\"hide\",\"address2Label\":\"Address Line 2\",\"address2Value\":\"\",\"address2ErrorMessage\":\"Please enter additional address details.\",\"city\":\"hide\",\"cityLabel\":\"City\",\"cityValue\":\"\",\"cityErrorMessage\":\"Please enter your city.\",\"state\":\"hide\",\"stateLabel\":\"State\",\"stateValue\":\"\",\"stateErrorMessage\":\"Please choose your state.\",\"states\":\"\",\"zip\":\"hide\",\"zipLabel\":\"Zip Code\",\"zipValue\":\"\",\"zipErrorMessage\":\"Please enter your zip code.\",\"phone\":\"hide\",\"phoneLabel\":\"Phone\",\"phoneValue\":\"\",\"phoneErrorMessage\":\"Please enter your phone number.\",\"email\":\"required\",\"emailLabel\":\"Email\",\"emailValue\":\"\",\"emailErrorMessage\":\"Please enter your email address.\",\"comments\":\"show\",\"commentsLabel\":\"Comments\",\"commentsValue\":\"\",\"commentsErrorMessage\":\"Please enter your comments.\",\"custom1\":\"hide\",\"custom1Label\":\"Custom 1\",\"custom1Value\":\"\",\"custom1ErrorMessage\":\"Please choose one.\",\"custom1Type\":\"select\",\"custom1Values\":\"One,Two,Three\",\"custom1Mask\":\"\",\"custom1HelpTitle\":\"\",\"custom1HelpText\":\"\",\"custom2\":\"hide\",\"custom2Label\":\"Custom 2\",\"custom2Value\":\"\",\"custom2ErrorMessage\":\"Please choose one.\",\"custom2Type\":\"select\",\"custom2Values\":\"One,Two,Three\",\"custom2Mask\":\"\",\"custom2HelpTitle\":\"\",\"custom2HelpText\":\"\",\"custom3\":\"hide\",\"custom3Label\":\"Custom 3\",\"custom3Value\":\"\",\"custom3ErrorMessage\":\"Please choose one.\",\"custom3Type\":\"select\",\"custom3Values\":\"One,Two,Three\",\"custom3Mask\":\"\",\"custom3HelpTitle\":\"\",\"custom3HelpText\":\"\",\"custom4\":\"hide\",\"custom4Label\":\"Custom 4\",\"custom4Value\":\"\",\"custom4ErrorMessage\":\"Please choose one.\",\"custom4Type\":\"select\",\"custom4Values\":\"One,Two,Three\",\"custom4Mask\":\"\",\"custom4HelpTitle\":\"\",\"custom4HelpText\":\"\",\"custom5\":\"hide\",\"custom5Label\":\"Custom 5\",\"custom5Value\":\"\",\"custom5ErrorMessage\":\"Please choose one.\",\"custom5Type\":\"select\",\"custom5Values\":\"One,Two,Three\",\"custom5Mask\":\"\",\"custom5HelpTitle\":\"\",\"custom5HelpText\":\"\",\"captcha\":\"show\",\"captcha-public\":\"6LeUxMUSAAAAAIm_9_Gfp-QUzsTJFQHzMPJ2wdsn\",\"captcha-private\":\"6LeUxMUSAAAAABWv7Z5ldYClGFu2HsWwHYViWHwl\",\"moduleclass_sfx\":\"\",\"sendemail\":\"jcbrandsmith@gmail.com\",\"emailsubject\":\"\",\"table\":\"#__contactform\",\"successnotice\":\"Submission received successfully.  Thank you.\",\"dberror\":\"We are sorry for the inconvenience, but there was an error saving your data.  Please try again.\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}',0,'*'),(95,80,'Map','','',1,'sidebar-a',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_wrapper',1,0,'{\"url\":\"https:\\/\\/maps.google.com\\/maps?f=q&source=s_q&hl=en&geocode=&q=Vantage+Point+Properties+Inc,+Portland,+OR&aq=0&oq=Vantage+Point+Pro&sll=45.586173,-122.639008&sspn=0.396443,0.891953&t=h&ie=UTF8&hq=Vantage+Point+Properties+Inc,&hnear=Portland,+Multnomah,+Oregon&ll=45.532567,-122.680893&spn=0.051106,0.072956&z=13&iwloc=A&output=embed\",\"add\":\"0\",\"scrolling\":\"no\",\"width\":\"100%\",\"height\":\"350\",\"height_auto\":\"0\",\"frameborder\":\"1\",\"target\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}',0,'*'),(96,81,'Facebook Sidebar','','<p>{SCOpenGraph image=http://www.vppihomes.com/images/logo-114.png} {JFBCFan height=200 width=908 colorscheme=light href=https://www.facebook.com/vppihomes show_faces=1 stream=0 header=0 show_border=0 force_wall=0 key=}</p>',1,'sidebar-a',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'mod_custom',1,0,'{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}',0,'*'),(98,85,'VPPI Admin Icon','','',1,'cpanel',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_vppiadminicon',1,1,'{\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}',1,'*'),(99,86,'Facebook Footer','','',1,'copyright-a',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_wrapper',1,0,'{\"url\":\"\\/\\/www.facebook.com\\/plugins\\/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fvppihomes&send=false&layout=standard&width=300&show_faces=true&action=like&colorscheme=light&font&height=80&appId=449484815070477\",\"add\":\"0\",\"scrolling\":\"auto\",\"width\":\"100%\",\"height\":\"80\",\"height_auto\":\"0\",\"frameborder\":\"0\",\"target\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}',0,'*'),(100,87,'Facebook Sidebar Wrapper','','',1,'sidebar-a',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_wrapper',1,0,'{\"url\":\"\\/\\/www.facebook.com\\/plugins\\/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fvppihomes&send=false&layout=standard&width=300&show_faces=true&action=like&colorscheme=light&font&height=80&appId=449484815070477\",\"add\":\"0\",\"scrolling\":\"auto\",\"width\":\"100%\",\"height\":\"80\",\"height_auto\":\"0\",\"frameborder\":\"0\",\"target\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}',0,'*');
/*!40000 ALTER TABLE `mf0kn_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_modules_menu`
--

DROP TABLE IF EXISTS `mf0kn_modules_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_modules_menu`
--

LOCK TABLES `mf0kn_modules_menu` WRITE;
/*!40000 ALTER TABLE `mf0kn_modules_menu` DISABLE KEYS */;
INSERT INTO `mf0kn_modules_menu` VALUES (1,0),(2,0),(3,0),(4,0),(6,0),(7,0),(8,0),(9,0),(10,0),(12,0),(13,0),(14,0),(15,0),(16,0),(17,0),(79,0),(86,0),(91,0),(92,101),(93,103),(94,0),(95,132),(96,104),(96,129),(96,130),(96,131),(98,0),(99,0),(100,131);
/*!40000 ALTER TABLE `mf0kn_modules_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_newsfeeds`
--

DROP TABLE IF EXISTS `mf0kn_newsfeeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `link` varchar(200) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(10) unsigned NOT NULL DEFAULT '3600',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `images` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_newsfeeds`
--

LOCK TABLES `mf0kn_newsfeeds` WRITE;
/*!40000 ALTER TABLE `mf0kn_newsfeeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_newsfeeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_oneclickaction_actions`
--

DROP TABLE IF EXISTS `mf0kn_oneclickaction_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_oneclickaction_actions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) unsigned NOT NULL,
  `actionurl` varchar(4000) NOT NULL,
  `otp` char(64) NOT NULL,
  `expiry` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_oneclickaction_actions`
--

LOCK TABLES `mf0kn_oneclickaction_actions` WRITE;
/*!40000 ALTER TABLE `mf0kn_oneclickaction_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_oneclickaction_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_opengraph_action`
--

DROP TABLE IF EXISTS `mf0kn_opengraph_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_opengraph_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `plugin` varchar(20) DEFAULT NULL,
  `system_name` varchar(20) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `action` varchar(20) DEFAULT NULL,
  `fb_built_in` tinyint(1) DEFAULT NULL,
  `can_disable` tinyint(1) DEFAULT NULL,
  `params` text,
  `published` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_opengraph_action`
--

LOCK TABLES `mf0kn_opengraph_action` WRITE;
/*!40000 ALTER TABLE `mf0kn_opengraph_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_opengraph_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_opengraph_action_object`
--

DROP TABLE IF EXISTS `mf0kn_opengraph_action_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_opengraph_action_object` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_opengraph_action_object`
--

LOCK TABLES `mf0kn_opengraph_action_object` WRITE;
/*!40000 ALTER TABLE `mf0kn_opengraph_action_object` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_opengraph_action_object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_opengraph_activity`
--

DROP TABLE IF EXISTS `mf0kn_opengraph_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_opengraph_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `action_id` int(11) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `url` text,
  `status` tinyint(2) DEFAULT NULL,
  `unique_key` varchar(32) DEFAULT NULL,
  `response` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_opengraph_activity`
--

LOCK TABLES `mf0kn_opengraph_activity` WRITE;
/*!40000 ALTER TABLE `mf0kn_opengraph_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_opengraph_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_opengraph_object`
--

DROP TABLE IF EXISTS `mf0kn_opengraph_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_opengraph_object` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `plugin` varchar(15) DEFAULT NULL,
  `system_name` varchar(20) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `fb_built_in` int(1) DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `params` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_opengraph_object`
--

LOCK TABLES `mf0kn_opengraph_object` WRITE;
/*!40000 ALTER TABLE `mf0kn_opengraph_object` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_opengraph_object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_overrider`
--

DROP TABLE IF EXISTS `mf0kn_overrider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_overrider` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `constant` varchar(255) NOT NULL,
  `string` text NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_overrider`
--

LOCK TABLES `mf0kn_overrider` WRITE;
/*!40000 ALTER TABLE `mf0kn_overrider` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_overrider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_postinstall_messages`
--

DROP TABLE IF EXISTS `mf0kn_postinstall_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_postinstall_messages` (
  `postinstall_message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) NOT NULL DEFAULT '',
  `language_extension` varchar(255) NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`postinstall_message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_postinstall_messages`
--

LOCK TABLES `mf0kn_postinstall_messages` WRITE;
/*!40000 ALTER TABLE `mf0kn_postinstall_messages` DISABLE KEYS */;
INSERT INTO `mf0kn_postinstall_messages` VALUES (1,700,'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE','PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY','PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION','plg_twofactorauth_totp',1,'action','site://plugins/twofactorauth/totp/postinstall/actions.php','twofactorauth_postinstall_action','site://plugins/twofactorauth/totp/postinstall/actions.php','twofactorauth_postinstall_condition','3.2.0',0),(2,700,'COM_CPANEL_MSG_EACCELERATOR_TITLE','COM_CPANEL_MSG_EACCELERATOR_BODY','COM_CPANEL_MSG_EACCELERATOR_BUTTON','com_cpanel',1,'action','admin://components/com_admin/postinstall/eaccelerator.php','admin_postinstall_eaccelerator_action','admin://components/com_admin/postinstall/eaccelerator.php','admin_postinstall_eaccelerator_condition','3.2.0',1),(3,700,'COM_CPANEL_WELCOME_BEGINNERS_TITLE','COM_CPANEL_WELCOME_BEGINNERS_MESSAGE','','com_cpanel',1,'message','','','','','3.2.0',0),(4,700,'COM_CPANEL_MSG_PHPVERSION_TITLE','COM_CPANEL_MSG_PHPVERSION_BODY','','com_cpanel',1,'message','','','admin://components/com_admin/postinstall/phpversion.php','admin_postinstall_phpversion_condition','3.2.2',1),(5,10017,'AKEEBA_POSTSETUP_LBL_SRP','AKEEBA_POSTSETUP_DESC_SRP','AKEEBA_POSTSETUP_BTN_ENABLE_FEATURE','com_akeeba',1,'action','admin://components/com_akeeba/helpers/postinstall.php','com_akeeba_postinstall_srp_action','admin://components/com_akeeba/helpers/postinstall.php','com_akeeba_postinstall_srp_condition','4.0.0',1),(6,10017,'AKEEBA_POSTSETUP_LBL_BACKUPONUPDATE','AKEEBA_POSTSETUP_DESC_BACKUPONUPDATE','AKEEBA_POSTSETUP_BTN_ENABLE_FEATURE','com_akeeba',1,'action','admin://components/com_akeeba/helpers/postinstall.php','com_akeeba_postinstall_backuponupdate_action','admin://components/com_akeeba/helpers/postinstall.php','com_akeeba_postinstall_backuponupdate_condition','4.0.0',1),(7,10017,'AKEEBA_POSTSETUP_LBL_CONFWIZ','AKEEBA_POSTSETUP_DESC_CONFWIZ','AKEEBA_POSTSETUP_BTN_RUN_CONFWIZ','com_akeeba',1,'action','admin://components/com_akeeba/helpers/postinstall.php','com_akeeba_postinstall_confwiz_action','admin://components/com_akeeba/helpers/postinstall.php','com_akeeba_postinstall_confwiz_condition','4.0.0',1),(8,10017,'AKEEBA_POSTSETUP_LBL_ANGIEUPGRADE','AKEEBA_POSTSETUP_DESC_ANGIEUPGRADE','AKEEBA_POSTSETUP_BTN_ANGIEUPGRADE','com_akeeba',1,'action','admin://components/com_akeeba/helpers/postinstall.php','com_akeeba_postinstall_angie_action','admin://components/com_akeeba/helpers/postinstall.php','com_akeeba_postinstall_angie_condition','4.0.0',1),(9,10017,'AKEEBA_POSTSETUP_LBL_ACCEPTLICENSE','AKEEBA_POSTSETUP_DESC_ACCEPTLICENSE','AKEEBA_POSTSETUP_BTN_I_CONFIRM_THIS','com_akeeba',1,'message','','','','','4.0.0',0),(10,10017,'AKEEBA_POSTSETUP_LBL_ACCEPTSUPPORT','AKEEBA_POSTSETUP_DESC_ACCEPTSUPPORT','AKEEBA_POSTSETUP_BTN_I_CONFIRM_THIS','com_akeeba',1,'message','','','','','4.0.0',1),(11,10017,'AKEEBA_POSTSETUP_LBL_ACCEPTBACKUPTEST','AKEEBA_POSTSETUP_DESC_ACCEPTBACKUPTEST','AKEEBA_POSTSETUP_BTN_I_CONFIRM_THIS','com_akeeba',1,'message','','','','','4.0.0',1),(12,10008,'COM_ADMINTOOLS_POSTSETUP_LBL_ACCEPTLICENSE','COM_ADMINTOOLS_POSTSETUP_DESC_ACCEPTLICENSE','COM_ADMINTOOLS_POSTSETUP_BTN_I_CONFIRM_THIS','com_admintools',1,'message','','','','','3.1.2',1),(13,10008,'COM_ADMINTOOLS_POSTSETUP_LBL_ACCEPTSUPPORT','COM_ADMINTOOLS_POSTSETUP_DESC_ACCEPTSUPPORT','COM_ADMINTOOLS_POSTSETUP_BTN_I_CONFIRM_THIS','com_admintools',1,'message','','','','','3.1.2',1),(14,700,'COM_CPANEL_MSG_HTACCESS_TITLE','COM_CPANEL_MSG_HTACCESS_BODY','','com_cpanel',1,'message','','','admin://components/com_admin/postinstall/htaccess.php','admin_postinstall_htaccess_condition','3.4.0',0),(15,700,'COM_CPANEL_MSG_ROBOTS_TITLE','COM_CPANEL_MSG_ROBOTS_BODY','','com_cpanel',1,'message','','','','','3.3.0',0),(16,700,'COM_CPANEL_MSG_LANGUAGEACCESS340_TITLE','COM_CPANEL_MSG_LANGUAGEACCESS340_BODY','','com_cpanel',1,'message','','','admin://components/com_admin/postinstall/languageaccess340.php','admin_postinstall_languageaccess340_condition','3.4.1',1);
/*!40000 ALTER TABLE `mf0kn_postinstall_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_redirect_links`
--

DROP TABLE IF EXISTS `mf0kn_redirect_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_redirect_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `old_url` varchar(255) NOT NULL,
  `new_url` varchar(255) DEFAULT NULL,
  `referer` varchar(150) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_link_old` (`old_url`),
  KEY `idx_link_modifed` (`modified_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_redirect_links`
--

LOCK TABLES `mf0kn_redirect_links` WRITE;
/*!40000 ALTER TABLE `mf0kn_redirect_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_redirect_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_schemas`
--

DROP TABLE IF EXISTS `mf0kn_schemas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) NOT NULL,
  PRIMARY KEY (`extension_id`,`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_schemas`
--

LOCK TABLES `mf0kn_schemas` WRITE;
/*!40000 ALTER TABLE `mf0kn_schemas` DISABLE KEYS */;
INSERT INTO `mf0kn_schemas` VALUES (700,'3.4.0-2015-02-26');
/*!40000 ALTER TABLE `mf0kn_schemas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_session`
--

DROP TABLE IF EXISTS `mf0kn_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_session` (
  `session_id` varchar(200) NOT NULL DEFAULT '',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `guest` tinyint(4) unsigned DEFAULT '1',
  `time` varchar(14) DEFAULT '',
  `data` mediumtext,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) DEFAULT '',
  PRIMARY KEY (`session_id`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_session`
--

LOCK TABLES `mf0kn_session` WRITE;
/*!40000 ALTER TABLE `mf0kn_session` DISABLE KEYS */;
INSERT INTO `mf0kn_session` VALUES ('53mkudvpcv89f2gs4d230llng3',0,1,'1479978009','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479978009;s:18:\"session.timer.last\";i:1479978009;s:17:\"session.timer.now\";i:1479978009;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('6k6967jbbie59cfmg40dh10lj5',0,1,'1479977109','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479977109;s:18:\"session.timer.last\";i:1479977109;s:17:\"session.timer.now\";i:1479977109;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('78r954gcumha5hal0lu7kor411',0,1,'1479975009','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479975009;s:18:\"session.timer.last\";i:1479975009;s:17:\"session.timer.now\";i:1479975009;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('8gqg2c5efkirl7o0bvv7juemq2',0,1,'1479976809','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479976808;s:18:\"session.timer.last\";i:1479976808;s:17:\"session.timer.now\";i:1479976808;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('ckk0iqf65f53selc71amr1kvm0',0,1,'1479974709','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479974709;s:18:\"session.timer.last\";i:1479974709;s:17:\"session.timer.now\";i:1479974709;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('dhok2r1cpigjgdkami88vhch84',0,1,'1479975309','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479975309;s:18:\"session.timer.last\";i:1479975309;s:17:\"session.timer.now\";i:1479975309;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('futdqpk96v9s9btr2so1i8oeg0',0,1,'1479975909','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479975909;s:18:\"session.timer.last\";i:1479975909;s:17:\"session.timer.now\";i:1479975909;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('g0qjk50rcukhcvlps6gif0cua1',0,1,'1479977709','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479977709;s:18:\"session.timer.last\";i:1479977709;s:17:\"session.timer.now\";i:1479977709;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('n72n339q1iiudci53qkjemn0a7',0,1,'1479976209','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479976209;s:18:\"session.timer.last\";i:1479976209;s:17:\"session.timer.now\";i:1479976209;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('opv17ioqal9m5ksn9e0jhujpi0',0,1,'1479976509','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479976509;s:18:\"session.timer.last\";i:1479976509;s:17:\"session.timer.now\";i:1479976509;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('senis7ovdn8nmg1u73bchnh2b1',0,1,'1479977409','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479977409;s:18:\"session.timer.last\";i:1479977409;s:17:\"session.timer.now\";i:1479977409;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('ss6e75qlca5q62oi2rfhmepjs5',0,1,'1479975609','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479975609;s:18:\"session.timer.last\";i:1479975609;s:17:\"session.timer.now\";i:1479975609;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,''),('v78ndsoogau808712m2oe22t02',0,1,'1479978309','__default|a:8:{s:15:\"session.counter\";i:1;s:19:\"session.timer.start\";i:1479978309;s:18:\"session.timer.last\";i:1479978309;s:17:\"session.timer.now\";i:1479978309;s:24:\"session.client.forwarded\";s:14:\"69.162.124.232\";s:22:\"session.client.browser\";s:70:\"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\";s:8:\"registry\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:4:\"user\";O:5:\"JUser\":26:{s:9:\"\\0\\0\\0isRoot\";N;s:2:\"id\";i:0;s:4:\"name\";N;s:8:\"username\";N;s:5:\"email\";N;s:8:\"password\";N;s:14:\"password_clear\";s:0:\"\";s:5:\"block\";N;s:9:\"sendEmail\";i:0;s:12:\"registerDate\";N;s:13:\"lastvisitDate\";N;s:10:\"activation\";N;s:6:\"params\";N;s:6:\"groups\";a:1:{i:0;s:1:\"9\";}s:5:\"guest\";i:1;s:13:\"lastResetTime\";N;s:10:\"resetCount\";N;s:12:\"requireReset\";N;s:10:\"\\0\\0\\0_params\";O:24:\"Joomla\\Registry\\Registry\":2:{s:7:\"\\0\\0\\0data\";O:8:\"stdClass\":0:{}s:9:\"separator\";s:1:\".\";}s:14:\"\\0\\0\\0_authGroups\";N;s:14:\"\\0\\0\\0_authLevels\";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:\"\\0\\0\\0_authActions\";N;s:12:\"\\0\\0\\0_errorMsg\";N;s:13:\"\\0\\0\\0userHelper\";O:18:\"JUserWrapperHelper\":0:{}s:10:\"\\0\\0\\0_errors\";a:0:{}s:3:\"aid\";i:0;}}',0,'');
/*!40000 ALTER TABLE `mf0kn_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_tags`
--

DROP TABLE IF EXISTS `mf0kn_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tag_idx` (`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_tags`
--

LOCK TABLES `mf0kn_tags` WRITE;
/*!40000 ALTER TABLE `mf0kn_tags` DISABLE KEYS */;
INSERT INTO `mf0kn_tags` VALUES (1,0,0,1,0,'','ROOT','root','','',1,0,'0000-00-00 00:00:00',1,'','','','',0,'2011-01-01 00:00:01','',0,'0000-00-00 00:00:00','','',0,'*',1,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `mf0kn_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_template_styles`
--

DROP TABLE IF EXISTS `mf0kn_template_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_template_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(50) NOT NULL DEFAULT '',
  `client_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `home` char(7) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_template` (`template`),
  KEY `idx_home` (`home`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_template_styles`
--

LOCK TABLES `mf0kn_template_styles` WRITE;
/*!40000 ALTER TABLE `mf0kn_template_styles` DISABLE KEYS */;
INSERT INTO `mf0kn_template_styles` VALUES (4,'beez3',0,'0','Beez3 - Default','{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"logo\":\"images\\/joomla_black.gif\",\"sitetitle\":\"Joomla!\",\"sitedescription\":\"Open Source Content Management\",\"navposition\":\"left\",\"templatecolor\":\"personal\",\"html5\":\"0\"}'),(5,'hathor',1,'0','Hathor - Default','{\"showSiteName\":\"0\",\"colourChoice\":\"\",\"boldText\":\"0\"}'),(7,'protostar',0,'0','protostar - Default','{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}'),(8,'isis',1,'1','isis - Default','{\"templateColor\":\"\",\"logoFile\":\"\"}'),(10,'rt_voxel_responsive',0,'1','Voxel - Default','{\"master\":\"true\",\"current_id\":\"10\",\"template_full_name\":\"Voxel\",\"grid_system\":\"12\",\"template_prefix\":\"voxel-\",\"cookie_time\":\"31536000\",\"name\":\"Preset1\",\"copy_lang_files_if_diff\":\"1\",\"main\":{\"style\":\"light\",\"pattern\":\"diagonal\"},\"accent\":{\"colorchooser\":\"#6e7e29\",\"overlay\":\"light\"},\"stylepanel\":{\"enabled\":\"0\",\"position\":\"panel\"},\"loadtransition\":\"1\",\"font\":{\"family\":\"s:voxel\",\"size\":\"default\",\"size-is\":\"default\"},\"viewswitcher-priority\":\"1\",\"logo-priority\":\"2\",\"copyright-priority\":\"3\",\"styledeclaration-priority\":\"4\",\"date-priority\":\"6\",\"fontsizer-priority\":\"7\",\"totop-priority\":\"8\",\"systemmessages-priority\":\"9\",\"inactive-priority\":\"10\",\"morearticles-priority\":\"12\",\"smartload-priority\":\"13\",\"pagesuffix-priority\":\"14\",\"resetsettings-priority\":\"15\",\"analytics-priority\":\"16\",\"fusionmenu-priority\":\"18\",\"jstools-priority\":\"21\",\"rtl-priority\":\"23\",\"splitmenu-priority\":\"24\",\"touchmenu-priority\":\"26\",\"styledeclaration-enabled\":\"1\",\"logo\":{\"enabled\":\"1\",\"position\":\"header-a\",\"type\":\"custom\",\"voxel\":{\"logostyle\":\"style5\"},\"custom\":{\"image\":\"{\'path\':\'images\\/logo-horiz.png\'}\"}},\"social\":{\"enabled\":\"0\",\"position\":\"header-b\",\"twitter\":\"\",\"facebook\":\"\",\"buzz\":\"\",\"rss\":\"\"},\"date\":{\"enabled\":\"0\",\"position\":\"utility-a\",\"clientside\":\"0\",\"formats\":\"%A, %B %d, %Y\"},\"fontsizer\":{\"enabled\":\"0\",\"position\":\"utility-b\"},\"login\":{\"enabled\":\"0\",\"position\":\"utility-c\",\"text\":\"Member Login\",\"logouttext\":\"Logout\"},\"popup\":{\"enabled\":\"0\",\"position\":\"utility-d\",\"text\":\"Popup Module\",\"width\":\"250\",\"height\":\"235\"},\"branding\":{\"enabled\":\"0\",\"position\":\"copyright-c\"},\"copyright\":{\"enabled\":\"1\",\"position\":\"copyright-c\",\"text\":\"&copy; 2002 &ndash; 2014, Vantage Point Properties, Inc.\",\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"smartload\":{\"enabled\":\"1\",\"text\":\"200\",\"ignores\":\"com_community,com_contact,com_k2,com_tienda,com_weblinks\",\"exclusion\":\"\"},\"morearticles\":{\"enabled\":\"0\",\"text\":\"Load More Articles\",\"pagination\":\"0\"},\"totop\":{\"enabled\":\"0\",\"position\":\"copyright-b\",\"text\":\"Top\"},\"systemmessages\":{\"enabled\":\"1\",\"position\":\"drawer\"},\"resetsettings\":{\"enabled\":\"0\",\"position\":\"footer-b\",\"text\":\"Reset Settings\"},\"analytics\":{\"enabled\":\"1\",\"code\":\"UA-4406128-1\",\"position\":\"analytics\"},\"menu\":{\"enabled\":\"1\",\"type\":\"fusionmenu\",\"fusionmenu\":{\"menutype\":\"mainmenu\",\"position\":\"navigation-a\",\"enable_js\":\"1\",\"opacity\":\"1\",\"effect\":\"slidefade\",\"hidedelay\":\"500\",\"menu-animation\":\"Circ.easeOut\",\"menu-duration\":\"300\",\"centered-offset\":\"0\",\"tweak-initial-x\":\"-10\",\"tweak-initial-y\":\"-12\",\"tweak-subsequent-x\":\"-8\",\"tweak-subsequent-y\":\"-11\",\"tweak-width\":\"18\",\"tweak-height\":\"20\",\"enable-current-id\":\"0\",\"theme\":\"gantry-fusion\",\"limit_levels\":\"0\",\"startLevel\":\"0\",\"showAllChildren\":\"1\",\"class_sfx\":\"top\",\"cache\":\"0\",\"module_cache\":\"1\"},\"splitmenu\":{\"roknavmenu_fusion_enable-current-id\":\"0\",\"menutype\":\"mainmenu\",\"theme\":\"gantry-splitmenu\",\"cache\":\"0\",\"mainmenu-position\":\"navigation-a\",\"mainmenu-limit_levels\":\"1\",\"mainmenu-startLevel\":\"0\",\"mainmenu-endLevel\":\"0\",\"mainmenu-class_sfx\":\"top\",\"submenu-position\":\"subnavigation\",\"submenu-limit_levels\":\"1\",\"submenu-startLevel\":\"1\",\"submenu-endLevel\":\"1\",\"submenu-class_sfx\":\"\",\"sidemenu-position\":\"sidebar-a\",\"sidemenu-title\":\"1\",\"sidemenu-class_sfx\":\"\",\"sidemenu-module_sfx\":\"\",\"sidemenu-limit_levels\":\"1\",\"sidemenu-startLevel\":\"2\",\"sidemenu-endLevel\":\"9\",\"sidemenu-fulllevel\":\"0\",\"module_cache\":\"1\"}},\"top\":{\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"navigation\":{\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"header\":{\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"showcase\":{\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"feature\":{\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"utility\":{\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"maintop\":{\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"mainbodyPosition\":\"a:1:{i:12;a:4:{i:1;a:1:{s:2:\\\"mb\\\";i:12;}i:2;a:2:{s:2:\\\"mb\\\";i:7;s:2:\\\"sa\\\";i:5;}i:3;a:3:{s:2:\\\"mb\\\";i:6;s:2:\\\"sa\\\";i:3;s:2:\\\"sb\\\";i:3;}i:4;a:4:{s:2:\\\"mb\\\";i:3;s:2:\\\"sa\\\";i:3;s:2:\\\"sb\\\";i:3;s:2:\\\"sc\\\";i:3;}}}\",\"mainbottom\":{\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"extension\":{\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"bottom\":{\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"footer\":{\"layout\":\"3,3,3,3\",\"showall\":\"0\",\"showmax\":\"6\"},\"layout-mode\":\"responsive\",\"cache\":{\"enabled\":\"0\",\"time\":\"900\"},\"component-enabled\":\"1\",\"mainbody-enabled\":\"1\",\"rtl-enabled\":\"1\",\"pagesuffix-enabled\":\"1\",\"typography\":{\"enabled\":\"1\",\"style\":\"light\"},\"k2\":\"1\",\"selectivizr-enabled\":\"1\"}');
/*!40000 ALTER TABLE `mf0kn_template_styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_ucm_base`
--

DROP TABLE IF EXISTS `mf0kn_ucm_base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_ucm_base` (
  `ucm_id` int(10) unsigned NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL,
  PRIMARY KEY (`ucm_id`),
  KEY `idx_ucm_item_id` (`ucm_item_id`),
  KEY `idx_ucm_type_id` (`ucm_type_id`),
  KEY `idx_ucm_language_id` (`ucm_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_ucm_base`
--

LOCK TABLES `mf0kn_ucm_base` WRITE;
/*!40000 ALTER TABLE `mf0kn_ucm_base` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_ucm_base` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_ucm_content`
--

DROP TABLE IF EXISTS `mf0kn_ucm_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_ucm_content` (
  `core_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `core_type_alias` varchar(255) NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(255) NOT NULL,
  `core_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `core_body` mediumtext NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) NOT NULL DEFAULT '',
  `core_checked_out_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_access` int(10) unsigned NOT NULL DEFAULT '0',
  `core_params` text NOT NULL,
  `core_featured` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) NOT NULL,
  `core_publish_up` datetime NOT NULL,
  `core_publish_down` datetime NOT NULL,
  `core_content_item_id` int(10) unsigned DEFAULT NULL COMMENT 'ID from the individual type table',
  `asset_id` int(10) unsigned DEFAULT NULL COMMENT 'FK to the #__assets table.',
  `core_images` text NOT NULL,
  `core_urls` text NOT NULL,
  `core_hits` int(10) unsigned NOT NULL DEFAULT '0',
  `core_version` int(10) unsigned NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text NOT NULL,
  `core_metadesc` text NOT NULL,
  `core_catid` int(10) unsigned NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`core_content_id`),
  KEY `tag_idx` (`core_state`,`core_access`),
  KEY `idx_access` (`core_access`),
  KEY `idx_alias` (`core_alias`),
  KEY `idx_language` (`core_language`),
  KEY `idx_title` (`core_title`),
  KEY `idx_modified_time` (`core_modified_time`),
  KEY `idx_created_time` (`core_created_time`),
  KEY `idx_content_type` (`core_type_alias`),
  KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  KEY `idx_core_created_user_id` (`core_created_user_id`),
  KEY `idx_core_type_id` (`core_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains core content data in name spaced fields';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_ucm_content`
--

LOCK TABLES `mf0kn_ucm_content` WRITE;
/*!40000 ALTER TABLE `mf0kn_ucm_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_ucm_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_ucm_history`
--

DROP TABLE IF EXISTS `mf0kn_ucm_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_ucm_history` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ucm_item_id` int(10) unsigned NOT NULL,
  `ucm_type_id` int(10) unsigned NOT NULL,
  `version_note` varchar(255) NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `character_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep',
  PRIMARY KEY (`version_id`),
  KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  KEY `idx_save_date` (`save_date`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_ucm_history`
--

LOCK TABLES `mf0kn_ucm_history` WRITE;
/*!40000 ALTER TABLE `mf0kn_ucm_history` DISABLE KEYS */;
INSERT INTO `mf0kn_ucm_history` VALUES (1,1,1,'','2014-07-03 00:47:25',404,2509,'882e67924d01b3e148a69c155636e88ec57f5058','{\"id\":1,\"asset_id\":69,\"title\":\"Mission Statement\",\"alias\":\"mission-statement\",\"introtext\":\"<p>To serve the real estate needs of our clients, customers and our community, with the emphasis on cooperation, teamwork and utmost integrity. Our company has been part of the Portland real estate community since 2002. Our market areas include close-in, east- and west-sides, North Portland, Gresham, and East County neighborhoods. With a long history working with first-time buyers, we volunteer at the <a href=\\\"http:\\/\\/portlandhousingcenter.org\\/\\\" target=\\\"_blank\\\">Portland Housing Center<\\/a>.<\\/p>\\r\\n<p>In addition to our expertise with single-family homes, we are well-versed in condominium and small-income unit sales and commercial leasing. Again, we believe in working as a team with our buyers, lenders, home inspectors and escrow agents to provide complete and well-rounded service to our clients. Our goal is <em>your<\\/em> success.<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2014-07-03 00:47:25\",\"created_by\":\"404\",\"created_by_alias\":\"\",\"modified\":\"\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2014-07-03 00:47:25\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(2,2,1,'','2014-07-03 00:49:56',404,5256,'d35bffe61a1ab1f57547e2b7ab47abba0fe987af','{\"id\":2,\"asset_id\":70,\"title\":\"Vantage Point Brokers\",\"alias\":\"vantage-point-brokers\",\"introtext\":\"<div class=\\\"contactinfo\\\"><a href=\\\"mailto:gary@vppihomes.com\\\">gary@vppihomes.com<\\/a><br \\/>Cell: <a href=\\\"tel:5038603740\\\">503-860-3740<\\/a><br \\/>Office: 503-243-4620<br \\/>Fax: 503-243-4621<br \\/>Home: 503-663-1357<\\/div>\\r\\n<div>\\r\\n<p><img style=\\\"float: left; margin-right: 15px; margin-bottom: 10px;\\\" src=\\\"images\\/gary.jpg\\\" alt=\\\"Gary Slac\\\" width=\\\"129\\\" height=\\\"181\\\" \\/><span style=\\\"font-size: x-large;\\\">Gary Slac<\\/span><br \\/><br \\/> I am the owner and Principal Broker of Vantage Point Properties, Inc., a family owned business since 2002. I\'ve been an active, full-time agent for over 25 years. I am a first-time buyer specialist. After all these years, there is nothing more satisfying than helping someone find and purchase their first home. I\'ve been proudly associated with the Portland Housing Center for over 10 years.<\\/p>\\r\\n<p>In addition to my real estate career, I am a licensed and bonded general contractor. My construction company is Vantage Point Renovations, LLC. I am well-versed in home restoration and renovation and have held my contractor\'s license since 2001. I received my formal education from Pomona College, where I received my BA degree in Psychology. I just recently completed a course of study in conflict mangagement and dispute resolution at Marylhurst University. I am constantly striving to improve my negotiation skills as well as my ability to interact effectively with people.<\\/p>\\r\\n<p>I am a dedicated husband and father. My interests include gardening, golf and woodworking. I remain forever dedicated to the principle that everyone deserves equal access to home ownership.<\\/p>\\r\\n<\\/div>\\r\\n<div style=\\\"clear: left;\\\"><hr \\/><\\/div>\\r\\n<div class=\\\"contactinfo\\\"><a href=\\\"mailto:mary@vppihomes.com\\\">mary@vppihomes.com<\\/a><br \\/>Cell: <a href=\\\"tel:5039017480\\\">503-998-1780<\\/a><br \\/>Office: 503-243-4620<br \\/>Fax: 503-243-4621<br \\/>Home: 503-663-1357<\\/div>\\r\\n<div>\\r\\n<p><img style=\\\"float: left; margin-right: 15px; margin-bottom: 10px;\\\" src=\\\"images\\/mary.jpg\\\" alt=\\\"Mary Mayther-Slac\\\" width=\\\"171\\\" height=\\\"183\\\" \\/><span style=\\\"font-size: x-large;\\\">Mary Mayther-Slac<\\/span><br \\/><br \\/> Mary Mayther-Slac is a Portland native with 25+ years experience assessing, refining and achieving the real estate goals of her clients. She is the President of Vantage Point Properties, Inc. A homegrown real estate firm that has helped clients to realize their property ownership dreams since 2002. Her inherent knack for spotting the potential of diamond-in-the-rough properties, recognizing opportunities, and networking for her clients earned her the Portland Monthly magazine 5 Star Real Estate Agent award in 2011 \\u2013 2013, based on her buyers satisfaction.<\\/p>\\r\\n<p>Mary is service oriented and solution driven, and very skilled in negotiating and creating opportunities for her clients to obtain maximum results with minimum anxiety. She loves assisting her sellers in maximizing their profit potential, by counseling them on making value adding improvements to their properties, crucial market timing, and creating opportunities for building wealth through investment in real estate. She has been a volunteer educator for first time home buyers at the Portland Housing Center for ten years.<\\/p>\\r\\n<p>In her free time Mary loves spending time with her husband Gary and son Andrew, fund raising for non-profits, volunteering in her community, supporting friends, family and colleagues in their dreams and endeavors, gardening, and snuggling with her two rescued cats, Fiona and Alejandro.<\\/p>\\r\\n<\\/div>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2014-07-03 00:49:56\",\"created_by\":\"404\",\"created_by_alias\":\"\",\"modified\":\"\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2014-07-03 00:49:56\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(3,3,1,'','2014-07-03 00:51:56',404,6208,'961aba982056c2ab01aafe3d9133e33f9b4112b8','{\"id\":3,\"asset_id\":71,\"title\":\"What Every Buyer Should Know\",\"alias\":\"what-every-buyer-should-know\",\"introtext\":\"<p>We think it\'s always a good idea to make informed decisions. We\'ve noticed over the years that there is frequently confusion about the home buying process. What follows is an overview of that process and how it works.<\\/p>\\r\\n<p>The most important piece of this puzzle comes early in the game. Probably one of the most significant decisions you will make will be what Real Estate Agent you choose. Baring glaring differences in skill levels most Real Estate professionals are capable and able people. In other words, most brokers will be able to steward you through this process. We feel that the most important criterion for choosing your agent is based on feel. Does this person feel right for me? Is this person a good fit for me? Is this person a good listener? How well do our personalities match? And finally, do I trust this person? In effect, do I like this person?<\\/p>\\r\\n<p>Our method for determining this fit is simple. We arrange a meeting that fits your schedule in a place where you feel comfortable. We take the time to get to know each other. The meeting takes its own course and we both decide on making the commitment to work together. We tailor our time together to fit around your work schedule and other personal commitments. We arrange our first day of house hunting.<\\/p>\\r\\n<p>Our house hunting is also tailor made around your needs and style. It is something that we design together. It revolves around how we exchange information, how we remain in contact and how often. What you\\u2019ll experience is a custom built relationship.<\\/p>\\r\\n<p>Our job is to make you the best buyer possible before we go out shopping, getting you pre-approved for a loan, funds available for purchase and inspection costs and criteria of your search clearly stated.<\\/p>\\r\\n<p>Typically, that relationship yields exactly what you\\u2019ve envisioned in your home. It will be a reflection of your personality and a statement about who you are. When that special home is found we will sit down and craft an offer to be presented to the sellers through their broker\\/representative. They decide to either accept your offer, counter it with changes to some of the terms of our offer, or they can simply say that they\\u2019re not going to accept it. Let\\u2019s assume that your offer is accepted.<\\/p>\\r\\n<p>What follows after writing an offer and getting it accepted is the time that the value of your agent really comes through. There are numerous tasks that much be completed in a timely manner that will determine the successful maneuvering through the transaction. Here\\u2019s the sequence of events that occur:<\\/p>\\r\\n<ol>\\r\\n<li>All paperwork is sent to an escrow company (we\\u2019ll talk more about that later)<\\/li>\\r\\n<li>We make formal loan application within a specified time period and provide the bank with a copy of the sale agreement and any additional paperwork they might need<\\/li>\\r\\n<li>We start to order all of our inspections for the home. These include:<ol style=\\\"list-style-type: lower-alpha;\\\">\\r\\n<li>A full home and dry rot inspection<\\/li>\\r\\n<li>Additional inspection noted by your home inspector (electrical, furnace, roof, mold, etc.)<\\/li>\\r\\n<li>A sewer inspection<\\/li>\\r\\n<li>Radon inspection (with homes with basements)<\\/li>\\r\\n<\\/ol><\\/li>\\r\\n<li>After the home inspections are done we negotiate with the seller(s) to see what repairs they\\u2019re willing to do.<\\/li>\\r\\n<li>When we\\u2019ve reached an agreement on them we notify the bank to proceed to the next step and order the appraisal of the property (this will tell us about the value and condition of the home (did we pay too much or did we offer the true value of the home?)<\\/li>\\r\\n<li>When the appraisal comes back the bank goes into the final approval stage and you are given formal full approval to buy the home.<\\/li>\\r\\n<li>All of the paperwork required to purchase the home goes to your escrow company. This is a neutral third party that processes paperwork and acts according to the terms of your sale agreement. They can only do what you and the seller have instructed them to do in writing.<\\/li>\\r\\n<li>We all go to the escrow company. You bring them the funds needed to get your loan and sign all of the paperwork.<\\/li>\\r\\n<li>The paperwork goes back to the bank for review and they give their approval.<\\/li>\\r\\n<li>The escrow company is told about the approval, they get the deed to the county for recording, and the home is yours! You can usually move in by 5:00 the day the deed is recorded.<\\/li>\\r\\n<\\/ol>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2014-07-03 00:51:56\",\"created_by\":\"404\",\"created_by_alias\":\"\",\"modified\":\"\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2014-07-03 00:51:56\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(4,4,1,'','2014-07-03 20:23:39',404,2089,'cf1b2e1d3c5f550ebb73b1872dc463c96d54e226','{\"id\":4,\"asset_id\":78,\"title\":\"Contact Us\",\"alias\":\"contact-us\",\"introtext\":\"<p style=\\\"text-align: center;\\\">1410 SW 11th Ave #606<br \\/>Portland, Oregon 97201-3342<\\/p>\\r\\n<p style=\\\"text-align: center;\\\">(503) 243-4620<br \\/>Fax: (503) 243-4621<br \\/>Gary Cell: (503) 860-3740<br \\/>Mary Cell: (503) 998-1780<br \\/>Crispin Cell: (503) 888-3656<\\/p>\\r\\n<p style=\\\"text-align: center;\\\"><a href=\\\"mailto:info@vppihomes.com?subject=Inquiry%20From%20Website\\\">info@vppihomes.com<\\/a><\\/p>\\r\\n<p>{loadposition contactform}<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2014-07-03 20:23:39\",\"created_by\":\"404\",\"created_by_alias\":\"\",\"modified\":\"\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2014-07-03 20:23:39\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(5,4,1,'','2014-07-04 01:10:37',404,2153,'5654f7e623a5f0058c314be07928b6ca9d35f047','{\"id\":4,\"asset_id\":\"78\",\"title\":\"Contact Us\",\"alias\":\"contact-us\",\"introtext\":\"<p style=\\\"text-align: center;\\\">1410 SW 11th Ave #606<br \\/>Portland, Oregon 97201-3342<\\/p>\\r\\n<p style=\\\"text-align: center;\\\">(503) 243-4620<br \\/>Fax: (503) 243-4621<br \\/>Gary Cell: (503) 860-3740<br \\/>Mary Cell: (503) 998-1780<br \\/>Crispin Cell: (503) 888-3656<\\/p>\\r\\n<p style=\\\"text-align: center;\\\"><a href=\\\"mailto:info@vppihomes.com?subject=Inquiry%20From%20Website\\\">info@vppihomes.com<\\/a><\\/p>\\r\\n<p>{loadposition contactform}{loadposition locationmap}<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2014-07-03 20:23:39\",\"created_by\":\"404\",\"created_by_alias\":\"\",\"modified\":\"2014-07-04 01:10:37\",\"modified_by\":\"404\",\"checked_out\":\"404\",\"checked_out_time\":\"2014-07-04 01:10:21\",\"publish_up\":\"2014-07-03 20:23:39\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_layout\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":2,\"ordering\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":\"3\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}',0),(6,1,3,'','2014-07-30 01:28:52',404,2900,'a0629addc00f5c9ec0a21d824a0a8965e53e460a','{\"id\":1,\"name\":\"Gary Slac\",\"alias\":\"gary-slac\",\"con_position\":\"Owner\\/Principle Broker\",\"address\":\"\",\"suburb\":\"\",\"state\":\"\",\"country\":\"\",\"postcode\":\"\",\"telephone\":\"503-243-4620\",\"fax\":\"503-243-4621\",\"misc\":\"<p>I am the owner and Principal Broker of Vantage Point Properties, Inc., a family owned business since 2002. I\'ve been an active, full-time agent for over 25 years. I am a first-time buyer specialist. After all these years, there is nothing more satisfying than helping someone find and purchase their first home. I\'ve been proudly associated with the Portland Housing Center for over 10 years.<\\/p>\\r\\n<p>In addition to my real estate career, I am a licensed and bonded general contractor. My construction company is Vantage Point Renovations, LLC. I am well-versed in home restoration and renovation and have held my contractor\'s license since 2001. I received my formal education from Pomona College, where I received my BA degree in Psychology. I just recently completed a course of study in conflict mangagement and dispute resolution at Marylhurst University. I am constantly striving to improve my negotiation skills as well as my ability to interact effectively with people.<\\/p>\\r\\n<p>I am a dedicated husband and father. My interests include gardening, golf and woodworking. I remain forever dedicated to the principle that everyone deserves equal access to home ownership.<\\/p>\",\"image\":\"images\\/gary.jpg\",\"email_to\":\"gary@vppihomes.com\",\"default_con\":0,\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"ordering\":1,\"params\":\"{\\\"show_contact_category\\\":\\\"\\\",\\\"show_contact_list\\\":\\\"\\\",\\\"presentation_style\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_name\\\":\\\"\\\",\\\"show_position\\\":\\\"\\\",\\\"show_email\\\":\\\"\\\",\\\"show_street_address\\\":\\\"\\\",\\\"show_suburb\\\":\\\"\\\",\\\"show_state\\\":\\\"\\\",\\\"show_postcode\\\":\\\"\\\",\\\"show_country\\\":\\\"\\\",\\\"show_telephone\\\":\\\"\\\",\\\"show_mobile\\\":\\\"\\\",\\\"show_fax\\\":\\\"\\\",\\\"show_webpage\\\":\\\"\\\",\\\"show_misc\\\":\\\"\\\",\\\"show_image\\\":\\\"\\\",\\\"allow_vcard\\\":\\\"\\\",\\\"show_articles\\\":\\\"\\\",\\\"show_profile\\\":\\\"\\\",\\\"show_links\\\":\\\"\\\",\\\"linka_name\\\":\\\"\\\",\\\"linka\\\":false,\\\"linkb_name\\\":\\\"\\\",\\\"linkb\\\":false,\\\"linkc_name\\\":\\\"\\\",\\\"linkc\\\":false,\\\"linkd_name\\\":\\\"\\\",\\\"linkd\\\":false,\\\"linke_name\\\":\\\"\\\",\\\"linke\\\":\\\"\\\",\\\"contact_layout\\\":\\\"\\\",\\\"show_email_form\\\":\\\"\\\",\\\"show_email_copy\\\":\\\"\\\",\\\"banned_email\\\":\\\"\\\",\\\"banned_subject\\\":\\\"\\\",\\\"banned_text\\\":\\\"\\\",\\\"validate_session\\\":\\\"\\\",\\\"custom_reply\\\":\\\"\\\",\\\"redirect\\\":\\\"\\\"}\",\"user_id\":\"\",\"catid\":\"4\",\"access\":\"1\",\"mobile\":\"503-860-3740\",\"webpage\":\"http:\\/\\/vppihomes.com\",\"sortname1\":\"\",\"sortname2\":\"\",\"sortname3\":\"\",\"language\":\"*\",\"created\":\"2014-07-30 01:28:52\",\"created_by\":\"404\",\"created_by_alias\":\"\",\"modified\":\"\",\"modified_by\":null,\"metakey\":\"\",\"metadesc\":\"\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"rights\\\":\\\"\\\"}\",\"featured\":\"1\",\"xreference\":\"\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"version\":1,\"hits\":null}',0),(7,2,3,'','2014-07-30 01:30:35',404,1713,'c2db92572226b7865ff78d3ce3f3c6e0cc911aef','{\"id\":2,\"name\":\"Mary Mayther-Slac\",\"alias\":\"mary-mayther-slac\",\"con_position\":\"President\",\"address\":\"\",\"suburb\":\"\",\"state\":\"\",\"country\":\"\",\"postcode\":\"\",\"telephone\":\"503-243-4620\",\"fax\":\"503-243-4621\",\"misc\":\"\",\"image\":\"images\\/mary.jpg\",\"email_to\":\"mary@vppihomes.com\",\"default_con\":0,\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"ordering\":2,\"params\":\"{\\\"show_contact_category\\\":\\\"\\\",\\\"show_contact_list\\\":\\\"\\\",\\\"presentation_style\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_name\\\":\\\"\\\",\\\"show_position\\\":\\\"\\\",\\\"show_email\\\":\\\"\\\",\\\"show_street_address\\\":\\\"\\\",\\\"show_suburb\\\":\\\"\\\",\\\"show_state\\\":\\\"\\\",\\\"show_postcode\\\":\\\"\\\",\\\"show_country\\\":\\\"\\\",\\\"show_telephone\\\":\\\"\\\",\\\"show_mobile\\\":\\\"\\\",\\\"show_fax\\\":\\\"\\\",\\\"show_webpage\\\":\\\"\\\",\\\"show_misc\\\":\\\"\\\",\\\"show_image\\\":\\\"\\\",\\\"allow_vcard\\\":\\\"\\\",\\\"show_articles\\\":\\\"\\\",\\\"show_profile\\\":\\\"\\\",\\\"show_links\\\":\\\"\\\",\\\"linka_name\\\":\\\"\\\",\\\"linka\\\":false,\\\"linkb_name\\\":\\\"\\\",\\\"linkb\\\":false,\\\"linkc_name\\\":\\\"\\\",\\\"linkc\\\":false,\\\"linkd_name\\\":\\\"\\\",\\\"linkd\\\":false,\\\"linke_name\\\":\\\"\\\",\\\"linke\\\":\\\"\\\",\\\"contact_layout\\\":\\\"\\\",\\\"show_email_form\\\":\\\"\\\",\\\"show_email_copy\\\":\\\"\\\",\\\"banned_email\\\":\\\"\\\",\\\"banned_subject\\\":\\\"\\\",\\\"banned_text\\\":\\\"\\\",\\\"validate_session\\\":\\\"\\\",\\\"custom_reply\\\":\\\"\\\",\\\"redirect\\\":\\\"\\\"}\",\"user_id\":\"\",\"catid\":\"4\",\"access\":\"1\",\"mobile\":\"503-998-1780\",\"webpage\":\"http:\\/\\/vppihomes.com\",\"sortname1\":\"\",\"sortname2\":\"\",\"sortname3\":\"\",\"language\":\"*\",\"created\":\"2014-07-30 01:30:35\",\"created_by\":\"404\",\"created_by_alias\":\"\",\"modified\":\"\",\"modified_by\":null,\"metakey\":\"\",\"metadesc\":\"\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"rights\\\":\\\"\\\"}\",\"featured\":\"1\",\"xreference\":\"\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"version\":1,\"hits\":null}',0),(8,2,3,'','2014-07-30 01:31:02',404,3163,'e9c1b976df360f5fdb74828b400f04a2c44c7cae','{\"id\":2,\"name\":\"Mary Mayther-Slac\",\"alias\":\"mary-mayther-slac\",\"con_position\":\"President\",\"address\":\"\",\"suburb\":\"\",\"state\":\"\",\"country\":\"\",\"postcode\":\"\",\"telephone\":\"503-243-4620\",\"fax\":\"503-243-4621\",\"misc\":\"<p>Mary Mayther-Slac is a Portland native with 25+ years experience assessing, refining and achieving the real estate goals of her clients. She is the President of Vantage Point Properties, Inc. A homegrown real estate firm that has helped clients to realize their property ownership dreams since 2002. Her inherent knack for spotting the potential of diamond-in-the-rough properties, recognizing opportunities, and networking for her clients earned her the Portland Monthly magazine 5 Star Real Estate Agent award in 2011 \\u2013 2013, based on her buyers satisfaction.<\\/p>\\r\\n<p>Mary is service oriented and solution driven, and very skilled in negotiating and creating opportunities for her clients to obtain maximum results with minimum anxiety. She loves assisting her sellers in maximizing their profit potential, by counseling them on making value adding improvements to their properties, crucial market timing, and creating opportunities for building wealth through investment in real estate. She has been a volunteer educator for first time home buyers at the Portland Housing Center for ten years.<\\/p>\\r\\n<p>In her free time Mary loves spending time with her husband Gary and son Andrew, fund raising for non-profits, volunteering in her community, supporting friends, family and colleagues in their dreams and endeavors, gardening, and snuggling with her two rescued cats, Fiona and Alejandro.<\\/p>\",\"image\":\"images\\/mary.jpg\",\"email_to\":\"mary@vppihomes.com\",\"default_con\":0,\"published\":\"1\",\"checked_out\":\"404\",\"checked_out_time\":\"2014-07-30 01:30:45\",\"ordering\":\"2\",\"params\":\"{\\\"show_contact_category\\\":\\\"\\\",\\\"show_contact_list\\\":\\\"\\\",\\\"presentation_style\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_name\\\":\\\"\\\",\\\"show_position\\\":\\\"\\\",\\\"show_email\\\":\\\"\\\",\\\"show_street_address\\\":\\\"\\\",\\\"show_suburb\\\":\\\"\\\",\\\"show_state\\\":\\\"\\\",\\\"show_postcode\\\":\\\"\\\",\\\"show_country\\\":\\\"\\\",\\\"show_telephone\\\":\\\"\\\",\\\"show_mobile\\\":\\\"\\\",\\\"show_fax\\\":\\\"\\\",\\\"show_webpage\\\":\\\"\\\",\\\"show_misc\\\":\\\"\\\",\\\"show_image\\\":\\\"\\\",\\\"allow_vcard\\\":\\\"\\\",\\\"show_articles\\\":\\\"\\\",\\\"show_profile\\\":\\\"\\\",\\\"show_links\\\":\\\"\\\",\\\"linka_name\\\":\\\"\\\",\\\"linka\\\":false,\\\"linkb_name\\\":\\\"\\\",\\\"linkb\\\":false,\\\"linkc_name\\\":\\\"\\\",\\\"linkc\\\":false,\\\"linkd_name\\\":\\\"\\\",\\\"linkd\\\":false,\\\"linke_name\\\":\\\"\\\",\\\"linke\\\":\\\"\\\",\\\"contact_layout\\\":\\\"\\\",\\\"show_email_form\\\":\\\"\\\",\\\"show_email_copy\\\":\\\"\\\",\\\"banned_email\\\":\\\"\\\",\\\"banned_subject\\\":\\\"\\\",\\\"banned_text\\\":\\\"\\\",\\\"validate_session\\\":\\\"\\\",\\\"custom_reply\\\":\\\"\\\",\\\"redirect\\\":\\\"\\\"}\",\"user_id\":\"0\",\"catid\":\"4\",\"access\":\"1\",\"mobile\":\"503-998-1780\",\"webpage\":\"http:\\/\\/vppihomes.com\",\"sortname1\":\"\",\"sortname2\":\"\",\"sortname3\":\"\",\"language\":\"*\",\"created\":\"2014-07-30 01:30:35\",\"created_by\":\"404\",\"created_by_alias\":\"\",\"modified\":\"2014-07-30 01:31:02\",\"modified_by\":\"404\",\"metakey\":\"\",\"metadesc\":\"\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"rights\\\":\\\"\\\"}\",\"featured\":\"1\",\"xreference\":\"\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"version\":2,\"hits\":\"0\"}',0),(9,1,3,'','2014-07-30 22:44:30',404,2940,'d2290ab0ec511a62f45bd8fc1f69814cb4f671d9','{\"id\":1,\"name\":\"Gary Slac\",\"alias\":\"gary-slac\",\"con_position\":\"Owner\\/Principle Broker\",\"address\":\"\",\"suburb\":\"\",\"state\":\"\",\"country\":\"\",\"postcode\":\"\",\"telephone\":\"503-243-4620\",\"fax\":\"503-243-4621\",\"misc\":\"<p>I am the owner and Principal Broker of Vantage Point Properties, Inc., a family owned business since 2002. I\'ve been an active, full-time agent for over 25 years. I am a first-time buyer specialist. After all these years, there is nothing more satisfying than helping someone find and purchase their first home. I\'ve been proudly associated with the Portland Housing Center for over 10 years.<\\/p>\\r\\n<p>In addition to my real estate career, I am a licensed and bonded general contractor. My construction company is Vantage Point Renovations, LLC. I am well-versed in home restoration and renovation and have held my contractor\'s license since 2001. I received my formal education from Pomona College, where I received my BA degree in Psychology. I just recently completed a course of study in conflict mangagement and dispute resolution at Marylhurst University. I am constantly striving to improve my negotiation skills as well as my ability to interact effectively with people.<\\/p>\\r\\n<p>I am a dedicated husband and father. My interests include gardening, golf and woodworking. I remain forever dedicated to the principle that everyone deserves equal access to home ownership.<\\/p>\",\"image\":\"images\\/gary.jpg\",\"email_to\":\"gary@vppihomes.com\",\"default_con\":0,\"published\":\"1\",\"checked_out\":\"404\",\"checked_out_time\":\"2014-07-30 22:44:22\",\"ordering\":\"1\",\"params\":\"{\\\"show_contact_category\\\":\\\"\\\",\\\"show_contact_list\\\":\\\"\\\",\\\"presentation_style\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_name\\\":\\\"\\\",\\\"show_position\\\":\\\"\\\",\\\"show_email\\\":\\\"\\\",\\\"show_street_address\\\":\\\"\\\",\\\"show_suburb\\\":\\\"\\\",\\\"show_state\\\":\\\"\\\",\\\"show_postcode\\\":\\\"\\\",\\\"show_country\\\":\\\"\\\",\\\"show_telephone\\\":\\\"\\\",\\\"show_mobile\\\":\\\"\\\",\\\"show_fax\\\":\\\"\\\",\\\"show_webpage\\\":\\\"\\\",\\\"show_misc\\\":\\\"\\\",\\\"show_image\\\":\\\"\\\",\\\"allow_vcard\\\":\\\"\\\",\\\"show_articles\\\":\\\"\\\",\\\"show_profile\\\":\\\"\\\",\\\"show_links\\\":\\\"\\\",\\\"linka_name\\\":\\\"\\\",\\\"linka\\\":false,\\\"linkb_name\\\":\\\"\\\",\\\"linkb\\\":false,\\\"linkc_name\\\":\\\"\\\",\\\"linkc\\\":false,\\\"linkd_name\\\":\\\"\\\",\\\"linkd\\\":false,\\\"linke_name\\\":\\\"\\\",\\\"linke\\\":\\\"\\\",\\\"contact_layout\\\":\\\"\\\",\\\"show_email_form\\\":\\\"\\\",\\\"show_email_copy\\\":\\\"\\\",\\\"banned_email\\\":\\\"\\\",\\\"banned_subject\\\":\\\"\\\",\\\"banned_text\\\":\\\"\\\",\\\"validate_session\\\":\\\"\\\",\\\"custom_reply\\\":\\\"\\\",\\\"redirect\\\":\\\"\\\"}\",\"user_id\":\"0\",\"catid\":\"4\",\"access\":\"1\",\"mobile\":\"503-860-3740\",\"webpage\":\"http:\\/\\/vppihomes.com\",\"sortname1\":\"\",\"sortname2\":\"\",\"sortname3\":\"\",\"language\":\"*\",\"created\":\"2014-07-30 01:28:52\",\"created_by\":\"404\",\"created_by_alias\":\"\",\"modified\":\"2014-07-30 22:44:30\",\"modified_by\":\"404\",\"metakey\":\"\",\"metadesc\":\"\",\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"rights\\\":\\\"\\\"}\",\"featured\":\"1\",\"xreference\":\"\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"version\":2,\"hits\":\"6\"}',0);
/*!40000 ALTER TABLE `mf0kn_ucm_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_update_sites`
--

DROP TABLE IF EXISTS `mf0kn_update_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_update_sites` (
  `update_site_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `location` text NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='Update Sites';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_update_sites`
--

LOCK TABLES `mf0kn_update_sites` WRITE;
/*!40000 ALTER TABLE `mf0kn_update_sites` DISABLE KEYS */;
INSERT INTO `mf0kn_update_sites` VALUES (1,'Joomla Core','collection','http://update.joomla.org/core/list.xml',1,1479971358,''),(2,'Joomla Extension Directory','collection','http://update.joomla.org/jed/list.xml',1,1479971358,''),(3,'Accredited Joomla! Translations','collection','http://update.joomla.org/language/translationlist_3.xml',1,0,''),(5,'RocketTheme Update Directory','collection','http://updates.rockettheme.com/joomla/updates.xml',1,0,''),(6,'Admin Tools Professional','extension','http://cdn.akeebabackup.com/updates/atpro.xml',1,0,'dlid=40703:83e3fb4a30d457f9758232fc028c9d0a'),(7,'Akeeba GeoIP Provider Plugin','extension','http://cdn.akeebabackup.com/updates/akgeoip.xml',1,1479971355,''),(8,'Akeeba Backup Professional','extension','http://cdn.akeebabackup.com/updates/abpro.xml',1,1479971355,'dlid=40703:83e3fb4a30d457f9758232fc028c9d0a'),(9,'JCE Editor Updates','extension','https://www.joomlacontenteditor.net/index.php?option=com_updates&view=update&format=xml&id=1&file=extension.xml',1,1479971355,''),(13,'PDXfixIT','extension','http://updates.pdxfixit.com/mod_contactform.xml',0,0,''),(14,'Xmap Update Site','extension','https://raw.github.com/guilleva/Xmap/master/xmap-update.xml',0,0,''),(15,'Gantry Framework Update Site','extension','http://www.gantry-framework.org/updates/joomla16/gantry.xml',1,1479971355,''),(16,'Joomla! Update Component Update Site','extension','http://update.joomla.org/core/extensions/com_joomlaupdate.xml',1,1479971355,''),(17,'Weblinks Update Site','extension','https://raw.githubusercontent.com/joomla-extensions/weblinks/master/manifest.xml',1,1479971355,'');
/*!40000 ALTER TABLE `mf0kn_update_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_update_sites_extensions`
--

DROP TABLE IF EXISTS `mf0kn_update_sites_extensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`update_site_id`,`extension_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links extensions to update sites';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_update_sites_extensions`
--

LOCK TABLES `mf0kn_update_sites_extensions` WRITE;
/*!40000 ALTER TABLE `mf0kn_update_sites_extensions` DISABLE KEYS */;
INSERT INTO `mf0kn_update_sites_extensions` VALUES (1,700),(2,700),(3,600),(5,10007),(6,10008),(7,10016),(8,10017),(9,10025),(13,10043),(14,10045),(15,10058),(16,28),(17,801);
/*!40000 ALTER TABLE `mf0kn_update_sites_extensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_updates`
--

DROP TABLE IF EXISTS `mf0kn_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) DEFAULT '',
  `description` text NOT NULL,
  `element` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `folder` varchar(20) DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) DEFAULT '',
  `data` text NOT NULL,
  `detailsurl` text NOT NULL,
  `infourl` text NOT NULL,
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Available Updates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_updates`
--

LOCK TABLES `mf0kn_updates` WRITE;
/*!40000 ALTER TABLE `mf0kn_updates` DISABLE KEYS */;
INSERT INTO `mf0kn_updates` VALUES (1,1,700,'Joomla','','joomla','file','',0,'3.6.4','','https://update.joomla.org/core/sts/extension_sts.xml','',''),(2,7,0,'Akeeba GeoIP Provider Plugin','Akeeba GeoIP Provider Plugin','akgeoip','plugin','system',1,'1.0.6','','http://cdn.akeebabackup.com/updates/akgeoip.xml','https://www.akeebabackup.com/component/ars/?view=release&id=2001',''),(3,8,10017,'Akeeba Backup Professional','Akeeba Backup Professional','com_akeeba','component','',1,'4.7.4','','http://cdn.akeebabackup.com/updates/abpro.xml','https://www.akeebabackup.com/component/ars/?view=Items&release_id=2539','dlid=40703:83e3fb4a30d457f9758232fc028c9d0a'),(4,9,10025,'JCE Editor','','com_jce','component','',1,'2.5.31','','https://www.joomlacontenteditor.net/index.php?option=com_updates&view=update&format=xml&id=1&file=extension.xml','http://www.joomlacontenteditor.net/news/item/jce-2531-released',''),(5,15,10058,'Gantry','Gantry Framework','lib_gantry','library','',0,'4.1.32','','http://www.gantry-framework.org/updates/joomla16/gantry.xml','http://www.gantry.org','');
/*!40000 ALTER TABLE `mf0kn_updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_user_keys`
--

DROP TABLE IF EXISTS `mf0kn_user_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_user_keys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `series` varchar(255) NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) NOT NULL,
  `uastring` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `series` (`series`),
  UNIQUE KEY `series_2` (`series`),
  UNIQUE KEY `series_3` (`series`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_user_keys`
--

LOCK TABLES `mf0kn_user_keys` WRITE;
/*!40000 ALTER TABLE `mf0kn_user_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_user_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_user_notes`
--

DROP TABLE IF EXISTS `mf0kn_user_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_user_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_category_id` (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_user_notes`
--

LOCK TABLES `mf0kn_user_notes` WRITE;
/*!40000 ALTER TABLE `mf0kn_user_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_user_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_user_profiles`
--

DROP TABLE IF EXISTS `mf0kn_user_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) NOT NULL,
  `profile_value` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Simple user profile storage table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_user_profiles`
--

LOCK TABLES `mf0kn_user_profiles` WRITE;
/*!40000 ALTER TABLE `mf0kn_user_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_user_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_user_usergroup_map`
--

DROP TABLE IF EXISTS `mf0kn_user_usergroup_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_user_usergroup_map` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_user_usergroup_map`
--

LOCK TABLES `mf0kn_user_usergroup_map` WRITE;
/*!40000 ALTER TABLE `mf0kn_user_usergroup_map` DISABLE KEYS */;
INSERT INTO `mf0kn_user_usergroup_map` VALUES (403,8),(404,8),(405,7),(406,7),(407,7);
/*!40000 ALTER TABLE `mf0kn_user_usergroup_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_usergroups`
--

DROP TABLE IF EXISTS `mf0kn_usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_usergroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  KEY `idx_usergroup_title_lookup` (`title`),
  KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_usergroups`
--

LOCK TABLES `mf0kn_usergroups` WRITE;
/*!40000 ALTER TABLE `mf0kn_usergroups` DISABLE KEYS */;
INSERT INTO `mf0kn_usergroups` VALUES (1,0,1,18,'Public'),(2,1,8,15,'Registered'),(3,2,9,14,'Author'),(4,3,10,13,'Editor'),(5,4,11,12,'Publisher'),(6,1,4,7,'Manager'),(7,6,5,6,'Administrator'),(8,1,16,17,'Super Users'),(9,1,2,3,'Guest');
/*!40000 ALTER TABLE `mf0kn_usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_users`
--

DROP TABLE IF EXISTS `mf0kn_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_block` (`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=408 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_users`
--

LOCK TABLES `mf0kn_users` WRITE;
/*!40000 ALTER TABLE `mf0kn_users` DISABLE KEYS */;
INSERT INTO `mf0kn_users` VALUES (403,'Ben Sandberg','ben','ben@pdxfixit.com','$2y$10$do5U5L6n8SYOOm2W8VoGY.VZZjvX5ygc929NTGYbUx8ytiqS1UW7O',0,1,'2014-06-05 01:05:51','2016-11-24 07:17:17','0','{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}','0000-00-00 00:00:00',0,'','',0),(404,'J. Curtis BrandSmith','curtis','jcbrandsmith@gmail.com','$2y$10$raPy6qkr8Aoll3W0uKK92eOpx1FWBELyzRV4nPrOyXFwD.eO32/wC',0,1,'2014-06-05 02:02:02','2015-01-29 21:07:48','','{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}','0000-00-00 00:00:00',0,'','',0),(405,'Mary Mayther-Slac','mary','mary@vppihomes.com','$2y$10$SoSx9znAiXJQ8jUYXJDAYu18Fn./mXgneDy.J5HkAxGlr/G1bcGI2',0,1,'2014-09-04 00:48:17','0000-00-00 00:00:00','','{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"America\\/Los_Angeles\"}','0000-00-00 00:00:00',0,'','',0),(406,'Gary Slac','gary','gary@vppihomes.com','$2y$10$gBcq91Mt2/nPkWeqv9Ruc.QL.61fFirRtBLM5/7TtEMxl8BDEoO7C',0,0,'2014-09-04 00:49:37','0000-00-00 00:00:00','','{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"America\\/Los_Angeles\"}','0000-00-00 00:00:00',0,'','',0),(407,'Andrew Slac','andrew','andrew@vppihomes.com','$2y$10$eOGw2i0RoAHzv5UfO8fEyOQ0tZ3awi5SaokSV4FtCDoJb89zuZBxK',0,0,'2015-01-30 02:19:42','0000-00-00 00:00:00','','{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"America\\/Los_Angeles\"}','0000-00-00 00:00:00',0,'','',0);
/*!40000 ALTER TABLE `mf0kn_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_viewlevels`
--

DROP TABLE IF EXISTS `mf0kn_viewlevels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_viewlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `title` varchar(100) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_assetgroup_title_lookup` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_viewlevels`
--

LOCK TABLES `mf0kn_viewlevels` WRITE;
/*!40000 ALTER TABLE `mf0kn_viewlevels` DISABLE KEYS */;
INSERT INTO `mf0kn_viewlevels` VALUES (1,'Public',0,'[1]'),(2,'Registered',1,'[6,2,8]'),(3,'Special',2,'[6,3,8]'),(5,'Guest',0,'[9]'),(6,'Super Users',0,'[8]');
/*!40000 ALTER TABLE `mf0kn_viewlevels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_vppi_homes`
--

DROP TABLE IF EXISTS `mf0kn_vppi_homes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_vppi_homes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(127) NOT NULL,
  `alias` varchar(127) NOT NULL,
  `city` varchar(127) NOT NULL,
  `state_prov` varchar(127) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `ml_number` int(11) NOT NULL,
  `price` decimal(18,2) NOT NULL,
  `area` varchar(127) NOT NULL,
  `elem_school` varchar(127) NOT NULL,
  `mid_school` varchar(127) NOT NULL,
  `high_school` varchar(127) NOT NULL,
  `short_sale` tinyint(1) NOT NULL,
  `bank_owned` tinyint(1) NOT NULL,
  `waterfront` varchar(127) NOT NULL,
  `body_of_water` varchar(127) NOT NULL,
  `tax_per_year` varchar(127) NOT NULL,
  `property_type` varchar(127) NOT NULL,
  `neighborhood_building` varchar(127) NOT NULL,
  `levels` tinyint(2) NOT NULL,
  `garage` varchar(127) NOT NULL,
  `roof` varchar(127) NOT NULL,
  `ext_description` varchar(127) NOT NULL,
  `mast_bed_level` varchar(127) NOT NULL,
  `fireplace` varchar(127) NOT NULL,
  `basement_foundation` varchar(127) NOT NULL,
  `view` varchar(127) NOT NULL,
  `acres` float(11,2) NOT NULL,
  `lot_size` varchar(127) NOT NULL,
  `lot_dimensions` varchar(127) NOT NULL,
  `lot_description` varchar(127) NOT NULL,
  `heat_fuel` varchar(127) NOT NULL,
  `cool` varchar(127) NOT NULL,
  `water` varchar(127) NOT NULL,
  `sewer` varchar(127) NOT NULL,
  `hot_water` varchar(127) NOT NULL,
  `zoning` varchar(127) NOT NULL,
  `remarks` text NOT NULL,
  `dining_room` varchar(127) NOT NULL,
  `family_room` varchar(127) NOT NULL,
  `living_room` varchar(127) NOT NULL,
  `kitchen` varchar(127) NOT NULL,
  `interior` varchar(127) NOT NULL,
  `exterior` varchar(127) NOT NULL,
  `accessibility` varchar(127) NOT NULL,
  `green_certification` varchar(127) NOT NULL,
  `energy_eff_features` varchar(127) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL,
  `featured` tinyint(1) NOT NULL,
  `sold` tinyint(1) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_vppi_homes`
--

LOCK TABLES `mf0kn_vppi_homes` WRITE;
/*!40000 ALTER TABLE `mf0kn_vppi_homes` DISABLE KEYS */;
INSERT INTO `mf0kn_vppi_homes` VALUES (1,55,'4705 SW Beech Dr','4705-sw-beech-dr','Beaverton','OR',97005,13287230,0.00,'Beaverton, Aloha','Raleigh Hilss','','Beaverton',0,0,'','','$2,323.97','DETACHD','',1,'1 / ATTACHD','COMP','LAP / WOOD','M','1 / WOOD','CRAWLSP','',0.14,'5K-6,999SF','Irregular','CORNER, LEVEL','FOR-AIR, FOR-90 / GAS','WINDOW','PUBLIC / PUBLIC','PUBLIC / PUBLIC','ELECT','R-7','<p>Great 1 level 50\'s ranch style in quiet neighborhood has masonry fireplc, built ins, hardwd floors throughout, gas range, 90+ eff. gas furnace, slider off eating area to covered back deck, many newer windows, raised bed gardens & RV pkg. Close to shopping, bus lines, Hwy 217 and town.</p>','SLIDER / LAM-FL','','BLT-INS / FIREPL / HARDWOOD','BI-MICO / DISHWAS / GASAPPL','CEILFAN / GAR-OPN / HARDWOD / LAM-FL','FENCED / GARDEN / STMDOR / YARD / COVDECK','1LEVEL','','FOR-90',1,403,'2015-03-18 00:42:11',2,1,1,403,'2014-09-04 01:19:05',404,'2014-07-02 22:21:43'),(2,62,'408 SE 105th Ave','408-se-105th-ave','Portland','OR',97216,13513732,0.00,'Portland Southeast','Ventura Park','Floyd Light','David Douglas',0,0,'','','$2,061.17','DETACHD','',1,'0','COMP','STUCCO','M','1 / WOOD','NO-BAS','',0.25,'10K-14,999SF','','LEVEL','FOR-AIR / ELECT, GAS','','PUBLIC / PUBLIC','PUBLIC / PUBLIC','GAS','R-1 d','','','','','','','','','','',1,0,'0000-00-00 00:00:00',4,1,1,403,'2014-09-04 01:19:05',404,'2014-07-02 23:25:30'),(3,73,'10013 SE Eastmont Dr','10013-se-eastmont-dr','Damascus','OR',97089,13600509,0.00,'Milwaukie, Gladstone, Happy Valley, Clckmas, Dmscus','East Orient','West Orient','Sam Barlow',0,0,'','','$3,0123.25','DETACHD','',3,'2 / ATTACHD','COMP','BRICK / WOOD','U','2 / WOOD','SLAB','TREES / MNTAIN',1.08,'1-2.99AC','110X440X164X341','PRIVATE, SECLDED, SLOPED, TREES','FOR-AIR / GAS','','COMMUNITY, PRIVATE / SEPTIC','COMMUNITY, PRIVATE / SEPTIC','ELECT','res','<p>Stunning Tri-level in a pristine, private setting, beautifully landscaped with views of Mt. Hood-rolling lawns surrounded by established plantings-Wrap around porch and upper deck expand living area-Two floor to ceiling fireplaces in the living rooms-Truly one of a kind!!</p>','','','','','','','','','',1,0,'0000-00-00 00:00:00',3,1,1,403,'2014-09-04 01:19:05',404,'2014-07-03 01:32:49'),(4,74,'3405 SE 79th Ave','3405-se-79th-ave','Portland','OR',97206,13696363,0.00,'143','Bridger','','Franklin',0,0,'','','$2,625.41','DETACHD','',2,'1 / DETACHD','COMP','LAP, VINYL','M','','FINISHD','',0.11,'3K-4,999SF','','','FOR-AIR / GAS','','PUBLIC / PUBLIC','PUBLIC / PUBLIC','','','<p>Move right in to this sweet 40\'s bungalow w/ cherry hdwd floors, updated kitchen w/ granite countertop, tile bath with jetted tub, energy efficient windows, 90% eff gas furnace. This home has a lovely backyard w/ deck, garden area and fruit trees. Close to town, shopping, parks and busline.</p>','','','','GRANITE, FS-RANG','TILE-FL, LAUNDRY, HARDWOD, GAR-OPN, GRANITE, WW-CARP','VYW-DBL, YARD, STMDOOR, PORCH, FENCED, GARDEN, DECK','','','BYW-DBL, FOR-95',1,0,'0000-00-00 00:00:00',5,0,1,403,'2014-09-04 01:19:05',404,'2014-07-03 19:44:24'),(5,75,'8042 SE Clay St','8042-se-clay-st','Portland','OR',97215,13697141,0.00,'Montavilla','','','',0,0,'','','','','',2,'','','','','GAS','','',0.00,'','','','','','','','','','','','','','','','','','','',0,0,'0000-00-00 00:00:00',6,0,1,403,'2014-09-04 01:19:05',404,'2014-07-03 19:50:08'),(6,76,'5402 SW Nevada Ct','5402-sw-nevada-ct','Portland','OR',97219,12077760,0.00,'148','Maplewood','Gray','Wilson',0,0,'','','$2,859.55','DETACHD','Maplewood',1,'1 / DETACHD','COMP','WOOD, LAP','M','','CRAWLSP','',0.14,'5K-6,999SF','','CORNER, LEVEL','HT-PUMP / ELECT','HT-PUMP','PUBLIC / PUBLIC','PUBLIC / PUBLIC','ELECT','resid','<p>Simpe/effiecient living in garden artist\'s cottage. Rusitc charm and lush, mature landscaping inspire creativity in this garden cottage. For those who seek simplicity and efficient use of a cozy space, this artist\'s haven is your dream home! Old growth fir floors throughout, knotty pine living room, custom tile kitchen and many other thoughtful touches make this home 1 of a kind.</p>','','','WOODFLR, BAYWIND','DISPOSL, DISHWAS, TILE, FS-RANG, FS-REFR','LAUNDRY, WOODFLR','FENCED, PATIO, PORCH, GARDEN, VYW-DBL, YARD','1LEVEL','','HT-PUMP, VYW-DBL',1,0,'0000-00-00 00:00:00',7,0,1,403,'2014-09-04 01:19:05',404,'2014-07-03 20:01:05'),(7,77,'13985 SE 153rd Dr','13985-se-153rd-dr','Clackamas','OR',97015,13154186,0.00,'Milwaukie, Gladstone, Happy Valley, Clckmas, Dmscus','Oregon Trail','Rock Creek','Clackamas',0,0,'','','$3,548.06','DETACHD','Sunnyside',2,'2 / OVRSIZE','COMP','VINYL','U','1 / GAS','CRAWLSP','',0.11,'3K-4,999SF','50.78 x 98.57','LEVEL, PRIVATE','FOR-AIR / GAS','','PUBLIC / PUBLIC','PUBLIC / PUBLIC','GAS','res','<p>Beautifully cared-for 4 bd 2.1 bth home in neighborhood of well-maintained homes-living and farm rms, frplc-hdwd flrs in kit. A very comfortable, inviting home, breakfast nook looks out onto patio in prvt bkyd-oversized dbl gar-esatblished garden area, fruit trees, blueberries, and more.</p>','WW-CARP','BLT-INS / FIREPL / WW-CARP','WW-CARP','DISHWAS / DISPOSL / TILE / FS-RANG','GAR-OPN / LAUNDRY / WW-CARP / WOODFLR','FENCED / GARDEN / PATIO / PORCH / YARD','1LEVEL','','WDW-DBL',1,0,'0000-00-00 00:00:00',8,0,1,403,'2014-09-04 01:19:05',404,'2014-07-03 20:09:51'),(8,83,'7010 SE Cottrell Rd','7010-se-cottrell-rd','Gresham','Oregon',97080,14202198,327900.00,'144','East Orient','West Orient','Sam Barlow',0,0,'N','N','$2,002.47','Detached','Rural',1,'Yes','Comp','Shake, cedar','1','wood','Crawlspace','pastoral',4.89,'3-4.99AC','','Trees, level','Forced air, gas','Window','Public Water','Septic system','Gas','MUA20','<p>Lovely midcentury sweetheart on 4.89 acres great for growing food, trees or pasture your pony! This home has a large country kitchen with ample counter space and pantry, spacious living room, formal dining room, private patio with water feature and beautifully landscaped yard with fruit trees, berries, grapes and garden area. Small animal barn for critters, level acreage with southern exposure for small home farmer or CSA. Treed oasis in the back with Mt Hood view potential. Great school district. Within 30 minutes of Portland.</p>','','','','','','','','','',1,0,'0000-00-00 00:00:00',1,1,0,403,'2014-09-11 04:42:14',403,'2014-09-04 01:04:09');
/*!40000 ALTER TABLE `mf0kn_vppi_homes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_vppi_images`
--

DROP TABLE IF EXISTS `mf0kn_vppi_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_vppi_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `home_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_vppi_images`
--

LOCK TABLES `mf0kn_vppi_images` WRITE;
/*!40000 ALTER TABLE `mf0kn_vppi_images` DISABLE KEYS */;
INSERT INTO `mf0kn_vppi_images` VALUES (1,1,'/images/homes/1/poster.jpg',1),(2,1,'/images/homes/1/13287230-11.jpg',2),(3,1,'/images/homes/1/13287230-10.jpg',3),(4,1,'/images/homes/1/13287230-9.jpg',4),(5,1,'/images/homes/1/13287230-8.jpg',5),(6,1,'/images/homes/1/13287230-7.jpg',6),(7,1,'/images/homes/1/13287230-6.jpg',7),(8,1,'/images/homes/1/13287230-5.jpg',8),(9,1,'/images/homes/1/13287230-4.jpg',9),(10,1,'/images/homes/1/13287230-3.jpg',10),(11,1,'/images/homes/1/13287230-2.jpg',11),(13,1,'/images/homes/1/13287230-13.jpg',12),(14,2,'/images/homes/2/poster.jpg',1),(15,2,'/images/homes/2/05.jpg',2),(16,2,'/images/homes/2/10.jpg',3),(17,2,'/images/homes/2/09.jpg',4),(18,2,'/images/homes/2/02.jpg',5),(19,2,'/images/homes/2/08.jpg',6),(20,2,'/images/homes/2/11.jpg',7),(21,2,'/images/homes/2/12.jpg',8),(22,2,'/images/homes/2/13.jpg',9),(23,2,'/images/homes/2/14.jpg',10),(24,3,'/images/homes/3/poster.jpg',1),(25,3,'/images/homes/3/2.jpg',2),(26,3,'/images/homes/3/3.jpg',3),(27,3,'/images/homes/3/4.jpg',4),(28,3,'/images/homes/3/5.jpg',5),(29,3,'/images/homes/3/6.jpg',6),(30,3,'/images/homes/3/7.jpg',7),(31,3,'/images/homes/3/8.jpg',8),(32,3,'/images/homes/3/9.jpg',9),(33,3,'/images/homes/3/10.jpg',10),(34,3,'/images/homes/3/11.jpg',11),(35,3,'/images/homes/3/12.jpg',12),(36,4,'/images/homes/4/poster.jpg',1),(37,4,'/images/homes/4/13696363-1.jpg',2),(38,4,'/images/homes/4/13696363-2.jpg',3),(39,4,'/images/homes/4/13696363-3.jpg',4),(40,4,'/images/homes/4/13696363-4.jpg',5),(41,4,'/images/homes/4/13696363-5.jpg',6),(42,4,'/images/homes/4/13696363-6.jpg',7),(43,4,'/images/homes/4/13696363-7.jpg',8),(44,4,'/images/homes/4/13696363-8.jpg',9),(45,4,'/images/homes/4/13696363-9.jpg',10),(46,4,'/images/homes/4/13696363-10.jpg',11),(47,4,'/images/homes/4/13696363-11.jpg',12),(48,4,'/images/homes/4/13696363-12.jpg',13),(49,4,'/images/homes/4/13696363-13.jpg',14),(50,4,'/images/homes/4/13696363-14.jpg',15),(51,5,'/images/homes/5/poster.jpg',1),(52,5,'/images/homes/5/01.jpg',2),(53,5,'/images/homes/5/03.jpg',3),(54,5,'/images/homes/5/04.jpg',4),(55,5,'/images/homes/5/05.jpg',5),(56,5,'/images/homes/5/06.jpg',6),(57,5,'/images/homes/5/07.jpg',7),(58,5,'/images/homes/5/08.jpg',8),(59,5,'/images/homes/5/09.jpg',9),(60,5,'/images/homes/5/10.jpg',10),(61,5,'/images/homes/5/14.jpg',11),(62,5,'/images/homes/5/15.jpg',12),(63,5,'/images/homes/5/16.jpg',13),(64,5,'/images/homes/5/17.jpg',14),(65,5,'/images/homes/5/18.jpg',15),(66,5,'/images/homes/5/19.jpg',16),(67,5,'/images/homes/5/20.jpg',17),(68,5,'/images/homes/5/21.jpg',18),(69,5,'/images/homes/5/22.jpg',19),(70,5,'/images/homes/5/23.jpg',20),(71,5,'/images/homes/5/24.jpg',21),(72,5,'/images/homes/5/25.jpg',22),(73,6,'/images/homes/6/poster.jpg',1),(74,6,'/images/homes/6/02.jpg',2),(75,6,'/images/homes/6/03.jpg',3),(76,6,'/images/homes/6/04.jpg',4),(77,6,'/images/homes/6/05.jpg',5),(78,6,'/images/homes/6/06.jpg',6),(79,6,'/images/homes/6/07.jpg',7),(80,6,'/images/homes/6/08.jpg',8),(81,6,'/images/homes/6/09.jpg',9),(82,6,'/images/homes/6/10.jpg',10),(83,6,'/images/homes/6/11.jpg',11),(84,6,'/images/homes/6/12.jpg',12),(85,6,'/images/homes/6/13.jpg',13),(86,6,'/images/homes/6/15.jpg',14),(87,6,'/images/homes/6/16.jpg',15),(88,7,'/images/homes/7/poster.jpg',1),(89,7,'/images/homes/7/dsc02439.jpg',2),(90,7,'/images/homes/7/dsc02440.jpg',3),(91,7,'/images/homes/7/dsc02441.jpg',4),(92,7,'/images/homes/7/dsc02443.jpg',5),(93,7,'/images/homes/7/dsc02445.jpg',6),(94,7,'/images/homes/7/dsc02448.jpg',7),(95,7,'/images/homes/7/dsc02450.jpg',8),(96,7,'/images/homes/7/dsc02452.jpg',9),(97,7,'/images/homes/7/dsc02453.jpg',10),(98,8,'/images/homes/8/poster.jpg',1),(99,8,'/images/homes/8/01.jpg',2),(100,8,'/images/homes/8/02.jpg',3),(101,8,'/images/homes/8/03.jpg',4),(102,8,'/images/homes/8/04.jpg',5),(103,8,'/images/homes/8/05.jpg',6),(104,8,'/images/homes/8/06.jpg',7),(105,8,'/images/homes/8/07.jpg',8),(106,8,'/images/homes/8/08.jpg',9),(107,8,'/images/homes/8/09.jpg',10),(108,8,'/images/homes/8/10.jpg',11),(109,8,'/images/homes/8/11.jpg',12),(110,8,'/images/homes/8/12.jpg',13),(111,8,'/images/homes/8/13.jpg',14),(112,8,'/images/homes/8/14.jpg',15),(113,8,'/images/homes/8/15.jpg',16),(114,8,'/images/homes/8/16.jpg',17),(115,8,'/images/homes/8/17.jpg',18),(116,8,'/images/homes/8/18.jpg',19),(117,8,'/images/homes/8/19.jpg',20),(118,8,'/images/homes/8/20.jpg',21),(119,8,'/images/homes/8/21.jpg',22),(120,8,'/images/homes/8/22.jpg',23),(121,8,'/images/homes/8/23.jpg',24),(122,8,'/images/homes/8/24.jpg',25),(123,8,'/images/homes/8/25.jpg',26),(124,8,'/images/homes/8/26.jpg',27),(125,8,'/images/homes/8/27.jpg',28),(126,8,'/images/homes/8/28.jpg',29),(127,8,'/images/homes/8/29.jpg',30),(128,8,'/images/homes/8/30.jpg',31),(129,8,'/images/homes/8/31.jpg',32),(130,8,'/images/homes/8/32.jpg',33),(131,8,'/images/homes/8/33.jpg',34);
/*!40000 ALTER TABLE `mf0kn_vppi_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_weblinks`
--

DROP TABLE IF EXISTS `mf0kn_weblinks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_weblinks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `language` char(7) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if link is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `images` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_weblinks`
--

LOCK TABLES `mf0kn_weblinks` WRITE;
/*!40000 ALTER TABLE `mf0kn_weblinks` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_weblinks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_wf_profiles`
--

DROP TABLE IF EXISTS `mf0kn_wf_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_wf_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `users` text NOT NULL,
  `types` text NOT NULL,
  `components` text NOT NULL,
  `area` tinyint(3) NOT NULL,
  `device` varchar(255) NOT NULL,
  `rows` text NOT NULL,
  `plugins` text NOT NULL,
  `published` tinyint(3) NOT NULL,
  `ordering` int(11) NOT NULL,
  `checked_out` tinyint(3) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_wf_profiles`
--

LOCK TABLES `mf0kn_wf_profiles` WRITE;
/*!40000 ALTER TABLE `mf0kn_wf_profiles` DISABLE KEYS */;
INSERT INTO `mf0kn_wf_profiles` VALUES (1,'Default','Default Profile for all users','','3,4,5,6,8,7','',0,'desktop,tablet,phone','help,newdocument,undo,redo,spacer,bold,italic,underline,strikethrough,justifyfull,justifycenter,justifyleft,justifyright,spacer,blockquote,formatselect,styleselect,removeformat,cleanup;fontselect,fontsizeselect,forecolor,backcolor,spacer,clipboard,indent,outdent,lists,sub,sup,textcase,charmap,hr;directionality,fullscreen,preview,source,print,searchreplace,spacer,table;visualaid,visualchars,visualblocks,nonbreaking,style,xhtmlxtras,anchor,unlink,link,imgmanager,spellchecker,article','charmap,contextmenu,browser,inlinepopups,media,help,clipboard,searchreplace,directionality,fullscreen,preview,source,table,textcase,print,style,nonbreaking,visualchars,visualblocks,xhtmlxtras,imgmanager,anchor,link,spellchecker,article,lists,formatselect,styleselect,fontselect,fontsizeselect,fontcolor',1,1,0,'0000-00-00 00:00:00',''),(2,'Front End','Sample Front-end Profile','','3,4,5','',1,'desktop,tablet,phone','help,newdocument,undo,redo,spacer,bold,italic,underline,strikethrough,justifyfull,justifycenter,justifyleft,justifyright,spacer,formatselect,styleselect;clipboard,searchreplace,indent,outdent,lists,cleanup,charmap,removeformat,hr,sub,sup,textcase,nonbreaking,visualchars,visualblocks;fullscreen,preview,print,visualaid,style,xhtmlxtras,anchor,unlink,link,imgmanager,spellchecker,article','charmap,contextmenu,inlinepopups,help,clipboard,searchreplace,fullscreen,preview,print,style,textcase,nonbreaking,visualchars,visualblocks,xhtmlxtras,imgmanager,anchor,link,spellchecker,article,lists,formatselect,styleselect',0,2,0,'0000-00-00 00:00:00',''),(3,'Blogger','Simple Blogging Profile','','3,4,5,6,8,7','',0,'desktop,tablet,phone','bold,italic,strikethrough,lists,blockquote,spacer,justifyleft,justifycenter,justifyright,spacer,link,unlink,imgmanager,article,spellchecker,fullscreen,kitchensink;formatselect,underline,justifyfull,forecolor,clipboard,removeformat,charmap,indent,outdent,undo,redo,help','link,imgmanager,article,spellchecker,fullscreen,kitchensink,clipboard,contextmenu,inlinepopups,lists,formatselect,fontcolor',0,3,0,'0000-00-00 00:00:00','{\"editor\":{\"toggle\":\"0\"}}'),(4,'Mobile','Sample Mobile Profile','','3,4,5,6,8,7','',0,'tablet,phone','undo,redo,spacer,bold,italic,underline,formatselect,spacer,justifyleft,justifycenter,justifyfull,justifyright,spacer,fullscreen,kitchensink;styleselect,lists,spellchecker,article,link,unlink','fullscreen,kitchensink,spellchecker,article,link,inlinepopups,lists,formatselect,styleselect',0,4,0,'0000-00-00 00:00:00','{\"editor\":{\"toolbar_theme\":\"mobile\",\"resizing\":\"0\",\"resize_horizontal\":\"0\",\"resizing_use_cookie\":\"0\",\"toggle\":\"0\",\"links\":{\"popups\":{\"default\":\"\",\"jcemediabox\":{\"enable\":\"0\"},\"window\":{\"enable\":\"0\"}}}}}');
/*!40000 ALTER TABLE `mf0kn_wf_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_xmap_items`
--

DROP TABLE IF EXISTS `mf0kn_xmap_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_xmap_items` (
  `uid` varchar(100) NOT NULL,
  `itemid` int(11) NOT NULL,
  `view` varchar(10) NOT NULL,
  `sitemap_id` int(11) NOT NULL,
  `properties` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`uid`,`itemid`,`view`,`sitemap_id`),
  KEY `uid` (`uid`,`itemid`),
  KEY `view` (`view`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_xmap_items`
--

LOCK TABLES `mf0kn_xmap_items` WRITE;
/*!40000 ALTER TABLE `mf0kn_xmap_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `mf0kn_xmap_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mf0kn_xmap_sitemap`
--

DROP TABLE IF EXISTS `mf0kn_xmap_sitemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mf0kn_xmap_sitemap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `introtext` text,
  `metadesc` text,
  `metakey` text,
  `attribs` text,
  `selections` text,
  `excluded_items` text,
  `is_default` int(1) DEFAULT '0',
  `state` int(2) DEFAULT NULL,
  `access` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `count_xml` int(11) DEFAULT NULL,
  `count_html` int(11) DEFAULT NULL,
  `views_xml` int(11) DEFAULT NULL,
  `views_html` int(11) DEFAULT NULL,
  `lastvisit_xml` int(11) DEFAULT NULL,
  `lastvisit_html` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mf0kn_xmap_sitemap`
--

LOCK TABLES `mf0kn_xmap_sitemap` WRITE;
/*!40000 ALTER TABLE `mf0kn_xmap_sitemap` DISABLE KEYS */;
INSERT INTO `mf0kn_xmap_sitemap` VALUES (1,'Sitemap','sitemap','',NULL,NULL,'{\"showintro\":\"1\",\"show_menutitle\":\"1\",\"classname\":\"\",\"columns\":\"\",\"exlinks\":\"img_blue.gif\",\"compress_xml\":\"1\",\"beautify_xml\":\"1\",\"include_link\":\"1\",\"news_publication_name\":\"\"}','{\"mainmenu\":{\"priority\":\"0.5\",\"changefreq\":\"weekly\",\"ordering\":0}}',NULL,1,1,1,'2014-07-16 03:54:21',5,0,88,0,1479760769,0);
/*!40000 ALTER TABLE `mf0kn_xmap_sitemap` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-24  1:05:44
